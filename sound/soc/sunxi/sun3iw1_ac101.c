/*
 * sound\soc\sunxi\sun3iw1_ac101.c
 * (C) Copyright 2014-2016
 * Allwinner Technology Co., Ltd. <www.allwinnertech.com>
 * Wolfgang Huang <huangjinhui@allwinnertech.com>
 *
 * some simple description for this code
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 */

#include <linux/module.h>
#include <linux/clk.h>
#include <linux/mutex.h>
#include <sound/pcm.h>
#include <sound/soc.h>
#include <sound/pcm_params.h>
#include <sound/soc-dapm.h>
#include <linux/io.h>
#include <linux/of.h>
#include "../codecs/ac101.h"

static bool daudio_pcm_select;
static int daudio_used;
static int ac101_used;
static int daudio_master;
static int audio_format;
static int signal_inversion;
static bool analog_bb;
static bool digital_bb;

static u32 over_sample_rate;

static atomic_t daudio_count = ATOMIC_INIT(-1);

static int sunxi_snddaudio_hw_params(struct snd_pcm_substream *substream,
		struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->codec_dai;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
	struct snd_soc_card *card = rtd->card;
	int sample_rate = params_rate(params);
	unsigned int freq, clk_div;
	int ret;

	switch (sample_rate) {
	case 8000:
	case 16000:
	case 32000:
	case 64000:
	case 128000:
	case 24000:
	case 48000:
	case 96000:
	case 192000:
		freq = 24576000;
		break;
	case 11025:
	case 22050:
	case 44100:
	case 88200:
	case 176400:
		freq = 22579200;
		break;
	default:
		dev_err(card->dev, "unsupport params rate\n");
		return -EINVAL;
	}

	ret = snd_soc_dai_set_sysclk(cpu_dai, 0, freq, 1);
	if (ret < 0) {
		pr_err("[daudio] cpu_dai set sysclk failed\n");
		return ret;
	}

	ret = snd_soc_dai_set_sysclk(codec_dai, AIF1_CLK, freq, 1);
	if (ret < 0) {
		pr_err("[daudio] codec_dai set sysclk failed\n");
		return ret;
	}

	/* ac101: master, AP: slave */
	ret = snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S |
			SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);
	if (ret < 0) {
		pr_err("[daudio] cpu_dai set fmt failed\n");
		return ret;
	}

	ret = snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_I2S |
			SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);
	if (ret < 0) {
		pr_err("[daudio] codec_dai set fmt failed\n");
		return ret;
	}

	ret = snd_soc_dai_set_clkdiv(cpu_dai, 0, sample_rate);
	if (ret < 0) {
		pr_err("[daudio] cpu_dai set clkdiv failed\n");
		return ret;
	}

	ret = snd_soc_dai_set_pll(codec_dai, AC101_MCLK1, 0,
			sample_rate * over_sample_rate, freq);
	if (ret < 0) {
		pr_err("[daudio] codec_dai set pll failed\n");
		return ret;
	}

	return 0;
}

static const struct snd_kcontrol_new ac101_pin_controls[] = {
	SOC_DAPM_PIN_SWITCH("External Speaker"),
	SOC_DAPM_PIN_SWITCH("Headphone"),
	SOC_DAPM_PIN_SWITCH("Earpiece"),
};
static const struct snd_soc_dapm_widget a80_ac101_dapm_widgets[] = {
	SND_SOC_DAPM_MIC("External MainMic", NULL),
	SND_SOC_DAPM_MIC("HeadphoneMic", NULL),
	SND_SOC_DAPM_MIC("DigitalMic", NULL),
	SND_SOC_DAPM_MIC("MIC2-MIC3", NULL),
};

static const struct snd_soc_dapm_route audio_map[] = {
	{"MainMic Bias", NULL, "External MainMic"},
	{"MIC1P", NULL, "MainMic Bias"},
	{"MIC1N", NULL, "MainMic Bias"},

	{"MIC2", NULL, "HMic Bias"},
	{"HMic Bias", NULL, "HeadphoneMic"},

	{"MIC3", NULL, "MIC2-MIC3"},

	{"MainMic Bias", NULL, "DigitalMic"},
	{"D_MIC", NULL, "MainMic Bias"},
};

static int sunxi_snddaudio_init(struct snd_soc_pcm_runtime *rtd)
{
/*        struct snd_soc_codec *codec = rtd->codec;*/
/*        snd_soc_dapm_disable_pin(&codec->dapm,	"HPOUTR");*/
/*        snd_soc_dapm_disable_pin(&codec->dapm,	"HPOUTL");*/
/*        snd_soc_dapm_disable_pin(&codec->dapm,	"EAROUTP");*/
/*        snd_soc_dapm_disable_pin(&codec->dapm,	"EAROUTN");*/
/*        snd_soc_dapm_disable_pin(&codec->dapm,	"SPK1P");*/
/*        snd_soc_dapm_disable_pin(&codec->dapm,	"SPK2P");*/
/*        snd_soc_dapm_disable_pin(&codec->dapm,	"SPK1N");*/
/*        snd_soc_dapm_disable_pin(&codec->dapm,	"SPK2N");*/
/*        snd_soc_dapm_disable_pin(&codec->dapm,	"MIC1P");*/
/*        snd_soc_dapm_disable_pin(&codec->dapm,	"MIC1N");*/
/*        snd_soc_dapm_disable_pin(&codec->dapm,	"MIC2");*/
/*        snd_soc_dapm_disable_pin(&codec->dapm,	"MIC3");*/
/*        snd_soc_dapm_disable_pin(&codec->dapm,	"D_MIC");*/
	return 0;
}

static struct snd_soc_ops sunxi_snddaudio_ops = {
	.hw_params      = sunxi_snddaudio_hw_params,
};

static struct snd_soc_dai_link sunxi_snddaudio_dai_link = {
	.name           = "sysvoice",
	.stream_name    = "SUNXI-AUDIO",
	.cpu_dai_name   = "sunxi-daudio",
	.platform_name  = "sunxi-daudio",
	.codec_dai_name = "snd-soc-dummy-dai",
	.codec_name     = "snd-soc-dummy",
	.init           = sunxi_snddaudio_init,
	.ops            = &sunxi_snddaudio_ops,
};

static struct snd_soc_card snd_soc_sunxi_snddaudio = {
	.name			= "snddaudio",
	.owner			= THIS_MODULE,
	.num_links		= 1,
	.dapm_widgets		= a80_ac101_dapm_widgets,
	.num_dapm_widgets	= ARRAY_SIZE(a80_ac101_dapm_widgets),
	.dapm_routes		= audio_map,
	.num_dapm_routes	= ARRAY_SIZE(audio_map),
	.controls		= ac101_pin_controls,
	.num_controls		= ARRAY_SIZE(ac101_pin_controls),
};

static int sunxi_snddaudio_dev_probe(struct platform_device *pdev)
{
	struct device_node *np = pdev->dev.of_node;
	struct snd_soc_dai_link	*dai_link;
	struct snd_soc_card *card;
	char name[30] = "snddaudio";
	unsigned int temp_val;
	int ret = 0;

	card = devm_kzalloc(&pdev->dev, sizeof(struct snd_soc_card),
			GFP_KERNEL);
	if (!card)
		return -ENOMEM;

	memcpy(card, &snd_soc_sunxi_snddaudio, sizeof(struct snd_soc_card));

	card->dev = &pdev->dev;

	card->dev = &pdev->dev;

	dai_link = devm_kzalloc(&pdev->dev,
			sizeof(struct snd_soc_dai_link), GFP_KERNEL);
	if (!dai_link) {
		ret = -ENOMEM;
		goto err_kfree_card;
	}
	memcpy(dai_link, &sunxi_snddaudio_dai_link,
			sizeof(struct snd_soc_dai_link));
	card->dai_link = dai_link;

	dai_link->cpu_dai_name = NULL;
	dai_link->cpu_of_node = of_parse_phandle(np,
			"sunxi,daudio0-controller", 0);
	if (dai_link->cpu_of_node)
		goto cpu_node_find;

	dai_link->cpu_of_node = of_parse_phandle(np,
			"sunxi,daudio1-controller", 0);
	if (dai_link->cpu_of_node)
		goto cpu_node_find;

	dai_link->cpu_of_node = of_parse_phandle(np,
			"sunxi,daudio-controller", 0);
	if (dai_link->cpu_of_node)
		goto cpu_node_find;

	dev_err(card->dev, "Perperty 'sunxi,daudio-controller' missing\n");

	goto err_kfree_link;

cpu_node_find:
	dai_link->platform_name = NULL;
	dai_link->platform_of_node =
				dai_link->cpu_of_node;

	ret = of_property_read_string(np, "sunxi,snddaudio-codec",
			&dai_link->codec_name);
	/*
	 * As we setting codec & codec_dai in dtb, we just setting the
	 * codec & codec_dai in the dai_link. But if we just not setting,
	 * we then using the snd-soc-dummy as the codec & codec_dai to
	 * construct the snd-card for audio playback & capture.
	 */
	if (!ret) {
		ret = of_property_read_string(np, "sunxi,snddaudio-codec-dai",
				&dai_link->codec_dai_name);
		if (ret < 0) {
			dev_err(card->dev, "codec_dai name invaild in dtb\n");
			ret = -EINVAL;
			goto err_kfree_link;
		}
		sprintf(name+3, "%s", dai_link->codec_name);
	} else {
		if (dai_link->cpu_of_node && of_property_read_u32(
			dai_link->cpu_of_node, "tdm_num", &temp_val) >= 0)
			sprintf(name+9, "%u", temp_val);
		else
			sprintf(name+9, "%d", atomic_inc_return(&daudio_count));
	}

	card->name = name;
	dev_info(card->dev, "codec: %s, codec_dai: %s.\n",
			dai_link->codec_name,
			dai_link->codec_dai_name);

	ret = snd_soc_register_card(card);
	if (ret) {
		dev_err(card->dev, "snd_soc_register_card failed\n");
		goto err_kfree_link;
	}

	ret = of_property_read_u32(np, "over_sample_rate", &over_sample_rate);
	if (ret < 0) {
		pr_err("[daudio] over_sample_rate configuration is "
				"misssing or invaild\n");
		goto err_kfree_link;
	}

	return 0;

err_kfree_link:
	devm_kfree(&pdev->dev, card->dai_link);
err_kfree_card:
	devm_kfree(&pdev->dev, card);
	return ret;
}

static int sunxi_snddaudio_dev_remove(struct platform_device *pdev)
{
	struct snd_soc_card *card = platform_get_drvdata(pdev);
	devm_kfree(&pdev->dev, card->dai_link);
	snd_soc_unregister_card(card);
	devm_kfree(&pdev->dev, card);
	return 0;
}

static const struct of_device_id sunxi_snddaudio_of_match[] = {
	{ .compatible = "allwinner,sunxi-daudio0-machine", },
	{},
};

static struct platform_driver sunxi_snddaudio_driver = {
	.driver = {
		.name   = "snddaudio",
		.owner  = THIS_MODULE,
		.pm     = &snd_soc_pm_ops,
		.of_match_table = sunxi_snddaudio_of_match,
	},
	.probe  = sunxi_snddaudio_dev_probe,
	.remove = sunxi_snddaudio_dev_remove,
};

static int __init sunxi_snddaudio_driver_init(void)
{
		return platform_driver_register(&sunxi_snddaudio_driver);
}

static void __exit sunxi_snddaudio_driver_exit(void)
{
		platform_driver_unregister(&sunxi_snddaudio_driver);
}

late_initcall(sunxi_snddaudio_driver_init);
module_exit(sunxi_snddaudio_driver_exit);

MODULE_AUTHOR("Wolfgang Huang");
MODULE_DESCRIPTION("SUNXI snddaudio ALSA SoC Audio Card Driver");
MODULE_LICENSE("GPL");
