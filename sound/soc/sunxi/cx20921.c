#include <linux/module.h>
#include <linux/cdev.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <linux/regmap.h>
#include <linux/regulator/consumer.h>
#include <linux/spi/spi.h>
#include <linux/slab.h>
#include <linux/of_device.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/pcm_params.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>
#include <sound/initval.h>
#include <sound/tlv.h>

#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/of_gpio.h>
#include <linux/sys_config.h>

#include "cx20921.h"


static const unsigned short cx209xx_addrs[] = {0x41, I2C_CLIENT_END};
static const struct i2c_device_id cx209xx_id[] = {
	{"cx209xx_1", 0},
	{}
};

static int cx20921_suspend(struct snd_soc_codec *codec)
{
	return 0;
}

static int cx20921_resume(struct snd_soc_codec *codec)
{
	return 0;
}

static int cx20921_hw_params(struct snd_pcm_substream *substream,
	struct snd_pcm_hw_params *params, struct snd_soc_dai *dai)
{
	return 0;
}

static int cx20921_set_fmt(struct snd_soc_dai *dai, unsigned int fmt)
{
	return 0;
}

static int cx20921_set_tdm_slot(struct snd_soc_dai *dai,
	unsigned int tx_mask, unsigned int rx_mask, int slots, int slot_width)
{
	return 0;
}

static int cx20921_set_sysclk(struct snd_soc_codec *codec,
	int clk_id, int source, unsigned int freq, int dir)
{
	return 0;
}

static int cx20921_set_pll(struct snd_soc_codec *codec, int pll_id, int source,
		unsigned int freq_in, unsigned int freq_out)
{
	return 0;
}


static const struct snd_soc_codec_driver cx20921_codec_driver = {
	.set_sysclk = cx20921_set_sysclk,
	.set_pll = cx20921_set_pll,
	.suspend = cx20921_suspend,
	.resume = cx20921_resume,
	.idle_bias_off = true,
};

static const struct snd_soc_dai_ops cx20921_dai_ops = {
	.hw_params = cx20921_hw_params,
	.set_fmt = cx20921_set_fmt,
	.set_tdm_slot = cx20921_set_tdm_slot,
};


#define CX20921_RATES SNDRV_PCM_RATE_8000_48000
#define CX20921_FORMATS (SNDRV_PCM_FMTBIT_S16_LE | SNDRV_PCM_FMTBIT_S20_3LE \
	 | SNDRV_PCM_FMTBIT_S24_3LE | SNDRV_PCM_FMTBIT_S32_LE)

static struct snd_soc_dai_driver cx20921_dai = {
	.name = "cx20921-dsp",
	.capture = {
		.stream_name = "Capture",
		.channels_min = 1,
		.channels_max = 2,
		.rates = CX20921_RATES,
		.formats = CX20921_FORMATS,
	},
};


/*---------cx209xx device--------------*/
static int cx209xx_probe(struct i2c_client *client,
	const struct i2c_device_id *id)
{
	struct device *dev = &client->dev;
	struct cx209xx_data *cx20921 = dev_get_platdata(dev);
	struct device_node *np = dev->of_node;
	struct gpio_config config;
	int ret;

	pr_debug("cx209xx_probe enter\n");

	if (!cx20921) {
		cx20921 = devm_kzalloc(dev, sizeof(*cx20921), GFP_KERNEL);
		if (!cx20921)
			return -ENOMEM;
	}

	i2c_set_clientdata(client, cx20921);

	if (!cx20921->pinctrl) {
		cx20921->pinctrl = devm_pinctrl_get(dev);
		if (IS_ERR_OR_NULL(cx20921->pinctrl)) {
			pr_warn(
			"[cx20921]request pinctrl handle for audio failed\n");
			return -EINVAL;
		}
	}

	if (!cx20921->pinstate) {
		cx20921->pinstate = pinctrl_lookup_state(
				cx20921->pinctrl, PINCTRL_STATE_DEFAULT);
		if (IS_ERR_OR_NULL(cx20921->pinstate)) {
			pr_warn("[cx20921] lookup pin default state failed\n");
			return -EINVAL;
		}
	}

	if (!cx20921->pinstate_sleep) {
		cx20921->pinstate_sleep = pinctrl_lookup_state(
				cx20921->pinctrl, PINCTRL_STATE_SLEEP);
		if (IS_ERR_OR_NULL(cx20921->pinstate_sleep)) {
			pr_warn("[cx20921]lookup pin default state failed\n");
			return -EINVAL;
		}
	}

	ret = pinctrl_select_state(cx20921->pinctrl, cx20921->pinstate);
	if (ret) {
		pr_warn("[cx20921]select pin default state failed\n");
		return ret;
	}

	cx20921->reset_gpio = of_get_named_gpio_flags(np, "reset-gpios",
		0, (enum of_gpio_flags *)&config);

	pr_debug("cx209xx reset_gpio: %d\n", cx20921->reset_gpio);
	if (gpio_is_valid(cx20921->reset_gpio)) {
		if (!devm_gpio_request(dev, cx20921->reset_gpio, "reset")) {
			gpio_direction_output(cx20921->reset_gpio, 1);
			devm_gpio_free(dev, cx20921->reset_gpio);
			}
	}

	/*register the codec*/
	return snd_soc_register_codec(dev,
		&cx20921_codec_driver, &cx20921_dai, 1);

}

static int cx209xx_remove(struct i2c_client *client)
{
	snd_soc_unregister_codec(&client->dev);
	return 0;
}

#ifdef CONFIG_OF
static const struct of_device_id cx20921_of_ids[] = {
	{ .compatible = "cx,cx20921", },
	{}
};
MODULE_DEVICE_TABLE(of, cx20921_of_ids);
#endif


MODULE_DEVICE_TABLE(i2c, cx209xx_id);

static struct i2c_driver cx209xx_i2c_driver = {
	.class          = I2C_CLASS_HWMON,
	.probe          = cx209xx_probe,
	.remove			= cx209xx_remove,
	.id_table       = cx209xx_id,
	.driver         = {
		.name   = "cx20921",
		.of_match_table = of_match_ptr(cx20921_of_ids),
		.owner  = THIS_MODULE,
	},
	.address_list   = cx209xx_addrs
};

module_i2c_driver(cx209xx_i2c_driver);

MODULE_AUTHOR("xudong@allwinnertech.com");
MODULE_DESCRIPTION("IOE for cx20921");
MODULE_LICENSE("GPL");
