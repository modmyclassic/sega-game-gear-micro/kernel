#ifndef __CX20921_H__

struct cx209xx_data {
	struct device *dev;
	struct regmap *regmap;
	struct pinctrl *pinctrl;
	struct pinctrl_state  *pinstate;
	struct pinctrl_state  *pinstate_sleep;
	int reset_gpio;
};

#endif
