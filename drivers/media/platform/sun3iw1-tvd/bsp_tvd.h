/* bsp_tvd.h
 *
 * Copyright (c) 2007-2017 Allwinnertech Co., Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#ifndef BSP_TVD_H_
#define BSP_TVD_H_

#include "asm-generic/int-ll64.h"
#include "linux/kernel.h"
#include "linux/mm.h"
#include "linux/semaphore.h"
#include <asm/memory.h>
#include <asm/uaccess.h>
#include <asm/unistd.h>
#include <linux/cdev.h>
#include <linux/clk.h>
#include <linux/delay.h>
#include <linux/dma-mapping.h>
#include <linux/err.h>
#include <linux/errno.h>
#include <linux/fs.h>
#include <linux/i2c.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/kthread.h>
#include <linux/module.h>
#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/of_iommu.h>
#include <linux/of_irq.h>
#include <linux/of_platform.h>
#include <linux/platform_device.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/types.h>
#include <linux/vmalloc.h>

/* */
/* detail information of registers */
/* */
/* pixel format(yuv) */
/* only used in scaler framebuffer */
typedef enum __PIXEL_YUVFMT {
	PIXEL_YUV444 = 0x10,
	PIXEL_YUV422,
	PIXEL_YUV420,
	PIXEL_YUV411,
	PIXEL_CSIRGB,
	PIXEL_OTHERFMT,
} __pixel_yuvfmt_t;

typedef enum { BT601 = 0, BT709, YCC, VXYCC, MONO } __cs_mode_t;

typedef struct {
	__s32 x;
	__s32 y;
	__u32 width;
	__u32 height;
} RECT; /* rect attrib                          */
typedef struct {
	__u32 width;
	__u32 height;
} SIZE; /* rect size                            */

typedef SIZE __rectsz_t;
typedef RECT __rect_t;

#define TVD_MODE_NUM 8
#define TVD_BUFFER_NUM 6

typedef enum tag_BUF_SCRAB_MODE_TVD {
	TVD0_FULL = 0, /*TVD0 on one buffer */
	TVD1_FULL = 1, /*TVD1 on one buffer */

	TVD_01_LR =
	    2, /*TVD0(L) and TVD1(R) with same size and format parallel on one
		  buffer */
	TVD_10_LR =
	    3, /*TVD1(L) and TVD0(R) with same size and format parallel on one
		  buffer */

	TVD_01_UD = 4, /*TVD0(L) and TVD1(R) with same size and format
			  sequential on one buffer */
	TVD_10_UD = 5, /*TVD1(L) and TVD0(R) with same size and format
			  sequential on one buffer */

	TVD0_TDM_2CH_2X1 = 6, /*TVD0 TDM CH0/1 with same size and format */
	TVD0_TDM_4CH_2X2 =
	    7, /*TVD0 TDM CH0/1 + CH2/3 with same size and format */

	TVD_SC_NA = 0xff,
} __buf_scrab_mode_tvd_t;

typedef struct tag_TVD_MODE {

	__pixel_yuvfmt_t color_format; /*__pixel_yuvfmt_t */
	__u32 component_seq;	   /*__yuv_seq_t + __bayer_seq_t */
	__u32 store_mode;	      /*__yuv_mod_t + __bayer_store_mode_t */
	__rectsz_t size;
	__u32 frame_rate;   /*Hz, x1000 */
	__u32 frame_period; /*us */
	__cs_mode_t color_space;
	/*=========add for TVD0+TVD1 =============== */
	__buf_scrab_mode_tvd_t tvd_buf_scrab_mode;
	/*======================== */
} __tvd_mode_t;

typedef struct tag_TVD_MODE_ALL {
	__u32 number;
	__tvd_mode_t tvd_mode[TVD_MODE_NUM];
} __tvd_mode_all_t;

typedef enum __DRV_TVD_CMD_SET {
	DRV_TVD_CMD_NONE = 0,
	DRV_TVD_CMD_INIT, /*ret = EPDK_OK/EPDK_FAIL */
	DRV_TVD_CMD_EXIT, /*ret = EPDK_OK/EPDK_FAIL */
	/*pbuffer = __csi_mode_t*, ret = EPDK_OK/EPDK_FAIL*/
	DRV_TVD_CMD_SET_MODE,
	/*pbuffer = __csi_mode_t*, ret = EPDK_OK/EPDK_FAIL*/
	DRV_TVD_CMD_GET_MODE,

	DRV_TVD_CMD_GET_MODE_ALL,/*pbuffer = __csi_mode_total_t * */
	DRV_TVD_CMD_CAPTURE_ON,/*ret = EPDK_OK/EPDK_FAIL */
	DRV_TVD_CMD_CAPTURE_OFF,/*ret = EPDK_OK/EPDK_FAIL */
	/*pbuffer= (__csi_frame_t**), ret =EPDK_OK/EPDK_FAIL */
	DRV_TVD_CMD_REQUEST_FRAME,
	DRV_TVD_CMD_RELEASE_FRAME, /*aux = frame_id, ret = EPDK_OK/EPDK_FAIL */
	DRV_TVD_CMD_SET_PTS,       /*aux = pts(__u32) */
	DRV_TVD_CMD_GET_PTS,       /*ret=pts(__u32) */
	/*设置PTS的获取方式，aux = 0:CSI自己算PTS;
	 * 1:通过回调函数得到PTS; 2.不需要PTS */
	DRV_TVD_CMD_SET_PTS_MODE,

	/*注册回调函数,得到PTS。该callback的原型为CB_GetPTS,
	 * 参数*arg是__s64*的指针,表示PTS，单位us.
	 * *pbuffer = callback */
	DRV_TVD_CMD_INSTALL_CALLBACK_GET_PTS,

	/*设置frame*/
	/*queue.必须在capture_on之前设置。因为capture*/
	/*off会清掉. pbuffer =*/
	/*__csi_frame_queue_t*,主要是设置buffer和frame_id，
	 * 其他的参数还是由csidrv决定.*/
	DRV_TVD_CMD_SET_FRAME_QUEUE,
	DRV_TVD_CMD_SET_COLOR_EFFECT, /*absence aux = color effet */
	DRV_TVD_CMD_GET_COLOR_EFFECT, /*absence ret = color effet */
	DRV_TVD_CMD_SET_AWB,	  /*absence aux = awb */
	DRV_TVD_CMD_GET_AWB,	  /*absence ret = awb */
	DRV_TVD_CMD_SET_AE,	   /*absence aux = ae */
	DRV_TVD_CMD_GET_AE,	   /*absence ret = ae */
	DRV_TVD_CMD_SET_BRIGHT,       /*absence aux = bright */
	DRV_TVD_CMD_GET_BRIGHT,       /*absence ret = bright */
	DRV_TVD_CMD_SET_CONTRAST,     /*absence aux = contrast */
	DRV_TVD_CMD_GET_CONTRAST,     /*absence ret = contrast */
	DRV_TVD_CMD_SET_BAND,	 /*absence aux = band */
	DRV_TVD_CMD_GET_BAND, /*absence ret = band//add 2011-8-3 13:13:13 */
	DRV_TVD_CMD_GET_PROGRSSV_INFO, /*absence ret = b_progr */
	DRV_TVD_CMD_SET_MASTER_PORT =
	    DRV_TVD_CMD_GET_PROGRSSV_INFO, /*absence */
	DRV_TVD_CMD_GET_MASTER_PORT,       /*absence */
	DRV_TVD_CMD_SET_TV_SEL = 0x100,
	DRV_TVD_CMD_SEL_CHANNEL, /*aux = __drv_TVD_sel_channel */
	DRV_TVD_CMD_SEL_SOURCE,  /*aux = TVD_SOURCE_CVBS, TVD_SOURCE_YPbPr */
	DRV_TVD_CMD_SEL_FORMAT,  /*aux = tvd_mod_fmt_t */
	DRV_TVD_CMD_PAL_NTSC, /*if TVD_SOURCE_CVBS, aux = __drv_TVD_pal_ntsc */
	/*if TVD_SOURCE_YPbPr, aux =__drv_TVD_YPbPr_size */
	DRV_TVD_CMD_YPbPr_SIZE,
	DRV_TVD_CMD_SET_INPUT_CHANNEL, /*aux = __drv_TVD_INPUT_CHANNEL */
	/* return : bit0: 1->plugin, 0->plugout ;bit4:1->pal,0:ntsc;bit8:*/
	DRV_TVD_CMD_GET_ADC_STATUS,
	/*返回32位整数，第0bit为1代表有信号，为0代表无信号。
	 * 如果有信号，那么第4bit(从0开始计)为1代表P制，为0代表N制*/
	DRV_TVD_CMD_GET_STATUS,

	DRV_TVD_CMD_,

} __drv_TVD_cmd_set_t;
typedef struct tag_TVD_FRAME {
	__s32 frame_id;

	bool bProgressiveSrc; /* Indicating the source is progressive or not */
	bool
	    bTopFieldFirst; /* VPO should check this flag when bProgressiveSrc
			       is FALSE */
	__u16 eAspectRatio; /*the source picture aspect ratio */
	__rect_t src_rect;  /* source valid size, 宏块对齐的图像大小,
			       一般就是frame buffer了 */
	__rect_t dst_rect; /* source display size,真实图像的位置大小 */
	__u32 addr[3];     /* data buffer address */

	__u16 color_format;  /*same with __tvd_mode_t */
	__u32 component_seq; /*same with __tvd_mode_t */
	__u32 store_mode;    /*same with __tvd_mode_t */
	__u16 frame_rate;    /*same with __tvd_mode_t */

	__s64 uPts; /*us */
	__s32 bPtsValid;
} __tvd_frame_t;

typedef struct tag_TVD_FRAME_QUEUE {
	__s32 num; /*有效的帧个数，num <= TVD_BUFFER_NUM */
	__tvd_frame_t tvd_frame_array[TVD_BUFFER_NUM];
} __tvd_frame_queue_t;

typedef enum e_TVD_SEL_CHANNEL {
	TVD_CHANNEL_0,
	TVD_CHANNEL_1,
	TVD_CHANNEL_2,
	TVD_CHANNEL_3,

	TVD_SEL_CHANNEL_MAX

} __drv_TVD_sel_channel;

typedef enum e_TVD_SEL_SOURCE {
	TVD_SOURCE_CVBS,
	TVD_SOURCE_YPbPr,

	TVD_SEL_SOURCE_MAX

} __drv_TVD_sel_source;

typedef enum e_TVD_PAL_NTSC {
	TVD_SOURCE_NTSC,
	TVD_SOURCE_PAL,

	TVD_PAL_NTSC_MAX

} __drv_TVD_pal_ntsc;

typedef enum e_TVD_YPbPr_SIZE {
	TVD_YPbPr_480i,
	TVD_YPbPr_576i,

	TVD_YPbPr_SIZE_MAX

} __drv_TVD_YPbPr_size;

typedef enum e_TVD_INPUT_CHANNEL {
	TVD_INPUT_CHANNEL_0,
	TVD_INPUT_CHANNEL_1,

	TVD_INPUT_CHANNEL_MAX

} __drv_TVD_INPUT_CHANNEL;

typedef enum {
	TVD_UV_NON_MB_COMBINED_YUV422,
	TVD_UV_NON_MB_COMBINED_YUV420,
	TVD_UV_MB_COMBINED_YUV420,
	TVD_MOD_FMT_MAX

} tvd_mod_fmt_t;

typedef enum {
	TVD_PL_YUV422,
	TVD_PL_YUV420,
	TVD_MB_YUV420,
} tvd_fmt_t;

typedef enum {
	TVD_NTSC,
	TVD_PAL,
} tvd_mode_t;

typedef enum {
	TVD_FRAME_DONE,
	TVD_LOCK,
	TVD_UNLOCK,
} tvd_irq_t;

void BSP_TVD_init(__u32 id);

void BSP_TVD_irq_enable(__u32 id, tvd_irq_t irq);
void BSP_TVD_irq_disable(__u32 id, tvd_irq_t irq);
__u32 BSP_TVD_irq_status_get(__u32 id, tvd_irq_t irq);
void BSP_TVD_irq_status_clear(__u32 id, tvd_irq_t irq);

void BSP_TVD_capture_on(__u32 id);
void BSP_TVD_capture_off(__u32 id);

void BSP_TVD_set_addr_y(__u32 id, __u32 addr);
void BSP_TVD_set_addr_c(__u32 id, __u32 addr);

void BSP_TVD_set_width(__u32 id, __u32 w);
void BSP_TVD_set_width_jump(__u32 id, __u32 j);
void BSP_TVD_set_height(__u32 id, __u32 h);
void BSP_TVD_set_hor_start(__u32 id, __u32 h_start);
void BSP_TVD_set_ver_start(__u32 id, __u32 v_start);
__u32 BSP_TVD_get_hor_start(__u32 id);
__u32 BSP_TVD_get_ver_start(__u32 id);

void BSP_TVD_set_fmt(__u32 id, tvd_fmt_t fmt);
void BSP_TVD_config(__u32 interface, __u32 system, __u32 format);
__u32 BSP_TVD_get_status(__u32 id);

void BSP_TVD_3D_COMB_Filter(__u32 enable, __u32 addr);
void BSP_TVD_input_select(__u32 input);

#endif
