/* bsp_tvd.c
 *
 * Copyright (c) 2007-2017 Allwinnertech Co., Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#include "bsp_tvd.h"

#define TVD_REGS_BASE 0xf1c0b000

#define REG_RD32(reg) (*((volatile __u32 *)(reg)))
#define REG_WR32(reg, value) (*((volatile __u32 *)(reg)) = (value))

void BSP_TVD_init(__u32 id)
{
	REG_WR32(TVD_REGS_BASE + 0x0088, 0x04000000);
	REG_WR32(TVD_REGS_BASE + 0x0070, 0x00000100);
	/*select tvin0 or tvin1*/
	if (id == 0)
		REG_WR32(TVD_REGS_BASE + 0x0e04, 0x8002AAA8);
	else
		REG_WR32(TVD_REGS_BASE + 0x0e04, 0x8002AAA9);

	REG_WR32(TVD_REGS_BASE + 0x0e2c, 0x000b0000);
	REG_WR32(TVD_REGS_BASE + 0x0040, 0x04000310); /*? */

	/* reset tvd */
	REG_WR32(TVD_REGS_BASE + 0x0000, 0x00000000);
	msleep(1); /* need delay here for tvd reset */

	REG_WR32(TVD_REGS_BASE + 0x0000, 0x00001f01);

	/* IP config */
	REG_WR32(TVD_REGS_BASE + 0x0014,
		 0x20000000); /* 2000_0000    2400_0000 */

	/* REG_WR32(TVD_REGS_BASE+0x0f6c, 0x00fffa0a); //YC separation config */
	/*		REG_WR32(TVD_REGS_BASE+0x0f44, 0x00004632); //burst gate
	 */
	REG_WR32(TVD_REGS_BASE + 0x0f24,
		 0x0682810a); /* DISABLE AGC GATE KILL!!!!!!!!!!!!!!!!! */
	/*REG_WR32(TVD_REGS_BASE+0x0f24, 0x0000b90a); //DISABLE AGC
	 */
	/*GATE KILL!!!!!!!!!!!!!!!!! */
	REG_WR32(TVD_REGS_BASE + 0x0f28,
		 0x00006440); /* 0000_6440    0000_5838 */
	REG_WR32(TVD_REGS_BASE + 0x0f4c,
		 0x0e70106c); /* NO CLAMP DURING VSYNC!!!!!!!!!!!!!!!!! */
	REG_WR32(TVD_REGS_BASE + 0x0f54, 0x00000000); /* */
	REG_WR32(TVD_REGS_BASE + 0x0f58, 0x00000082); /* */
	REG_WR32(TVD_REGS_BASE + 0x0f6c, 0x00fffad0); /* YC separation config */
	REG_WR32(TVD_REGS_BASE + 0x0f70, 0x0000a010); /* 0x00002010); */
}

void BSP_TVD_config(__u32 interface, __u32 system, __u32 format)
{
	/* global reg set here */

	if (interface == TVD_SOURCE_CVBS) { /* composite */
		switch (system) {
		case TVD_SOURCE_NTSC:
			REG_WR32(TVD_REGS_BASE + 0x0008, 0x00010001);
			REG_WR32(TVD_REGS_BASE + 0x000c,
				 0x00202068); /* adjust luma brightness */
			REG_WR32(
			    TVD_REGS_BASE + 0x0010,
			    0x00300080); /* just statruation and peak gain */
			REG_WR32(
			    TVD_REGS_BASE + 0x0018,
			    0x21f07c1f); /* 21f0_7c1f    262E_8BA2 Chroma DTO */
			REG_WR32(TVD_REGS_BASE + 0x001c,
				 0x00820022); /* hactive and vactive start */
			REG_WR32(TVD_REGS_BASE + 0x0f08,
				 0x00590100); /* notch width 0059_0100 */
			REG_WR32(TVD_REGS_BASE + 0x0f0c,
				 0x00000010); /* YC sep */
			REG_WR32(TVD_REGS_BASE + 0x0f10,
				 0x008A32DD); /* sync height */
			REG_WR32(TVD_REGS_BASE + 0x0f14, 0x80000080); /* */
			/*?		REG_WR32(TVD_REGS_BASE+0x0018, */
			/*0x00002080); //adjust luma brightness */
			REG_WR32(TVD_REGS_BASE + 0x0f1c,
				 0x008A0000); /* chroma AGC target */
			REG_WR32(TVD_REGS_BASE + 0x0f2c, 0x0000CB74);
			REG_WR32(TVD_REGS_BASE + 0x0f44,
				 0x00004632); /* burst gate */
			REG_WR32(TVD_REGS_BASE + 0x0f74,
				 0x000003c3); /* chroma edge enhance */
			/*		REG_WR32(TVD_REGS_BASE+0x0f74, */
			/*0x00000340); //default */

			REG_WR32(TVD_REGS_BASE + 0x0f80,
				 0x00500000); /* hactive width */
			REG_WR32(TVD_REGS_BASE + 0x0f84,
				 0x00610000); /* vactive height */
			REG_WR32(TVD_REGS_BASE + 0x0000,
				 0x00000001); /* test = 0x80001f3f */

			if (TVD_MB_YUV420 == format) {
				BSP_TVD_set_width(0, 704);
				BSP_TVD_set_width_jump(0, 704);
				BSP_TVD_set_height(0, 224);
				BSP_TVD_set_ver_start(0,
						      0x22 + (480 - 448) / 2);
			} else {
				BSP_TVD_set_width(0, 720);
				BSP_TVD_set_width_jump(0, 720);
				BSP_TVD_set_height(0, 240);
			}

			break;
		case TVD_SOURCE_PAL:
			REG_WR32(TVD_REGS_BASE + 0x0008, 0x01111001);
			REG_WR32(TVD_REGS_BASE + 0x000c,
				 0x03714080); /* adjust luma brightness */
			REG_WR32(TVD_REGS_BASE + 0x0010, 0x00310080);
			REG_WR32(TVD_REGS_BASE + 0x0018,
				 0x2a098acb); /* chroma dto */
			REG_WR32(TVD_REGS_BASE + 0x001c,
				 0x0087002f); /* hactive and vactive start */
			REG_WR32(
			    TVD_REGS_BASE + 0x0f08,
			    0x11590902); /* disable black level correction */
			/* for 7.5 blank-to-black setup */
			/* cagc en */
			REG_WR32(TVD_REGS_BASE + 0x0f0c,
				 0x00000016); /* YC sep */
			REG_WR32(TVD_REGS_BASE + 0x0f10, 0x008a32ec); /* */
			REG_WR32(TVD_REGS_BASE + 0x0f14,
				 0x80000080); /* adjust YC delay */
			REG_WR32(TVD_REGS_BASE + 0x0f1c,
				 0x00930000); /* chroma AGC target */
			REG_WR32(TVD_REGS_BASE + 0x0f2c, 0x00000d74);
			REG_WR32(TVD_REGS_BASE + 0x0f44,
				 0x0000412d); /* burst gate */
			/* REG_WR32(TVD_REGS_BASE+0x0f6c, 0x00fffa0a); // */
			REG_WR32(TVD_REGS_BASE + 0x0f74, 0x00000343); /* */

			REG_WR32(TVD_REGS_BASE + 0x0f80,
				 0x00500000); /* hactive width */
			REG_WR32(TVD_REGS_BASE + 0x0f84,
				 0x00c10000); /* vactive height */
			REG_WR32(TVD_REGS_BASE + 0x0000, 0x00000001); /* */
			/* REG_WR32(TVD_REGS_BASE+0x001c, 0x00870056); //hactive
			 */
			/* and vactive start */

			/* REG_WR32(TVD_REGS_BASE+0x0e04, 0x00000000); */
			if (TVD_MB_YUV420 == format) {
				BSP_TVD_set_width(0, 704);
				BSP_TVD_set_width_jump(0, 704);
				BSP_TVD_set_height(0, 224);
				BSP_TVD_set_ver_start(0,
						      0x28 + (576 - 448) / 2);

			} else {
				BSP_TVD_set_width(0, 720);
				BSP_TVD_set_width_jump(0, 720);
				BSP_TVD_set_height(0, 288);
			}
			break;
		} /* switch */
		  /* REG_WR32(TVD_REGS_BASE+0x0504,0x00000000); */
		  /* REG_WR32(TVD_REGS_BASE+0x052c,0x00110000); */
		  /* 1 channel cvbs */
		  /* REG_WR32(TVD_REGS_BASE+0x0500,0x00000111); */
		  /* REG_WR32(TVD_REGS_BASE+0x0000,0x00000321); */
		  /* default open all 4 channels if you don't care power */
		  /* consumption */
		  /* REG_WR32(TVD_REGS_BASE+0x0500,0x00000f11); */

		/*		REG_WR32(TVD_REGS_BASE+0x0e2c,0x60000000); */
	}
#if 1
	{
		__u32 v;
		v = REG_RD32(TVD_REGS_BASE + 0x0f20);
		/* DebugPrintf("1-----0x%x\n", REG_RD32(TVD_REGS_BASE+0x0f20));
		 */
		v &= ~((1 << 6) | (1 << 7));
		v |= ((0 << 6) | (1 << 7));
		REG_WR32(TVD_REGS_BASE + 0x0f20, v);
		/* DebugPrintf("1-----0x%x\n", REG_RD32(TVD_REGS_BASE+0x0f20));
		 */
	}
#endif
}

void BSP_TVD_set_width(__u32 id, __u32 w)
{
	__u32 reg_val;
	reg_val = REG_RD32(TVD_REGS_BASE + 0x008c);
	reg_val &= ~(0xfff << 0);
	reg_val |= ((w > 720) ? 720 : w) << 0;
	REG_WR32(TVD_REGS_BASE + 0x008c, reg_val);
}

void BSP_TVD_set_width_jump(__u32 id, __u32 j)
{
	REG_WR32(TVD_REGS_BASE + 0x0090, j);
}

void BSP_TVD_set_height(__u32 id, __u32 h)
{
	__u32 reg_val;
	reg_val = REG_RD32(TVD_REGS_BASE + 0x008c);
	reg_val &= ~(0x7ff << 16);
	reg_val |= h << 16;
	REG_WR32(TVD_REGS_BASE + 0x008c, reg_val);
}
void BSP_TVD_set_hor_start(__u32 id, __u32 h_start)
{
	__u32 reg_val;
	reg_val = REG_RD32(TVD_REGS_BASE + 0x001c);
	reg_val &= ~(0xfff << 16);
	reg_val |= (h_start & 0xfff) << 16;
	REG_WR32(TVD_REGS_BASE + 0x001c, reg_val);
}

void BSP_TVD_set_ver_start(__u32 id, __u32 v_start)
{
	__u32 reg_val;
	reg_val = REG_RD32(TVD_REGS_BASE + 0x001c);
	reg_val &= ~(0x7ff << 0);
	reg_val |= (v_start & 0x7ff) << 0;
	REG_WR32(TVD_REGS_BASE + 0x001c, reg_val);
}
__u32 BSP_TVD_get_hor_start(__u32 id)
{
	__u32 reg_val;
	reg_val = REG_RD32(TVD_REGS_BASE + 0x001c);
	reg_val = (reg_val >> 16) & 0xfff;
	return reg_val;
}

__u32 BSP_TVD_get_ver_start(__u32 id)
{
	__u32 reg_val;
	reg_val = REG_RD32(TVD_REGS_BASE + 0x001c);
	reg_val = reg_val & 0x7ff;
	return reg_val;
}
void BSP_TVD_irq_enable(__u32 id, tvd_irq_t irq)
{
	__u32 reg_val;
	switch (irq) {
	case TVD_FRAME_DONE:
		reg_val = REG_RD32(TVD_REGS_BASE + 0x009c);
		reg_val |= 1 << (24);
		REG_WR32(TVD_REGS_BASE + 0x009c, reg_val);
		break;
	case TVD_LOCK:
		reg_val = REG_RD32(TVD_REGS_BASE + 0x00a4);
		reg_val |= 1 << (0);
		REG_WR32(TVD_REGS_BASE + 0x00a4, reg_val);
		break;
	case TVD_UNLOCK:
		reg_val = REG_RD32(TVD_REGS_BASE + 0x00a4);
		reg_val |= 1 << (1);
		REG_WR32(TVD_REGS_BASE + 0x00a4, reg_val);
		break;
	}
}

void BSP_TVD_irq_disable(__u32 id, tvd_irq_t irq)
{
	__u32 reg_val;
	switch (irq) {
	case TVD_FRAME_DONE:
		reg_val = REG_RD32(TVD_REGS_BASE + 0x009c);
		reg_val &= ~(1 << 24);
		REG_WR32(TVD_REGS_BASE + 0x009c, reg_val);
		break;
	case TVD_LOCK:
		reg_val = REG_RD32(TVD_REGS_BASE + 0x00a4);
		reg_val &= ~(1 << 0);
		REG_WR32(TVD_REGS_BASE + 0x00a4, reg_val);
		break;
	case TVD_UNLOCK:
		reg_val = REG_RD32(TVD_REGS_BASE + 0x00a4);
		reg_val &= ~(1 << 1);
		REG_WR32(TVD_REGS_BASE + 0x00a4, reg_val);
		break;
	}
}

__u32 BSP_TVD_irq_status_get(__u32 id, tvd_irq_t irq)
{
	__u32 reg_val, ret = 0;
	switch (irq) {
	case TVD_FRAME_DONE:
		reg_val = REG_RD32(TVD_REGS_BASE + 0x0094);
		ret = (reg_val >> 24) & 1;
		break;
	}
	return ret;
}

void BSP_TVD_irq_status_clear(__u32 id, tvd_irq_t irq)
{
	__u32 reg_val;
	switch (irq) {
	case TVD_FRAME_DONE:
		reg_val = 1 << 24;
		REG_WR32(TVD_REGS_BASE + 0x0094, reg_val);
		break;
	case TVD_LOCK:
		reg_val = 1 << 0;
		REG_WR32(TVD_REGS_BASE + 0x00a8, reg_val);
		break;
	case TVD_UNLOCK:
		reg_val = 1 << 1;
		REG_WR32(TVD_REGS_BASE + 0x00a8, reg_val);
		break;
	}
}

void BSP_TVD_capture_on(__u32 id)
{
	__u32 reg_val;
	reg_val = REG_RD32(TVD_REGS_BASE + 0x0088);
	reg_val |= 1 << 0;
	REG_WR32(TVD_REGS_BASE + 0x0088, reg_val);
}

void BSP_TVD_capture_off(__u32 id)
{
	__u32 reg_val;
	reg_val = REG_RD32(TVD_REGS_BASE + 0x0088);
	reg_val &= ~(1 << 0);
	REG_WR32(TVD_REGS_BASE + 0x0088, reg_val);
}

void BSP_TVD_set_addr_y(__u32 id, __u32 addr)
{
	__u32 reg_val;
	REG_WR32(TVD_REGS_BASE + 0x0080, addr);
	reg_val = REG_RD32(TVD_REGS_BASE + 0x0088);
	reg_val |= (1 << 28);
	REG_WR32(TVD_REGS_BASE + 0x0088, reg_val);
}

void BSP_TVD_set_addr_c(__u32 id, __u32 addr)
{
	__u32 reg_val;
	REG_WR32(TVD_REGS_BASE + 0x0084, addr);
	reg_val = REG_RD32(TVD_REGS_BASE + 0x0088);
	reg_val |= (1 << 28);
	REG_WR32(TVD_REGS_BASE + 0x0088, reg_val);
}

void BSP_TVD_set_fmt(__u32 id, tvd_fmt_t fmt)
{
	__u32 reg_val;
	reg_val = REG_RD32(TVD_REGS_BASE + 0x0088);
	switch (fmt) {
	case TVD_PL_YUV422:
		reg_val &= ~(1 << 24);
		reg_val |= 1 << 4;
		break;
	case TVD_PL_YUV420:
		reg_val &= ~(1 << 24);
		reg_val &= ~(1 << 4);
		break;
	case TVD_MB_YUV420:
		reg_val |= 1 << 24;
		reg_val &= ~(1 << 4);
		break;
	}
	REG_WR32(TVD_REGS_BASE + 0x0088, reg_val);
}

__u32 BSP_TVD_get_status(__u32 id)
{
	__u32 reg_val = 0;
	__u32 det = 0;
	__u32 system = 0;
	reg_val = REG_RD32(TVD_REGS_BASE + 0x0e40);

	det = ((reg_val & 0x0000000e) == 0x0000000e)?1:0;
	if ( (reg_val & (1 << 18)) && (reg_val & (1 << 16)) ) {
		system = 1; /* get system = pal */
	} else {
		system = 0; /* get system = ntsc */
	}
	return (det << 0) + (system << 4); /* bit0=det bit4=system */
}

void BSP_TVD_3D_COMB_Filter(__u32 enable, __u32 addr)
{
	__u32 reg_val;

	if (enable) {
		/* 3D config */
		REG_WR32(TVD_REGS_BASE + 0x004c, addr);
		REG_WR32(TVD_REGS_BASE + 0x0050, addr + 0x200000);
		REG_WR32(TVD_REGS_BASE + 0x0054, 0x00200000);
		REG_WR32(TVD_REGS_BASE + 0x0048, 0x04040001);
		reg_val = REG_RD32(TVD_REGS_BASE + 0x0008);
		reg_val &= ~(1 << 0);
		REG_WR32(TVD_REGS_BASE + 0x0008, reg_val);
		reg_val = REG_RD32(TVD_REGS_BASE + 0x0000);
		reg_val |= (1 << 4);
		REG_WR32(TVD_REGS_BASE + 0x0000, reg_val);
	} else {
		reg_val = REG_RD32(TVD_REGS_BASE + 0x0008);
		reg_val |= (1 << 0);
		REG_WR32(TVD_REGS_BASE + 0x0008, reg_val);
		reg_val = REG_RD32(TVD_REGS_BASE + 0x0000);
		reg_val &= ~(1 << 4);
		REG_WR32(TVD_REGS_BASE + 0x0000, reg_val);
	}
}

void BSP_TVD_input_select(__u32 input)
{
	BSP_TVD_init(input);
}

void BSP_TVD_y_peak(__u32 enable)
{
	__u32 reg_val;
	if (enable) {
		reg_val = REG_RD32(TVD_REGS_BASE + 0x000c);
		reg_val |= (1 << 16);
		REG_WR32(TVD_REGS_BASE + 0x000c, reg_val);
	} else {
		reg_val = REG_RD32(TVD_REGS_BASE + 0x000c);
		reg_val &= ~(1 << 16);
		REG_WR32(TVD_REGS_BASE + 0x000c, reg_val);
	}
}
void BSP_TVD_c_peak(__u32 enable)
{
	__u32 reg_val;
	if (enable) {
		reg_val = REG_RD32(TVD_REGS_BASE + 0x0010);
		reg_val |= (1 << 16);
		REG_WR32(TVD_REGS_BASE + 0x0010, reg_val);
	} else {
		reg_val = REG_RD32(TVD_REGS_BASE + 0x0010);
		reg_val &= ~(1 << 16);
		REG_WR32(TVD_REGS_BASE + 0x0010, reg_val);
	}
}
__s32 BSP_TVD_get_lock_state(void)
{
	__u32 reg_val;
	__u32 lock = 0;
	reg_val = REG_RD32(TVD_REGS_BASE + 0x00a8);

	if (reg_val & (1 << 0))
		lock = 1;/* lock */
	else if (reg_val & (1 << 1))
		lock = 0;/* unlock */
	return lock;
}

__s32 BSP_TVD_get_tvin_system()
{
	__u32 reg_val = 0;
	__u32 system = 0;

	reg_val = REG_RD32(TVD_REGS_BASE + 0x0e40);
	if (reg_val & (1 << 18)) {
		system = TVD_SOURCE_PAL; /* get system = pal */
	} else {
		system = TVD_SOURCE_NTSC; /* get system = ntsc */
	}

	return system;
}
