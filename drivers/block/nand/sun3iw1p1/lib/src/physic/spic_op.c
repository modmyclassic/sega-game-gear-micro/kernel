/**
 * spi_hal.c
 * date:    2012/2/12 22:34:41
 * author:  Aaron<leafy.myeh@allwinnertech.com>
 * history: V0.1
 */
//#include "include.h"
#include "spinand_drv_cfg.h"
#include "nand_type_spinand.h"
#include "spic.h"

#define MAX_SPI_NUM 2

__s32 Wait_Tc_Complete(void)
{
	__u32 timeout = 0xffffff;

	while(!(readw(SPI_ISR)&(0x1<<12)))//wait transfer complete
	{
		timeout--;
		if (!timeout)
			break;
	}
	if(timeout == 0)
	{
		PHY_ERR("TC Complete wait status timeout!\n");
		return ERR_TIMEOUT;
	}

	return 0;
}

__s32 Spic_init(__u32 spi_no)
{
	__u32 rval;

	    //init pin
	if(0 != NAND_PIORequest(spi_no)) {
		PHY_ERR("request spi gpio fail!\n");
		return -1;
	} else
		PHY_DBG("request spi gpio  ok!\n");

	//request general dma channel
	if( 0 != spinand_request_tx_dma() ) {
		PHY_ERR("request tx dma fail!\n");
		return -1;
	} else
		PHY_DBG("request general tx dma channel ok!\n");

	if( 0 != spinand_request_rx_dma() ) {
		PHY_ERR("request rx dma fail!\n");
		return -1;
	} else
		PHY_DBG("request general rx dma channel ok!\n");

	//init clk
	NAND_ClkRequest(spi_no);

	NAND_SetClk(spi_no, 20, 20*2);

	rval = SPI_SOFT_RST|SPI_TXPAUSE_EN|SPI_MASTER|SPI_ENABLE;
	writew(rval, SPI_GCR);

	//set ss to high,discard unused burst,SPI select signal polarity(low,1=idle)
	rval = SPI_SET_SS_1|SPI_DHB|SPI_SS_ACTIVE0;
	writew(rval, SPI_TCR);


	writew(SPI_TXFIFO_RST|(SPI_TX_WL<<16)|(SPI_RX_WL), SPI_FCR);
	writew(SPI_ERROR_INT, SPI_IER);
	return 0;
}

__s32 Spic_exit(__u32 spi_no)
{
	__u32 rval;

	rval = readw(SPI_GCR);
	rval &= (~(SPI_SOFT_RST|SPI_MASTER|SPI_ENABLE));
	writew(rval, SPI_GCR);

	NAND_ClkRelease(spi_no);

	spinand_releasetxdma();
	spinand_releaserxdma();

	//init pin
	NAND_PIORelease(spi_no);

	//set ss to high,discard unused burst,SPI select signal polarity(low,1=idle)
	rval = SPI_SET_SS_1|SPI_DHB|SPI_SS_ACTIVE0;
	writew(rval, SPI_TCR);

	return 0;
}

void Spic_set_master_slave(__u32 spi_no, __u32 master)
{
	__u32 rval = readw(SPI_GCR)&(~(1 << 1));
	rval |= master << 1;
	writew(rval, SPI_GCR);
}

void Spic_sel_ss(__u32 spi_no, __u32 ssx)
{
	__u32 rval = readw(SPI_TCR)&(~(3 << 4));
	rval |= ssx << 4;
	writew(rval, SPI_TCR);
}

// add for aw1650
void Spic_set_transmit_LSB(__u32 spi_no, __u32 tmod)
{
	__u32 rval = readw(SPI_TCR)&(~(1 << 12));
	rval |= tmod << 12;
	writew(rval, SPI_TCR);
}

void Spic_set_ss_level(__u32 spi_no, __u32 level)
{
	__u32 rval = readw(SPI_TCR)&(~(1 << 7));
	rval |= level << 7;
	writew(rval, SPI_TCR);
}

void Spic_set_rapids(__u32 spi_no, __u32 rapids)
{
	__u32 rval = readw(SPI_TCR)&(~(1 << 10));
	rval |= rapids << 10;
	writew(rval, SPI_TCR);
}

void Spic_set_sample_mode(__u32 spi_no, __u32 smod)
{
	__u32 rval = readw(SPI_TCR)&(~(1 << 13));
	rval |= smod << 13;
	writew(rval, SPI_TCR);
}

void Spic_set_sample(__u32 spi_no, __u32 sample)
{
	__u32 rval = readw(SPI_TCR)&(~(1 << 11));
	rval |= sample << 11;
	writew(rval, SPI_TCR);
}

void Spic_set_trans_mode(__u32 spi_no, __u32 mode)
{
	__u32 rval = readw(SPI_TCR)&(~(3 << 0));
	rval |= mode << 0;
	writew(rval, SPI_TCR);
}

void Spic_set_wait_clk(__u32 spi_no, __u32 swc, __u32 wcc)
{
	writew((swc << 16) | (wcc), SPI_WCR);
}

void Spic_config_io_mode(__u32 spi_no, __u32 rxmode, __u32 dbc, __u32 stc)
{
	if (rxmode == 0)
		writew((dbc<<24)|(stc), SPI_BCC);
	else if (rxmode == 1)
		writew((1<<28)|(dbc<<24)|(stc), SPI_BCC);
	else if (rxmode == 2)
		writew((1<<29)|(dbc<<24)|(stc), SPI_BCC);
}

__s32 Spic_rw(__u32 spi_no, __u32 tcnt, __u8* txbuf, __u32 rcnt, __u8* rxbuf, __u32 dummy_cnt)
{
	__u32 i = 0,fcr;
	__u32 tx_dma_flag = 0;
	__u32 rx_dma_flag = 0;
	__s32 timeout = 0xffff;

	writew(0, SPI_IER);
	writew(0xffffffff, SPI_ISR);//clear status register

	writew(tcnt, SPI_MTC);
	writew(tcnt+rcnt+dummy_cnt, SPI_MBC);

	//read and write by cpu operation
	if(tcnt)
	{
		if(tcnt<64)
		{
			i = 0;
			while (i<tcnt)
			{
				//send data
				//while((readw(SPI_FSR)>>16)==SPI_FIFO_SIZE);
				if(((readw(SPI_FSR)>>16) & 0x7f)==SPI_FIFO_SIZE)
					PHY_ERR("TX FIFO size error!\n");
				writeb(*(txbuf+i),SPI_TXD);
				i++;
			}
		}
		else
		{
			tx_dma_flag = 1;

			writew((readw(SPI_FCR)|SPI_TXDMAREQ_EN), SPI_FCR);

			nand_dma_config_start(1, (__u32) txbuf, tcnt);
#if 0
			timeout = 0xffffff;
			 while(readw(DMA_IRQ_PEND_REG)&0x7 !=7)
			 {
				timeout--;
				if (!timeout)
					break;
			 }
			 if(timeout == 0)
			 {
				 PHY_ERR("TX DMA wait status timeout!\n");
				 return ERR_TIMEOUT;
			 }
#endif
		}
	}
	/* start transmit */
	writew(readw(SPI_TCR)|SPI_EXCHANGE, SPI_TCR);
	if(rcnt)
	{
		if(rcnt<64)
		{
			i = 0;
			#if 1
			timeout = 0xfffff;
			while(1)
			{
				if(((readw(SPI_FSR))&0x7f)==rcnt)
					break;
				if(timeout < 0)
				{
					PHY_ERR("RX FIFO size error,timeout!\n");
					break;
				}
				timeout--;
			}
			#endif
			while(i<rcnt)
			{
				//receive valid data
				//while(((readw(SPI_FSR))&0x7f)==0);
				//while((((readw(SPI_FSR))&0x7f)!=rcnt)||(timeout < 0))
				//	PHY_ERR("RX FIFO size error!\n");
				*(rxbuf+i)=readb(SPI_RXD);
				i++;
			}
		}
		else
		{
			rx_dma_flag = 1;

			writew((readw(SPI_FCR)|SPI_RXDMAREQ_EN), SPI_FCR);

			nand_dma_config_start(0, (__u32) rxbuf, rcnt);
#if 0
			timeout = 0xffffff;
			 while(readw(DMA_IRQ_PEND_REG)&0x7 !=7)
			 {
					timeout--;
					if (!timeout)
						break;
			 }
			 if(timeout == 0)
			 {
				 PHY_ERR("RX DMA wait status timeout!\n");
				 return ERR_TIMEOUT;
			 }

			 //while(readw(SPI_FSR)&0x7f);
			 if(readw(SPI_FSR)&0x7f)
			 {
				 PHY_ERR("RX FIFO not empty after DMA finish!\n");
			 }
#endif
		}
	}

	if(NAND_WaitDmaFinish(tx_dma_flag, rx_dma_flag))
	{
		PHY_ERR("DMA wait status timeout!\n");
		return ERR_TIMEOUT;
	}

	if(Wait_Tc_Complete())
	{
		PHY_ERR("wait tc complete timeout!\n");
		return ERR_TIMEOUT;
	}

	if(tx_dma_flag)
		Nand_Dma_End(1, (__u32) txbuf, tcnt);

	if(rx_dma_flag)
		Nand_Dma_End(0, (__u32) rxbuf, rcnt);

	fcr = readw(SPI_FCR);
	fcr &= ~(SPI_TXDMAREQ_EN|SPI_RXDMAREQ_EN);
	writew(fcr, SPI_FCR);
	if (readw(SPI_ISR) & (0xf << 8))	/* (1U << 11) | (1U << 10) | (1U << 9) | (1U << 8)) */
	{
		PHY_ERR("FIFO status error: 0x%x!\n",readw(SPI_ISR));
		return NAND_OP_FALSE;
	}

	if(readw(SPI_TCR)&SPI_EXCHANGE)
	{
		PHY_ERR("XCH Control Error!!\n");
	}

	writew(0xffffffff,SPI_ISR);  /* clear  flag */
	return NAND_OP_TRUE;
}

__s32 spi_nand_rdid(__u32 spi_no, __u32 chip, __u32 id_addr, __u32 addr_cnt, __u32 id_cnt, void* id)
{
	__s32 ret = NAND_OP_TRUE;
	__u8 sdata[2];
	__u32 txnum;
	__u32 rxnum;

	txnum = 1 + addr_cnt;
	rxnum = id_cnt;  //mira nand:rxnum=5

	sdata[0]=SPI_NAND_RDID;
	sdata[1]= id_addr;         //add 00H:Maunufation ID,01H:Device ID

	Spic_sel_ss(spi_no, chip);

	Spic_config_io_mode(spi_no, 0, 0, txnum);
	ret = Spic_rw(spi_no, txnum, (void*)sdata, rxnum, (void*)id, 0);
	return ret;
}
