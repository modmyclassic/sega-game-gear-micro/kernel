/*
************************************************************************************************************************
*                                                      eNand
*                                     Nand flash driver scan module define
*
*                             Copyright(C), 2008-2009, SoftWinners Microelectronic Co., Ltd.
*											       All Rights Reserved
*
* File Name : nand_scan.h
*
* Author : Kevin.z
*
* Version : v0.1
*
* Date : 2008.03.25
*
* Description : This file define the function __s32erface and some data structure export
*               for the nand flash scan module.
*
* Others : None at present.
*
*
* History :
*
*  <Author>        <time>       <version>      <description>
*
* Kevin.z         2008.03.25      0.1          build the file
*
************************************************************************************************************************
*/
#ifndef __NAND_SCAN_H__
#define __NAND_SCAN_H__

#include "nand_type_spinand.h"
#include "nand_physic.h"
#include "nand_physic_interface_spinand.h"
#include "../phy/phy.h"

//==============================================================================
//  define nand flash manufacture ID number
//==============================================================================

#define MICRON_NAND             0x2c                //Micron nand flash manufacture number
#define GD_NAND                 0xc8				//GD and MIRA nand flash manufacture number
#define ATO_NAND                0x9b				//ATO nand flash manufacture number
#define WINBOND_NAND			0xef				//winbond nand flash manufacture number
#define MXIC_NAND				0xc2				//mxic nand flash manufacture number
#define TOSHIBA_NAND			0x98                //toshiba nand flash manufacture number
#define ETRON_NAND				0xd5                //etron nand flash manufacture number
#define HeYang_NAND				0xc9                //HeYang nand flash manufacture number
#define Paragon_NAND				0xa1                //Paragon nand flash manufacture number
#define XTXTech_NAND				0x0b                //xtx tech nand flash manufacture number
#define DSTech_NAND				0xe5                //Dosilicon nand flash manufacture number
#define FORESEE_NAND			0xcd				//foresee nand flash manufacture number
#define ZETTA_NAND				0xba				//zetta tech nand flash manufacture number

//==============================================================================
//  define the function __s32erface for nand storage scan module
//==============================================================================

/*
************************************************************************************************************************
*                           ANALYZE NAND FLASH STORAGE SYSTEM
*
*Description: Analyze nand flash storage system, generate the nand flash physical
*             architecture parameter and connect information.
*
*Arguments  : none
*
*Return     : analyze result;
*               = 0     analyze successful;
*               < 0     analyze failed, can't recognize or some other error.
************************************************************************************************************************
*/
__s32  SCN_AnalyzeNandSystem(void);

__u32 NAND_GetValidBlkRatio(void);
__s32 NAND_SetValidBlkRatio(__u32 ValidBlkRatio);
__u32 NAND_GetFrequencePar(void);
__s32 NAND_SetFrequencePar(__u32 FrequencePar);
__u32 NAND_GetNandVersion(void);
//__s32 NAND_GetParam(boot_spinand_para_t * nand_param);

extern __u32 NAND_GetPlaneCnt(void);

#endif  //ifndef __NAND_SCAN_H__
