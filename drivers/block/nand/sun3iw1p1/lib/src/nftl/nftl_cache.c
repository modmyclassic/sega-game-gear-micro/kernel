
/*
 * allwinner nftl cache
 *
 * (C) 2008
 */

#define _NFTL_CACHE_C_

#include "nftl_inc.h"
#include "../nftl_interface/nftl_cfg.h"

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
int nftl_cache_init(struct _nftl_zone* zone)
{
    uint32 i;
    _cache* cache;

    cache = &zone->cache;

    cache->cache_totals = zone->cfg->nftl_max_cache_num;
    cache->cache_read_nums = 0;
    cache->cache_write_nums = 0;
    cache->cache_write_end_nums = 0;

    cache->cache_read_head.cache_read_next = NULL;
    cache->cache_read_head.cache_read_prev = NULL;
    cache->cache_read_head.cache_write_next = NULL;
    cache->cache_read_head.cache_write_prev = NULL;

    cache->cache_write_head.cache_read_next = NULL;
    cache->cache_write_head.cache_read_prev = NULL;
    cache->cache_write_head.cache_write_next = NULL;
    cache->cache_write_head.cache_write_prev = NULL;

    for(i=0; i<cache->cache_totals; i++)
    {
        cache->cache_node[i].cache_no = i;
        cache->cache_node[i].cache_info = CACHE_EMPTY;
        cache->cache_node[i].page_no = 0xffffffff;
        cache->cache_node[i].cache_read_next = NULL;
        cache->cache_node[i].cache_read_prev = NULL;
        cache->cache_node[i].cache_write_next = NULL;
        cache->cache_node[i].cache_write_prev = NULL;
        cache->cache_node[i].start_sector = 0;
        cache->cache_node[i].sector_len = 0;

        cache->cache_node[i].buf = nftl_malloc(zone->nand_chip->bytes_per_page);
        if(cache->cache_node[i].buf == NULL)
        {
            NFTL_ERR("[NE]%s:malloc fail for cache_node!\n",__func__);
            return NFTL_FAILURE;
        }
    }
    cache->cache_page_buf = nftl_malloc(zone->nand_chip->bytes_per_page);
    if(cache->cache_page_buf == NULL)
    {
    	NFTL_ERR("[NE]%s:malloc fail for cache_page_buf!\n",__func__);
        return NFTL_FAILURE;
    }

    return NFTL_SUCCESS;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
int nftl_cache_exit(struct _nftl_zone* zone)
{
    int i;
    _cache* cache;

    cache = &zone->cache;
    for(i=0; i<cache->cache_totals; i++)
    {
        nftl_free(cache->cache_node[i].buf);
    }
	nftl_free(cache->cache_page_buf);
    return NFTL_SUCCESS;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
int nftl_cache_clean(struct _nftl_zone* zone)
{
    uint32 i;
    _cache* cache;

    cache = &zone->cache;

    cache->cache_totals = zone->cfg->nftl_max_cache_num;
    cache->cache_read_nums = 0;
    cache->cache_write_nums = 0;
    cache->cache_write_end_nums = 0;

    cache->cache_read_head.cache_read_next = NULL;
    cache->cache_read_head.cache_read_prev = NULL;
    cache->cache_read_head.cache_write_next = NULL;
    cache->cache_read_head.cache_write_prev = NULL;

    cache->cache_write_head.cache_read_next = NULL;
    cache->cache_write_head.cache_read_prev = NULL;
    cache->cache_write_head.cache_write_next = NULL;
    cache->cache_write_head.cache_write_prev = NULL;

    for(i=0; i<cache->cache_totals; i++)
    {
        cache->cache_node[i].cache_no = i;
        cache->cache_node[i].cache_info = CACHE_EMPTY;
        cache->cache_node[i].page_no = 0xffffffff;
        cache->cache_node[i].cache_read_next = NULL;
        cache->cache_node[i].cache_read_prev = NULL;
        cache->cache_node[i].cache_write_next = NULL;
        cache->cache_node[i].cache_write_prev = NULL;
        cache->cache_node[i].start_sector = 0;
        cache->cache_node[i].sector_len = 0;
    }

    return NFTL_SUCCESS;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
_cache_node *search_from_cache_read_list(_cache* cache,uint32 page)
{
    _cache_node* p = &cache->cache_read_head;

    for(p=p->cache_read_next; p; p=p->cache_read_next)
    {
        if(p->page_no == page)
        {
            return p;
        }
    }
    return NULL;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
_cache_node *search_from_cache_write_list(_cache* cache,uint32 page)
{
    _cache_node * p = &cache->cache_write_head;

    for(p=p->cache_write_next; p; p=p->cache_write_next)
    {
        if(p->page_no == page)
        {
            return p;
        }
    }
    return NULL;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
int add_to_cache_read_list_tail(_cache *cache,_cache_node* cache_node,uint32 flag)
{
    _cache_node * p = &cache->cache_read_head;

    while(p->cache_read_next != NULL)
    {
        p = p->cache_read_next;
    }

    p->cache_read_next = cache_node;
    cache_node->cache_read_next = NULL;
    cache_node->cache_read_prev = p;
    cache_node->cache_info = flag;
    if(flag == CACHE_READ)
    {
        cache->cache_read_nums += 1;
    }
    else
    {
        cache->cache_write_end_nums += 1;
    }
    return NFTL_SUCCESS;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
int add_to_cache_write_list_tail(_cache *cache,_cache_node* cache_node)
{
    _cache_node * p = &cache->cache_write_head;

    while(p->cache_write_next != NULL)
    {
        p = p->cache_write_next;
    }

    p->cache_write_next = cache_node;
    cache_node->cache_write_next = NULL;
    cache_node->cache_write_prev = p;
    cache_node->cache_info = CACHE_WRITE;
    cache->cache_write_nums += 1;

    return NFTL_SUCCESS;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
_cache_node* del_from_cache_read_list(_cache *cache,_cache_node* cache_node)
{
    _cache_node * p = cache_node->cache_read_prev;

    p->cache_read_next = cache_node->cache_read_next;
    if(cache_node->cache_read_next != NULL)
        cache_node->cache_read_next->cache_read_prev = p;

    if(cache_node->cache_info == CACHE_READ)
    {
        cache->cache_read_nums -= 1;
    }
    else
    {
        cache->cache_write_end_nums -= 1;
    }
    cache_node->cache_info = CACHE_EMPTY;
    cache_node->cache_read_next = NULL;
    cache_node->cache_read_prev = NULL;
    return cache_node;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
_cache_node* del_from_cache_write_list(_cache *cache,_cache_node* cache_node)
{
    _cache_node * p = cache_node->cache_write_prev;
    p->cache_write_next = cache_node->cache_write_next;

    if(cache_node->cache_write_next != NULL)
        cache_node->cache_write_next->cache_write_prev = p;

    cache_node->cache_write_next = NULL;
    cache_node->cache_write_prev = NULL;
    cache_node->cache_info = CACHE_EMPTY;
	cache->cache_write_nums -= 1;

    return cache_node;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
_cache_node* get_empty_cahce_node(_cache *cache)
{
    uint16 i;
    _cache_node* node = NULL;

    for(i=0; i<cache->cache_totals; i++)
    {
        if(cache->cache_node[i].cache_info == CACHE_EMPTY)
            return &cache->cache_node[i];
    }

    if(cache->cache_write_end_nums >= MAX_CACHE_WRITE_END_NUM)
    {
        node = &cache->cache_read_head;
        for(node=node->cache_read_next; node; node=node->cache_read_next)
        {
            if(node->cache_info == CACHE_WRITE_END)
            {
                return del_from_cache_read_list(cache,node);
            }
        }
    }
    else
    {
        node = &cache->cache_read_head;
        for(node=node->cache_read_next; node; node=node->cache_read_next)
        {
            if(node->cache_info == CACHE_READ)
            {
                return del_from_cache_read_list(cache,node);
            }
        }
    }
    return NULL;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 __nand_read(struct _nftl_zone* zone,uint32 start_sector,uint32 len,uchar *buf)
{
    uint32 ret;
    if(zone->test == 3)
    {
        NFTL_DBG("[NE]read sector:0x%x,len:0x%x\n",start_sector,len);
    }

    zone->smart->total_recv_read_sectors += len;

    ret = nand_op(0,zone,start_sector,len,buf);

    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 __nand_write(struct _nftl_zone* zone,uint32 start_sector,uint32 len,uchar *buf)
{
    uint32 ret;

    if(zone->test == 3)
    {
        NFTL_DBG("[NE]write sector:0x%x,len:0x%x\n",start_sector,len);
    }

    zone->smart->total_recv_write_sectors += len;

    ret = nand_op(1,zone,start_sector,len,buf);
    return  ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 __nand_discard(struct _nftl_zone* zone,uint32 start_sector,uint32 len)
{
    uint32 ret;

    if(zone->test == 3)
        NFTL_DBG("[NE]dicard sector:0x%x,len:0x%x\n",start_sector,len);

    ret = nand_discard(zone,start_sector,len);
    return  ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 nand_op(uchar flag,struct _nftl_zone* zone,uint32 start_sector,uint32 len,uchar *buf)
{
    uint16  temp1,temp2,len_save,bitmap;
    uchar  *buf_save;
    uint32 page_save,page_no,ret;
    uint32 (*nand_op2)(struct _nftl_zone* zone,uint32 page_no,uint16 bitmap,uchar *buf);

    ret = 0;

    if(len == 0)
        return NFTL_SUCCESS;

    if((start_sector+len) > zone->logic_cap_in_sects)
    {
        NFTL_ERR("[NE]paramter error!\n");
        return NFTL_FAILURE;
    }

    if(flag == 0)
    {
//      NFTL_DBG("[NE]read start_sector :%d; len: %d; buf %x\n",start_sector,len,buf);
        nand_op2 = nand_cache_read;
    }
    else
    {
//      NFTL_DBG("[NE]write start_sector :%d; len: %d; buf %x\n",start_sector,len,buf);
        nand_op2 = nand_cache_write;
    }
//////////////////////////////////////////////////////////////
    page_no = start_sector / zone->nand_chip->sector_per_page;
    page_save = page_no;

    temp1 = (uint16)(start_sector % zone->nand_chip->sector_per_page);    //start sector
    temp2 = zone->nand_chip->sector_per_page - temp1;                    //sector len
    if(len <= temp2)
    {
        temp2 = len;
        len = 0;
    }
    else
    {
        len -= temp2;
    }

    buf_save = buf;
    len_save = temp2;
    bitmap = (temp1 << 8) | temp2;
//	NFTL_DBG("[NE]page :%d; bitmap: %x; buf %x\n",page_save,bitmap,buf_save);
    ret = nand_op2(zone,page_save,bitmap,buf_save);
    if(len == 0)
    {
        return ret;
    }

//////////////////////////////////////////////////////////////
    while(len > zone->nand_chip->sector_per_page)
    {
        temp2 = zone->nand_chip->sector_per_page;
        page_save += 1;
        buf_save += len_save << SHIFT_PER_SECTOR;
        bitmap = temp2;
        len -= zone->nand_chip->sector_per_page;
        len_save = zone->nand_chip->sector_per_page;
//        NFTL_DBG("[NE]page :%d; bitmap: %x; buf %x\n",page_save,bitmap,buf_save);
        ret |= nand_op2(zone,page_save,bitmap,buf_save);
    }

//////////////////////////////////////////////////////////////
    temp2 = (uint16)len;
    page_save += 1;
    buf_save += len_save << SHIFT_PER_SECTOR;
    bitmap = temp2;
//    NFTL_DBG("[NE]page :%d; bitmap: %x; buf %x\n",page_save,bitmap,buf_save);
    ret |= nand_op2(zone,page_save,bitmap,buf_save);
//////////////////////////////////////////////////////////////
    //NFTL_DBG("[NE]op end!\n");
    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 nand_cache_read(struct _nftl_zone* zone,uint32 page_no,uint16 bitmap,uchar *buf)
{
    uint32 ret;
    uint16 byte_start;
    uint16 byte_len;
    uint32 start,len;
    uchar* t_buf;
    _cache_node * node;
    _cache *cache;

    if(buf == NULL)
    {
        NFTL_DBG("[NE]nand_cache_read error  NULL!\n");
        return NFTL_SUCCESS;
    }

    ret = 0;
    start = bitmap >> 8;
    len = bitmap & 0x00ff;
    start <<= SHIFT_PER_SECTOR;
    len <<= SHIFT_PER_SECTOR;

    cache = &zone->cache;

    if(zone->cfg->nftl_dont_use_cache)
    {
        t_buf = cache->cache_node[0].buf;
        ret = zone->nftl_nand_read_logic_page(zone,page_no,t_buf);
        MEMCPY(buf,t_buf+start,len);
        if(ret == ECC_LIMIT)
        {
            //NFTL_DBG("[NE]ECC_LIMIT happened! page:%d!\n",page_no);
            zone->smart->total_recv_read_claim_pages++;
            zone->nftl_nand_write_logic_page(zone,page_no,t_buf);
            ret = NFTL_SUCCESS;
        }
        return ret;
    }

    node = search_from_cache_write_list(cache,page_no);
    if(node != NULL)
    {
        t_buf = node->buf;
        byte_start = node->start_sector << SHIFT_PER_SECTOR;
        byte_len = node->sector_len << SHIFT_PER_SECTOR;

        if(start <= (byte_start + byte_len))
        {
            if((start+len) <= (byte_start + byte_len))
            {
                MEMCPY(buf,t_buf+start,len);
                return NFTL_SUCCESS;
            }
        }

        ret = zone->nftl_nand_read_logic_page(zone,page_no,cache->cache_page_buf);
        MEMCPY(t_buf+byte_len,cache->cache_page_buf+byte_len,zone->nand_chip->bytes_per_page-byte_len);
        node->sector_len = zone->nand_chip->sector_per_page;
        MEMCPY(buf,t_buf+start,len);
        if(ret == ECC_LIMIT)
        {
            //NFTL_DBG("[NE]ECC_LIMIT happened 13! page:%d!\n",page_no);
        }
        return NFTL_SUCCESS;
    }

    node = search_from_cache_read_list(cache,page_no);
    if(node != NULL)
    {
		if(node->cache_info == CACHE_READ)
		{
			del_from_cache_read_list(cache,node);
	        t_buf = node->buf;
	        MEMCPY(buf,t_buf+start,len);
	        add_to_cache_read_list_tail(cache,node,CACHE_READ);
		}
		else
		{
			t_buf = node->buf;
	        MEMCPY(buf,t_buf+start,len);
		}
        return NFTL_SUCCESS;
    }

    node = get_empty_cahce_node(cache);
    if(node == NULL)
    {
        NFTL_ERR("[NE]error1 node %d,%d,%d!\n",cache->cache_write_nums,cache->cache_read_nums,cache->cache_write_end_nums);
        return NFTL_FAILURE;
    }

    node->page_no = page_no;
    t_buf = node->buf;
    ret = zone->nftl_nand_read_logic_page(zone,page_no,t_buf);
	node->start_sector = 0;
	node->sector_len = zone->nand_chip->sector_per_page;
    add_to_cache_read_list_tail(cache,node,CACHE_READ);
    MEMCPY(buf,t_buf+start,len);

    if(ret == ECC_LIMIT)
    {
        //NFTL_DBG("[NE]ECC_LIMIT happened! page:%d!\n",page_no);
        zone->smart->total_recv_read_claim_pages++;
        zone->nftl_nand_write_logic_page(zone,page_no,t_buf);
        ret = NFTL_SUCCESS;
    }
    //del_from_cache_read_list(cache,node);

    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 nand_cache_write(struct _nftl_zone* zone,uint32 page_no,uint16 bitmap,uchar *buf)
{
    uint32 ret;
    uint16 byte_start;
    uint16 byte_len;
    uint16 start,len;
    uchar* t_buf;
    _cache_node * node;
    _cache *cache;

    if(buf == NULL)
    {
        NFTL_DBG("[NE]nand_cache_write error  NULL!\n");
        return NFTL_SUCCESS;
    }

    start = bitmap >> 8;
    len = bitmap & 0x00ff;
    start <<= SHIFT_PER_SECTOR;
    len <<= SHIFT_PER_SECTOR;

    ret = 0;
    cache = &zone->cache;

    if(zone->cfg->nftl_dont_use_cache)
    {
        t_buf = cache->cache_node[0].buf;
        zone->nftl_nand_read_logic_page(zone,page_no,t_buf);
        MEMCPY(t_buf+start,buf,len);
        ret = zone->nftl_nand_write_logic_page(zone,page_no,t_buf);
        return ret;
    }

// case 1
    if((!zone->cfg->nftl_use_cache_sort)&&(bitmap == zone->nand_chip->sector_per_page)&&(!zone->cfg->nftl_cross_talk))
    {
        node = search_from_cache_write_list(cache,page_no);
        if(node != NULL)
        {
            del_from_cache_write_list(cache,node);
        }

        node = search_from_cache_read_list(cache,page_no);
        if(node != NULL)
        {
            del_from_cache_read_list(cache,node);
        }

        ret = zone->nftl_nand_write_logic_page(zone,page_no,buf);
        if(ret != 0)
        {
            NFTL_ERR("[NE]error1 bitmap %x!\n",bitmap);
            return NFTL_FAILURE;
        }
        else
        {
            return NFTL_SUCCESS;
        }
    }

// case 2
    node = search_from_cache_write_list(cache,page_no);
    if(node != NULL)
    {
        del_from_cache_write_list(cache,node);
        t_buf = node->buf;
        byte_start = node->start_sector << SHIFT_PER_SECTOR;
        byte_len = node->sector_len << SHIFT_PER_SECTOR;
		if(node->sector_len == zone->nand_chip->sector_per_page)
		{
			;
		}
        else if(start == byte_len)
        {
            node->sector_len += len >> SHIFT_PER_SECTOR;
        }
        else
        {
            ret = zone->nftl_nand_read_logic_page(zone,page_no,cache->cache_page_buf);
            MEMCPY(t_buf+byte_len,cache->cache_page_buf+byte_len,zone->nand_chip->bytes_per_page-byte_len);
            node->sector_len = zone->nand_chip->sector_per_page;
            if(ret == ECC_LIMIT)
            {
                NFTL_DBG("[NE]ECC_LIMIT happened 12! page:%d!\n",page_no);
            }
        }
        MEMCPY(t_buf+start,buf,len);
        add_to_cache_write_list_tail(cache,node);

        node = search_from_cache_read_list(cache,page_no);
        if(node != NULL)
        {
            del_from_cache_read_list(cache,node);
        }
        //nand_flush_write_cache(zone);
        return NFTL_SUCCESS;
    }

// case 3
    node = search_from_cache_read_list(cache,page_no);
    if(node != NULL)
    {
        t_buf = node->buf;
        MEMCPY(t_buf+start,buf,len);
		node->start_sector = 0;
        node->sector_len = zone->nand_chip->sector_per_page;
        del_from_cache_read_list(cache,node);
        add_to_cache_write_list_tail(cache,node);

        if(cache->cache_write_nums >= zone->cfg->nftl_max_cache_write_num)
        {
            if(flush_write_cache_to_nand(zone) != 0)
            {
                NFTL_ERR("[NE]error1 flush write cache to nand\n");
                return NFTL_FAILURE;
            }
        }
        return NFTL_SUCCESS;
    }

// case 4
    while(cache->cache_write_nums >= zone->cfg->nftl_max_cache_write_num)
    {
        if(flush_write_cache_to_nand(zone) != 0)
        {
            NFTL_ERR("[NE]error2 flush write cache to nand\n");
            return NFTL_FAILURE;
        }
    }

    node = get_empty_cahce_node(cache);
    if(node == NULL)
    {
        NFTL_ERR("[NE]error1 node %d,%d,%d!\n",cache->cache_write_nums,cache->cache_read_nums,cache->cache_write_end_nums);
        return NFTL_FAILURE;
    }

    node->page_no = page_no;
    t_buf = node->buf;
    if(start == 0)
    {
        node->start_sector = 0;
        node->sector_len = len>>SHIFT_PER_SECTOR;
    }
    else
    {
        ret = zone->nftl_nand_read_logic_page(zone,page_no,t_buf);
//        if(ret == ERR_ECC)
//        {
//            NFTL_ERR("[NE]error3 bitmap %x!\n",bitmap);
//            return NFTL_FAILURE;
//        }

        if(ret == ECC_LIMIT)
        {
            NFTL_DBG("[NE]ECC_LIMIT happened 11! page:%d!\n",page_no);
        }

        node->start_sector = 0;
        node->sector_len = zone->nand_chip->sector_per_page;
    }

    MEMCPY(t_buf+start,buf,len);
    add_to_cache_write_list_tail(cache,node);
    //nand_flush_write_cache(zone);

    return NFTL_SUCCESS;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 flush_write_cache_to_nand(struct _nftl_zone* zone)
{
    uint32 ret;
    uint16 byte_len;
    uchar* t_buf;
    _cache_node * node;
    _cache *cache;

    ret = 0;
    cache = &zone->cache;

    if(cache->cache_write_head.cache_write_next == NULL)
    {
        NFTL_DBG("[NE]flush write cache to nand no cache!\n");
        return NFTL_SUCCESS;
    }

    node = del_from_cache_write_list(cache,cache->cache_write_head.cache_write_next);
    if(node == NULL)
    {
        return NFTL_SUCCESS;
    }

    t_buf = node->buf;
    if(node->sector_len != zone->nand_chip->sector_per_page)
    {
        ret = zone->nftl_nand_read_logic_page(zone,node->page_no,cache->cache_page_buf);
        byte_len = node->sector_len << SHIFT_PER_SECTOR;
        MEMCPY(t_buf+byte_len,cache->cache_page_buf+byte_len,zone->nand_chip->bytes_per_page-byte_len);
        if(ret == ECC_LIMIT)
        {
            NFTL_DBG("[NE]ECC_LIMIT happened 10! page:%d cache: %d!\n",node->page_no,cache->cache_write_end_nums);
        }
    }

    node->start_sector = 0;
	node->sector_len = zone->nand_chip->sector_per_page;
    ret = zone->nftl_nand_write_logic_page(zone,node->page_no,t_buf);
    if(ret != 0)
    {
        NFTL_DBG("[NE]flush write cache to nand error!\n");
        //return NFTL_FAILURE;
    }

    add_to_cache_read_list_tail(cache,node,CACHE_WRITE_END);

    return NFTL_SUCCESS;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 __nand_flush_write_cache(struct _nftl_zone* zone,uint32 num)
{
    uint32 ret,write_nums = 0;
    _cache *cache;

    cache = &zone->cache;

    ret = 0;

    while(cache->cache_write_head.cache_write_next != NULL)
    {
        write_nums++;
        ret |= flush_write_cache_to_nand(zone);
        num--;
        if(num == 0)
        {
            break;
        }
    }

    if(ret != 0)
    {
        NFTL_ERR("[NE]nand_flush_write_cache write error\n");
    }

    if(write_nums != 0)
    {
        if(zone->test  != 0)
        {
            NFTL_DBG("[ND]flush cache %d %d!\n",zone->zone_no,write_nums);
        }
    }

    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 nand_discard(struct _nftl_zone* zone,uint32 start_sector,uint32 len)
{
    uint16  temp1,temp2;
    uint32 page_no,ret;
    _cache_node * node;
    _cache *cache;

    ret = 0;
    cache = &zone->cache;

    if((start_sector+len) > zone->logic_cap_in_sects)
    {
        NFTL_ERR("[NE]nand_discard paramter error!\n");
        return NFTL_FAILURE;
    }

    zone->smart->total_recv_discard_sectors += len;

    page_no = start_sector / zone->nand_chip->sector_per_page;
    temp1 = (uint16)(start_sector % zone->nand_chip->sector_per_page);    //start sector
    temp2 = zone->nand_chip->sector_per_page - temp1;                    //sector len

    if(temp1 != 0)
    {
        page_no++;
		if(len < temp2)
		{
			len = 0;
		}
		else
		{
			len -= temp2;
		}
    }
	else
    {
		if(len < zone->nand_chip->sector_per_page)
		{
			len = 0;
		}
	}

    len /= zone->nand_chip->sector_per_page;
    if(len == 0)
    {
        return NFTL_SUCCESS;
    }

    while(len > 0)
    {
        if(zone->cfg->nftl_dont_use_cache == 0)
        {
            node = search_from_cache_write_list(cache,page_no);
            if(node != NULL)
            {
                del_from_cache_write_list(cache,node);
            }

            node = search_from_cache_read_list(cache,page_no);
            if(node != NULL)
            {
                del_from_cache_read_list(cache,node);
            }
        }
        zone->nftl_nand_discard_logic_page(zone,page_no);
        len -= 1;
        page_no++;
        zone->smart->total_real_discard_sectors += zone->nand_chip->sector_per_page;
    }

    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 __shutdown_op(struct _nftl_zone* zone)
{
    uint32 ret = 0;

//    prio_gc_all(zone);
//
//    if((zone->cfg->nftl_support_fill_block == 0) || (zone->current_block_used_page == 0))
//    {
//        goto  retl;
//    }
//
//    if(zone->current_block_used_page == (zone->nand_chip->pages_per_blk -1))
//    {
//        NFTL_DBG("[ND]shutdown0 zone no:%d block:%d,page: %d!\n",zone->zone_no,zone->zone_current_used_block->phy_block.Block_NO,zone->current_block_used_page);
//        if(write_phy_page_map_to_current_block(zone) != NFTL_SUCCESS)
//        {
//            ret = 1;
//            goto  retl_fail;
//        }
//        if(get_new_current_block(zone) != NFTL_SUCCESS)
//        {
//            ret = 2;
//            goto  retl_fail;
//        }
//        NFTL_DBG("[ND]shutdown end!\n");
//        goto  retl;
//    }
//
//    NFTL_DBG("[ND]shutdown1 zone no:%d block:%d,page: %d!\n",zone->zone_no,zone->zone_current_used_block->phy_block.Block_NO,zone->current_block_used_page);
//    while(zone->current_block_used_page != (zone->nand_chip->pages_per_blk -1))
//    {
////        nand_write_logic_page_no_gc(zone,page_no,node->buf);
//        write_phy_page_map_to_current_block(zone);
//    }
//
//    if(zone->current_block_used_page == (zone->nand_chip->pages_per_blk -1))
//    {
//        NFTL_DBG("[ND]shutdown1 zone no:%d block:%d,page: %d!\n",zone->zone_no,zone->zone_current_used_block->phy_block.Block_NO,zone->current_block_used_page);
//        if(write_phy_page_map_to_current_block(zone) != NFTL_SUCCESS)
//        {
//            ret = 3;
//            goto  retl_fail;
//        }
//        if(get_new_current_block(zone) != NFTL_SUCCESS)
//        {
//            ret = 4;
//            goto  retl_fail;
//        }
//        NFTL_DBG("[ND]shutdown end!\n");
//        goto  retl;
//    }
//
//retl_fail:
//    NFTL_ERR("[NE]shutdown error!\n");
//retl:
    return ret;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////

uint32 print_logic_page_map(struct _nftl_zone *zone,uint32 start_page)
{
    uint32 total_pages,i;
    _mapping_page* page_map;

    total_pages = zone->logic_cap_in_sects / zone->nand_chip->sector_per_page;
    for(i=0; i<total_pages; i++)
    {
        page_map = get_logic_page_map(zone,i);
        if(page_map->Block_NO != 0xffff)
        {
            NFTL_DBG("%4d:<%4d %3d><%d>\n",i,page_map->Block_NO,page_map->Page_NO,page_map->Read_flag);
        }
    }
    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
struct _nftl_blk* get_last_nftl(struct _nftl_blk* start_blk)
{
    struct _nftl_blk *nftl_blk = start_blk;
    struct _nftl_blk *last_nftl_blk = start_blk;

    while(nftl_blk != NULL)
    {
        last_nftl_blk = nftl_blk;
        nftl_blk = nftl_blk->nftl_blk_next;
    }

    return last_nftl_blk;
}


/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
struct _nftl_blk* get_nftl_need_read_claim(struct _nftl_blk* start_blk)
{
    struct _nftl_blk *nftl_blk = start_blk;
    struct _nftl_blk *last_nftl_blk;
    uint32 zone_no = 0;

    last_nftl_blk = get_last_nftl(start_blk);

    zone_no = last_nftl_blk->nftl_zone->smart->read_reclaim_zone_no;

    nftl_blk = start_blk;

    while(nftl_blk != NULL)
    {
        if(nftl_blk->nftl_zone->zone_no == zone_no)
        {
            break;
        }
        nftl_blk = nftl_blk->nftl_blk_next;
    }

    return nftl_blk;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 get_nftl_read_claim_page_no(struct _nftl_blk* start_blk)
{
    struct _nftl_blk *last_nftl_blk;

    last_nftl_blk = get_last_nftl(start_blk);

   return last_nftl_blk->nftl_zone->smart->read_reclaim_page_no;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
void clean_read_reclaim_complete_flag(struct _nftl_blk* start_blk)
{
    struct _nftl_blk *nftl_blk = start_blk;

    while(nftl_blk != NULL)
    {
        nftl_blk->nftl_zone->already_read_flag = !nftl_blk->nftl_zone->already_read_flag;
        nftl_blk = nftl_blk->nftl_blk_next;
    }
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 set_nftl_read_claim_complete(struct _nftl_blk* start_blk,struct _nftl_blk* nftl_blk)
{
    struct _nftl_blk *last_nftl_blk;

    nftl_blk->nftl_zone->read_reclaim_complete = 1;

    last_nftl_blk = get_last_nftl(start_blk);
    if(nftl_blk->nftl_blk_next == NULL)   //last nftl
    {
        last_nftl_blk->nftl_zone->smart->read_reclaim_zone_no = 0;
        clean_read_reclaim_complete_flag(start_blk);
    }
    else
    {
        last_nftl_blk->nftl_zone->smart->read_reclaim_zone_no = nftl_blk->nftl_zone->zone_no+1;
    }

    last_nftl_blk->nftl_zone->smart->read_reclaim_page_no = 0;


    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 set_nftl_read_claim_flag(struct _nftl_blk* start_blk,uint32 zone_no,uint32 page_no)
{
     struct _nftl_blk *nftl_blk = start_blk;
    while(nftl_blk != NULL)
    {
        nftl_blk->nftl_zone->smart->read_reclaim_zone_no = zone_no;
        nftl_blk->nftl_zone->smart->read_reclaim_page_no = page_no;
        nftl_blk = nftl_blk->nftl_blk_next;
    }
    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :
*Note         :
*****************************************************************************/
uint32 get_cold_logic_page(struct _nftl_zone *zone,uint32 start_page,uint32 total_pages)
{
    uint32 i;
    _mapping_page* page_map;

    for(i=start_page; i<total_pages; i++)
    {
        page_map = get_logic_page_map(zone,i);
        if((page_map->Block_NO != 0xffff) && (page_map->Read_flag != zone->already_read_flag))
        {
            page_map->Read_flag = zone->already_read_flag;
            return i;
        }
    }
    return 0xffffffff;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       :1:read all page complete  0:read a page ok
*Note         :
*****************************************************************************/
int read_reclaim(struct _nftl_blk *start_blk,struct _nftl_blk *nftl_blk,uchar*buf)
{
    uint32 st_start_page,page,ret = 0;

    st_start_page = get_nftl_read_claim_page_no(start_blk);

    page = get_cold_logic_page(nftl_blk->nftl_zone,st_start_page,nftl_blk->nftl_zone->logic_cap_in_page);
    if(page == 0xffffffff)
    {
        ret = set_nftl_read_claim_complete(start_blk,nftl_blk);
    }
    else
    {
//		NFTL_DBG("%d rc:%d\n",nftl_blk->nftl_zone->zone_no,page);
        nand_cache_read(nftl_blk->nftl_zone,page,nftl_blk->nftl_zone->nand_chip->sector_per_page,buf);
        set_nftl_read_claim_flag(start_blk,nftl_blk->nftl_zone->zone_no,page);
    }
    return ret;
}
