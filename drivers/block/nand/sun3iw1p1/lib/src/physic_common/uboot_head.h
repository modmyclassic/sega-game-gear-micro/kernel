#ifndef UBOOT_HEAD
#define UBOOT_HEAD


#define TOC_MAIN_INFO_MAGIC		0x89119800
#define PHY_INFO_SIZE			0x8000
#define PHY_INFO_MAGIC			0xaa55a5a5
#define UBOOT_STAMP_VALUE		0xAE15BC34

#define TOC_LEN_OFFSET_BY_INT     9
#define TOC_MAGIC_OFFSET_BY_INT   4


#define NAND_BOOT0_BLK_START            0
#define NAND_BOOT0_BLK_CNT              2
#define NAND_UBOOT_BLK_START            (NAND_BOOT0_BLK_START+NAND_BOOT0_BLK_CNT)


#define NAND_UBOOT_BLK_CNT              6
#define NAND_BOOT0_PAGE_CNT_PER_COPY    64
#define NAND_BOOT0_PAGE_CNT_PER_COPY_2 128
#define BOOT0_MAX_COPY_CNT				(8)

#define UBOOT_SCAN_START_BLOCK		    4

#define UBOOT_START_BLOCK_BIGNAND		    4
#define UBOOT_START_BLOCK_SMALLNAND		    8
#define UBOOT_MAX_BLOCK_NUM			50

#define  PHYSIC_RECV_BLOCK          6

struct _uboot_info
{
	unsigned int  sys_mode;
	unsigned int  use_lsb_page;
	unsigned int  copys;

	unsigned int  uboot_len;
	unsigned int  total_len;
	unsigned int  uboot_pages;
	unsigned int  total_pages;

	unsigned int  blocks_per_total;
	unsigned int  page_offset_for_nand_info;
	unsigned int  byte_offset_for_nand_info;
	unsigned char  uboot_block[120];

	unsigned int  nboot_copys;
	unsigned int  nboot_len;
	unsigned int  nboot_data_per_page;   //43
	
	unsigned int  nouse[64-43];
};

#endif

