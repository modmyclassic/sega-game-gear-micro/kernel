/*
************************************************************************************************************************
*                                                      eNand
*                                           Nand flash driver scan module
*
*                             Copyright(C), 2008-2009, SoftWinners Microelectronic Co., Ltd.
*                                                  All Rights Reserved
*
* File Name : nand_chip_for_boot.c
*
* Author :
*
* Version : v0.1
*
* Date : 2013-11-20
*
* Description :
*
* Others : None at present.
*
*
*
************************************************************************************************************************
*/
#define _NCFRR3_C_

#include "../nand_physic_inc.h"

u8 m3_read_retry_mode = 0;
u8 m3_read_retry_cycle = 0;
u8 m3_read_retry_reg_num = 0;
u8 m3_read_retry_reg_adr[4] = {0};
s16 m3_read_retry_val[7][4] = {{0},{0}};

u8 m3_lsb_mode_reg_adr[5] = {0};
u8 m3_lsb_mode_default_val[5] = {0};
u8 m3_lsb_mode_val[5] = {0};
u8 m3_lsb_mode_reg_num = 0;

const s16 para0[7][4] = {	{0x00,  0x00,  0x00,  0x00},
							{0x00,  0x06,  0x0A,  0x06},
    						{0x00, -0x03, -0x07, -0x08},
    						{0x00, -0x06, -0x0D, -0x0F},
    						{0x00, -0x0B, -0x14, -0x17},
    						{0x00,  0x00, -0x1A, -0x1E},
    						{0x00,  0x00, -0x20, -0x25}
					};
const s16 para1[7][4] = {	{0x00,  0x00,  0x00,  0x00},
							{0x00,  0x06,  0x0a,  0x06},
    						{0x00, -0x03, -0x07, -0x08},
    						{0x00, -0x06, -0x0d, -0x0f},
    						{0x00, -0x09, -0x14, -0x17},
    						{0x00,  0x00, -0x1a, -0x1e},
    						{0x00,  0x00, -0x20, -0x25}
					};

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m3_read_page_end(struct _nand_physic_op_par* npo)
{
	int i=0, ret = 0;
    struct _nand_chip_info *nci =  nci_get_from_nsi(g_nsi, npo->chip);

    ret = m0_read_page_end_not_retry(npo);
    if(ret == ERR_ECC)
	{
	    PHY_DBG("m0 retry!\n");
	    for(i=0; i<m3_read_retry_cycle; i++)
	    {
	        ret = 0;
	        nci->retry_count ++;
	        if(nci->retry_count > m3_read_retry_cycle)
	        {
	            nci->retry_count = 0;
	        }

	        ret = m3_set_readretry(nci);
	        if(ret != 0)
	        {
                nci->retry_count = 0;
	            m3_set_readretry(nci);
	            break;
	        }

	        ret = m0_read_page_start(npo);
	        ret |= m0_read_page_end_not_retry(npo);
	        if((ret == ECC_LIMIT) || (ret == 0))
	        {
			    ret = ECC_LIMIT;
	            PHY_DBG("m3 ReadRetry ok! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
	            break;
	        }
	    }

		nci->retry_count = 0;
	    m3_set_readretry(nci);
	}
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m3_vender_get_param(struct _nand_chip_info *nci, u8 *para, u8 *addr, u32 count)
{
    u32 i;
    s32 ret = 0;
    u8 cmd_r;

    cmd_r = 0x37;
    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

//    for (i=0; i<count; i++)
//	{
//    	*nci->nctri->nreg.reg_byte_cnt = 1;
//		_set_addr(nci->nctri, 1, &addr[i]);
//		cfg = cmd_r;
//		cfg |= (NDFC_SEND_ADR | NDFC_DATA_TRANS | NDFC_SEND_CMD1 );
//		ret = ndfc_wait_cmdfifo_free(nci->nctri);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ret = ndfc_wait_cmd_finish(nci->nctri);
//		*(para+i) = *((u8 *)nci->nctri->nreg.reg_ram0_base);
//	}

    for (i=0; i<count; i++)
	{
        get_data_with_nand_bus_one_cmd(nci,&cmd_r,addr+i,para+i,1);
	}

    nand_disable_chip(nci);
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m3_vender_set_param(struct _nand_chip_info *nci, u8 *para, u8 *addr, u32 count)
{
    u32 i;
    u8 cmd_w, cmd_end;
    s32 ret = 0;

    cmd_w = 0x36;
    cmd_end = 0x16;

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

    for(i=0; i<count; i++)
	{
//		*nci->nctri->nreg.reg_byte_cnt = 1;
//		*nci->nctri->nreg.reg_ram0_base = para[i];
//		_set_addr(nci->nctri, 1, &addr[i]);
//		cfg = cmd_w;
//		cfg |= (NDFC_SEND_ADR | NDFC_DATA_TRANS | NDFC_ACCESS_DIR | NDFC_SEND_CMD1 | NDFC_WAIT_FLAG);
//		ret = ndfc_wait_cmdfifo_free(nci->nctri);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ret = ndfc_wait_cmd_finish(nci->nctri);
//
//		/*set NFC_REG_CMD*/
//		cfg = cmd_end;
//		cfg |= ( NDFC_SEND_CMD1);
//		ret = ndfc_wait_cmdfifo_free(nci->nctri);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ret = ndfc_wait_cmd_finish(nci->nctri);


		set_cmd_with_nand_bus(nci,&cmd_w,0,&addr[i],para+i,1,1);
		set_one_cmd(nci,cmd_end,0);
	}
    nand_disable_chip(nci);
    PHY_DBG("rr value %x %x %x %x!\n",para[0],para[1],para[2],para[3]);
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m3_lsb_init(struct _nand_chip_info *nci)
{
	//init
	m3_read_retry_mode = ((nci->npi->read_retry_type)>>16)&0xff;
	m3_read_retry_cycle =((nci->npi->read_retry_type)>>8)&0xff;
	m3_read_retry_reg_num = ((nci->npi->read_retry_type)>>0)&0xff;

    //set lsb mode
    m3_lsb_mode_reg_num = 5;

    m3_lsb_mode_reg_adr[0] = 0xa4;
    m3_lsb_mode_reg_adr[1] = 0xa5;
    m3_lsb_mode_reg_adr[2] = 0xb0;
    m3_lsb_mode_reg_adr[3] = 0xb1;
    m3_lsb_mode_reg_adr[4] = 0xc9;

    m3_lsb_mode_val[0] = 0x25;
    m3_lsb_mode_val[1] = 0x25;
    m3_lsb_mode_val[2] = 0x25;
    m3_lsb_mode_val[3] = 0x25;
    m3_lsb_mode_val[4] = 0x1;

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m3_lsb_enable(struct _nand_chip_info *nci)
{
    u32 i;
    u8 value[5];

    m3_vender_get_param(nci,&m3_lsb_mode_default_val[0], &m3_lsb_mode_reg_adr[0], m3_lsb_mode_reg_num);

    for(i=0;i<m3_lsb_mode_reg_num;i++)
    {
    	value[i] = m3_lsb_mode_default_val[i];
    	value[i] += m3_lsb_mode_val[i];
    }

    m3_vender_set_param(nci,&value[0], &m3_lsb_mode_reg_adr[0], m3_lsb_mode_reg_num);

    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m3_lsb_disable(struct _nand_chip_info *nci)
{
	m3_vender_set_param(nci,&m3_lsb_mode_default_val[0], &m3_lsb_mode_reg_adr[0], m3_lsb_mode_reg_num);

    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m3_lsb_exit(struct _nand_chip_info *nci)
{
	s32 timeout = 0xfffff;

	nand_enable_chip(nci);
	ndfc_disable_randomize(nci->nctri);

//    cfg = 0;
//    _set_addr(nci->nctri, 5, &addr[0]);
//    cfg = (NDFC_SEND_CMD1 | NDFC_SEND_ADR |(4<<16) | 0x00);
//    ndfc_wait_cmdfifo_free(nci->nctri);
//    *nci->nctri->nreg.reg_cmd = cfg;
//    ndfc_wait_cmd_finish(nci->nctri);
//
//    /*set 0x30*/
//    cfg = 0;
//    cfg = (NDFC_SEND_CMD1 | NDFC_WAIT_FLAG | 0x30);
//    ndfc_wait_cmdfifo_free(nci->nctri);
//    *nci->nctri->nreg.reg_cmd = cfg;
//    ndfc_wait_cmd_finish(nci->nctri);

    //dumy read
    set_one_cmd(nci,0x00,0);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_cmd(nci,0x30,1);

    while(timeout-- > 0) ;
    PHY_DBG("m3_lsb_exit\n");

    nand_disable_chip(nci);

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m3_getdefaultparam(struct _nand_chip_info *nci, u8* default_value)
{
    s32 i,ret = 0;

    ret = m3_vender_get_param(nci, &nci->readretry_value[0], &m3_read_retry_reg_adr[0], m3_read_retry_reg_num);
    for(i=0; i<m3_read_retry_reg_num; i++)
    {
        default_value[i] = nci->readretry_value[i];
    }

    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m3_setdefaultparam(struct _nand_chip_info *nci)
{
    s32 i,ret = 0;
	u8 default_value[8];

    for(i=0; i<m3_read_retry_reg_num; i++)
    {
    	default_value[i] = nci->readretry_value[i];
    }
    ret =m3_vender_set_param(nci, (u8 *)&default_value[0], (u8 *)&m3_read_retry_reg_adr[0], m3_read_retry_reg_num);

    PHY_DBG("set retry default value: ");
    for(i=0;i<m3_read_retry_reg_num;i++)
    {
    	PHY_DBG(" %x",default_value[i]);
    }
    PHY_DBG("\n");

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m3_get_default_param(struct _nand_chip_info *nci)
{
	u8 default_value[4];

    m3_getdefaultparam(nci,(u8 *)&default_value);
    PHY_DBG("m3_get_default_param: ch: %d, chip: %d, value: 0x%x 0x%x 0x%x 0x%x \n",nci->nctri->channel_id, nci->nctri_chip_no, default_value[0], default_value[1], default_value[2], default_value[3]);
    m3_setdefaultparam(nci);

    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m3_readretry_init(struct _nand_chip_info *nci)
{
	u32 i,j;
	//init
	m3_read_retry_mode = ((nci->npi->read_retry_type)>>16)&0xff;
	m3_read_retry_cycle =((nci->npi->read_retry_type)>>8)&0xff;
	m3_read_retry_reg_num = ((nci->npi->read_retry_type)>>0)&0xff;

	if(0x0 == m3_read_retry_mode)  //mode0  H27UCG8T2MYR
	{
		m3_read_retry_reg_adr[0] = 0xAC;
		m3_read_retry_reg_adr[1] = 0xAD;
		m3_read_retry_reg_adr[2] = 0xAE;
		m3_read_retry_reg_adr[3] = 0xAF;

		//set read retry level
		for(i=0;i<(m3_read_retry_cycle + 1);i++)
		{
			for(j=0; j<m3_read_retry_reg_num;j++)
			{
				m3_read_retry_val[i][j] = para0[i][j];
			}
		}
	}
	else if(0x1 == m3_read_retry_mode) //mode1  H27UBG8T2BTR
	{
		m3_read_retry_reg_adr[0] = 0xA7;
		m3_read_retry_reg_adr[1] = 0xAD;
		m3_read_retry_reg_adr[2] = 0xAE;
		m3_read_retry_reg_adr[3] = 0xAF;

		//set read retry level
		for(i=0;i<(m3_read_retry_cycle + 1);i++)
		{
			for(j=0; j<m3_read_retry_reg_num;j++)
			{
				m3_read_retry_val[i][j] = para1[i][j];
			}
		}
	}
	else
	{
		PHY_ERR("NFC_ReadRetryInit, unknown read retry mode 0x%x\n", m3_read_retry_mode);
		return ERR_NO_54;
	}

	nci->retry_count = 0;
	for(i=0;i<128;i++)
	{
		nci->readretry_value[i] = 0;
	}

	m3_get_default_param(nci);

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m3_readretry_exit(struct _nand_chip_info *nci)
{

	PHY_DBG("m3_readretry_exit. \n");

	m3_setdefaultparam(nci);

	nci->retry_count = 0;

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m3_set_readretry(struct _nand_chip_info *nci)
{
    u32 i;
    s32 ret=0;
    u8 param[4];
    s16 temp_val;
    s16 temp_val2;

	if(nci->retry_count > m3_read_retry_cycle)
	{
		return ERR_NO_53;
	}

    for(i=0; i<m3_read_retry_reg_num; i++)
    {
        temp_val2 = nci->readretry_value[i];
    	temp_val = (temp_val2 + m3_read_retry_val[nci->retry_count][i]);
    	if(temp_val > 255)
    	{
    		temp_val = 0xff;
    	}
    	else if(temp_val <0)
    	{
    		temp_val = 0;
    	}
    	else
    	{
    		temp_val &= 0xff;
    	}

    	param[i] = (u8)temp_val;
    }

    //fix 0
    if((nci->retry_count >=2)&&(nci->retry_count<=6))
    {
    	param[0] = 0;
    }

    if((nci->retry_count == 5)||(nci->retry_count == 6))
    {
    	param[1] = 0;
    }

    ret =m3_vender_set_param(nci,&param[0], &m3_read_retry_reg_adr[0], m3_read_retry_reg_num);

    PHY_DBG("m3 retry param:");
    for(i=0; i<m3_read_retry_reg_num; i++)
    {
    	PHY_DBG(" %x ", param[i]);
    }
    PHY_DBG("\n");

#if 0
    u8 param_debug[4];
    m3_vender_get_param(nci, &param_debug[0], &m3_read_retry_reg_adr[0], m3_read_retry_reg_num);
    PHY_DBG("rr_para_debug:");
    for(i=0; i<m3_read_retry_reg_num; i++)
    {
    	PHY_DBG(" %x ", param_debug[i]);
    }
    PHY_DBG("\n");
#endif

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m3_special_init(void)
{
    int ret = 0;
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
        ret |= m3_readretry_init(nci);
		nci = nci->nsi_next;
	}

	if(ret ==0)
	{
	    function_read_page_end = m3_read_page_end;
        PHY_DBG(" m3_special_init m3_read_retry_mode: %d m3_read_retry_cycle:%d m3_read_retry_reg_num:%d\n",m3_read_retry_mode,m3_read_retry_cycle,m3_read_retry_reg_num);
    }
	else
	{
	    PHY_ERR(" m3_special_init error m3_read_retry_mode: %d m3_read_retry_cycle:%d m3_read_retry_reg_num:%d\n",m3_read_retry_mode,m3_read_retry_cycle,m3_read_retry_reg_num);
	}
    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m3_special_exit(void)
{
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
		m3_readretry_exit(nci);
		nci = nci->nsi_next;
	}
	PHY_DBG(" m3_special_exit \n");
    return 0;
}

int m3_is_lsb_page(__u32 page_num)
{	
	struct _nand_chip_info *nci = g_nsi->nci;

	//hynix 26nm
	if((page_num==0)||(page_num==1))
		return 1;
	if((page_num==nci->npi->page_cnt_per_blk-2)||(page_num==nci->npi->page_cnt_per_blk-1))
		return 0;
	if((page_num%4 == 2)||(page_num%4 == 3))
		return 1;
	return 0;	
}

