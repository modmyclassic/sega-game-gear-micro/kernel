/*
************************************************************************************************************************
*                                                      eNand
*                                           Nand flash driver scan module
*
*                             Copyright(C), 2008-2009, SoftWinners Microelectronic Co., Ltd.
*                                                  All Rights Reserved
*
* File Name : nand_chip_for_boot.c
*
* Author :
*
* Version : v0.1
*
* Date : 2013-11-20
*
* Description :
*
* Others : None at present.
*
*
*
************************************************************************************************************************
*/
#define _NCFRR6_C_

#include "../nand_physic_inc.h"

u8 m6_read_retry_mode = 0;
u8 m6_read_retry_cycle = 0;
u8 m6_read_retry_reg_cnt = 0;

u8 m6_read_retry_start_cmd[2] = {0x5c,0xc5};
u8 m6_read_retry_end_cmd[2] = {0x26,0x5d};

u8 m6_read_retry_cmd_1[4] = {0x55,0x55,0x55,0x55};
u8 m6_read_retry_addr_1[4] = {0x4,0x5,0x6,0x7};
u8 m6_p1_1[7][4] = {
    {0x00, 0x00, 0x00, 0x00},
    {0x04, 0x04, 0x04, 0x04},
    {0x7c, 0x7c, 0x7c, 0x7c},
    {0x78, 0x78, 0x78, 0x78},
    {0x74, 0x74, 0x74, 0x74},
    {0x08, 0x08, 0x08, 0x08},
    {0xff, 0xff, 0xff, 0xff},
};

u8 m6_read_retry_cmd_2[5] = {0x55,0x55,0x55,0x55,0x55};
u8 m6_read_retry_addr_2[5] = {0x4,0x5,0x6,0x7,0x0D};
u8 m6_p1_2[8][5] = {
    {0x04, 0x04, 0x7c, 0x7e, 0x00},
    {0x00, 0x7c, 0x78, 0x78, 0x00},
    {0x7c, 0x76, 0x74, 0x72, 0x00},
    {0x08, 0x08, 0x00, 0x00, 0x00},
    {0x0b, 0x7e, 0x76, 0x74, 0x00},
    {0x10, 0x76, 0x72, 0x70, 0x00},
    {0x02, 0x00, 0x7e, 0x7c, 0x00},
    {0x00, 0x00, 0x00, 0x00, 0x00},
};

u8 m6_p1_3[11][5] = {
	{0x00, 0x00, 0x00, 0x00, 0x00},
	{0x02, 0x04, 0x02, 0x00, 0x00},
	{0x7c, 0x00, 0x7c, 0x7c, 0x00},
	{0x7a, 0x00, 0x7a, 0x7a, 0x00},
	{0x78, 0x02, 0x78, 0x7a, 0x00},
	{0x7e, 0x04, 0x7e, 0x7a, 0x00},
	{0x76, 0x04, 0x76, 0x78, 0x00},
	{0x04, 0x04, 0x04, 0x76, 0x00},
	{0x06, 0x0a, 0x06, 0x02, 0x00},
	{0x74, 0x7c, 0x74, 0x76, 0x00},
	{0x00, 0x00, 0x00, 0x00, 0x00},
};


u8 m6_read_retry_last_cmd[1] = {0xB3};
u8 m6_read_retry_exit_cmd[1] = {0xff};

static u32  m6_toggle_mode_flag = 0;
static u32  m6_retry_flag = 0;

static u32 m6_sclk0_bak = 0;
static u32  m6_sclk1_bak = 0;

s32 m6_exit_readretry(struct _nand_chip_info *nci);
/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m6_read_page_end(struct _nand_physic_op_par* npo)
{
	int i=0, ret = 0;
    struct _nand_chip_info *nci =  nci_get_from_nsi(g_nsi, npo->chip);

    ret = m0_read_page_end_not_retry(npo);
    if(ret == ERR_ECC)
	{
	    PHY_DBG("m6 retry!\n");
		m6_retry_flag = 1;
	    for(i=0;i<m6_read_retry_cycle; i++)
	    {
	        nci->retry_count = i;
	        ret = m6_set_readretry(nci);
	        if(ret != 0)
	        {
	            continue;
	        }

	        ret = m0_read_page_start(npo);
	        ret |= m0_read_page_end_not_retry(npo);
	        if((ret == ECC_LIMIT) || (ret == 0))
	        {
				ret = ECC_LIMIT;
	            PHY_DBG("m6 ReadRetry ok! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
	            break;
	        }
	    }
		m6_retry_flag = 0;
	    nci->retry_count = 0;
        m6_exit_readretry(nci);
	}
//	if((ret != 0) && (ret != ECC_LIMIT))
//	{
//	    PHY_DBG("m6 ReadRetry fail! ch =%d, chip = %d  block = %d, page = %d\n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page);
//	}
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m6_readretry_init(struct _nand_chip_info *nci)
{
	nci->retry_count = 0;

    m6_read_retry_mode = (nci->npi->read_retry_type >> 16) & 0xff;
    m6_read_retry_cycle = (nci->npi->read_retry_type >> 8) & 0xff;
    m6_read_retry_reg_cnt = nci->npi->read_retry_type & 0xff;

    PHY_DBG("toshiba read retry conut: %d !\n",m6_read_retry_cycle);

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m6_readretry_exit(struct _nand_chip_info *nci)
{
	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m6_vender_pre_condition(struct _nand_chip_info *nci)
{
    s32 ret;

    ret = set_cmd_with_nand_bus( nci,m6_read_retry_start_cmd,0,NULL,NULL,0,2);
    if (ret)
    {
        PHY_ERR("toshiba vender_pre_condition error!\n");
        return ret;
    }

	return 0;
}


/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m6_read_retry_clock_save(struct _nand_chip_info *nci)
{
    NAND_GetClk(nci->nctri->channel_id, &m6_sclk0_bak, &m6_sclk1_bak);
    NAND_SetClk(nci->nctri->channel_id, 10, 10*2);
    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m6_read_retry_clock_recover(struct _nand_chip_info *nci)
{
    NAND_SetClk(nci->nctri->channel_id, m6_sclk0_bak, m6_sclk1_bak);
    return 0;
}


/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m6_vender_set_param(struct _nand_chip_info *nci, u8 *para, u8 *addr, u32 count)
{
    s32 ret = 0;

	if(m6_read_retry_mode == 0x12)
    {
        ret |= set_cmd_with_nand_bus( nci,m6_read_retry_cmd_2,0,addr,para,1,count);
        if((0x00 == para[0])&&(0x00 == para[1])&&(0x00 == para[2])&&(0x00 == para[3])&&(m6_retry_flag == 0))
        {
            ret |= set_one_cmd(nci,m6_read_retry_exit_cmd[0],1);
            //PHY_DBG("m6 rr value 1 %x %x %x %x!\n",para[0],para[1],para[2],para[3]);
	        return ret;
        }
    }
    if(m6_read_retry_mode == 0x11)
    {
        ret |= set_cmd_with_nand_bus( nci,m6_read_retry_cmd_2,0,addr,para,1,count);
        if((0x00 == para[0])&&(0x00 == para[1])&&(0x00 == para[2])&&(0x00 == para[3]))
        {
            ret |= set_one_cmd(nci,m6_read_retry_exit_cmd[0],1);
            //PHY_DBG("m6 rr value 1 %x %x %x %x!\n",para[0],para[1],para[2],para[3]);
	        return ret;
        }
        if((0x02 == para[0])&&(0x00 == para[1])&&(0x7e == para[2])&&(0x7c == para[3]))
        {
            ret |= set_one_cmd(nci,m6_read_retry_last_cmd[0],0);
        }
    }
    if(m6_read_retry_mode == 0x10)
    {
        if((0xff == para[0])&&(0xff == para[1])&&(0xff == para[2])&&(0xff == para[3]))
        {
            ret |= set_one_cmd(nci,m6_read_retry_exit_cmd[0],1);
            //PHY_DBG("m6 rr value 2 %x %x %x %x!\n",para[0],para[1],para[2],para[3]);
	        return ret;
        }
        else
        {
            ret |= set_cmd_with_nand_bus( nci,m6_read_retry_cmd_1,0,addr,para,1,count);
        }
    }

    ret |= set_one_cmd(nci,m6_read_retry_end_cmd[0],0);
    ret |= set_one_cmd(nci,m6_read_retry_end_cmd[1],0);

    PHY_DBG("m6 rr value 3 %x %x %x %x!\n",para[0],para[1],para[2],para[3]);
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m6_set_readretry(struct _nand_chip_info *nci)
{
    s32 ret;
    u8* dat;
    u8* addr;
    u32 cnt;

    if((m6_read_retry_mode != 0x10) && (m6_read_retry_mode != 0x11) && (m6_read_retry_mode != 0x12))
    {
        return ERR_NO_89;
    }

    m6_read_retry_clock_save(nci);

    nand_enable_chip(nci);

    ndfc_disable_randomize(nci->nctri);

    if(nci->retry_count == 0)
    {
        m6_vender_pre_condition(nci);
    }

    if(ndfc_is_toogle_interface(nci->nctri) != 0) //change to legacy mode from toggle mode after 0x53h cmd
    {
        ndfc_set_legacy_interface(nci->nctri);
        m6_toggle_mode_flag = 1;
    }

    if(m6_read_retry_mode == 0x11)
    {
        dat = m6_p1_2[nci->retry_count];
        addr = m6_read_retry_addr_2;
        cnt = 5;
    }
	else if(m6_read_retry_mode == 0x12)
    {
        dat = m6_p1_3[nci->retry_count];
        addr = m6_read_retry_addr_2;
        cnt = 5;
    }
    else
    {
        dat = m6_p1_1[nci->retry_count];
        addr = m6_read_retry_addr_1;
        cnt = 4;
    }
    ret = m6_vender_set_param(nci,dat,addr,cnt);

    if(m6_toggle_mode_flag == 1)
    {
        ndfc_set_toogle_interface(nci->nctri);  //change to toggle mode from legacy mode  after set param
    }

	nand_disable_chip(nci);

	m6_read_retry_clock_recover(nci);
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m6_exit_readretry(struct _nand_chip_info *nci)
{
    s32 ret;
    u8* dat;
    u8* addr;
    u32 cnt;

    if((m6_read_retry_mode != 0x10) && (m6_read_retry_mode != 0x11) && (m6_read_retry_mode != 0x12))
    {
        return ERR_NO_88;
    }

    nand_enable_chip(nci);

    ndfc_disable_randomize(nci->nctri);

    if(ndfc_is_toogle_interface(nci->nctri) != 0) //change to legacy mode from toggle mode after 0x53h cmd
    {
        ndfc_set_legacy_interface(nci->nctri);
        m6_toggle_mode_flag = 1;
    }

    if(m6_read_retry_mode == 0x11)
    {
        dat = m6_p1_2[7];
        addr = m6_read_retry_addr_2;
        cnt = 5;
    }
	else if(m6_read_retry_mode == 0x12)
    {
        dat = m6_p1_3[10];
        addr = m6_read_retry_addr_2;
        cnt = 5;
    }
    else
    {
        dat = m6_p1_1[6];
        addr = m6_read_retry_addr_1;
        cnt = 0;
    }

    ret = m6_vender_set_param(nci,dat,addr,cnt);

    if(m6_toggle_mode_flag == 1)
    {
        ndfc_set_toogle_interface(nci->nctri); //change to toggle mode from legacy mode  after set param
    }

	nand_disable_chip(nci);

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m6_special_init(void)
{
    int ret = 0;
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
        ret |= m6_readretry_init(nci);
		nci = nci->nsi_next;
	}

    if(ret == 0)
    {
        function_read_page_end = m6_read_page_end;
	    PHY_DBG(" m6_special_init m6_read_retry_mode :%d m6_read_retry_cycle :%d m6_read_retry_reg_cnt :%d \n",m6_read_retry_mode,m6_read_retry_cycle,m6_read_retry_reg_cnt);
	}
	else
	{
	    PHY_ERR(" m6_special_init error m6_read_retry_mode :%d m6_read_retry_cycle :%d m6_read_retry_reg_cnt :%d \n",m6_read_retry_mode,m6_read_retry_cycle,m6_read_retry_reg_cnt);
	}
    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m6_special_exit(void)
{
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
		m6_readretry_exit(nci);
		nci = nci->nsi_next;
	}
	PHY_DBG(" m6_special_exit \n");
    return 0;
}

int m6_is_lsb_page(__u32 page_num)
{	
	struct _nand_chip_info *nci = g_nsi->nci;

	//toshiba 2xnm 19nm 1ynm
	if(page_num==0)
		return 1;
	if(page_num==nci->npi->page_cnt_per_blk - 1)
		return 0;
	if(page_num%2==1)
		return 1;
	return 0;
}

