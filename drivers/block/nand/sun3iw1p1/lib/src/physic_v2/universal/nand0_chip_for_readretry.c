/*
************************************************************************************************************************
*                                                      eNand
*                                           Nand flash driver scan module
*
*                             Copyright(C), 2008-2009, SoftWinners Microelectronic Co., Ltd.
*                                                  All Rights Reserved
*
* File Name : nand_chip_for_boot.c
*
* Author :
*
* Version : v0.1
*
* Date : 2013-11-20
*
* Description :
*
* Others : None at present.
*
*
*
************************************************************************************************************************
*/
#define _NCFRR0_C_

#include "../nand_physic_inc.h"


/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m0_special_init(void)
{
    function_read_page_end = m0_read_page_end_not_retry;
	PHY_DBG(" m0 special init \n");
    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m0_special_exit(void)
{
	PHY_DBG(" m0 special exit \n");
    return 0;
}

int m0_is_lsb_page(__u32 page_num)
{
	return 1;
}

