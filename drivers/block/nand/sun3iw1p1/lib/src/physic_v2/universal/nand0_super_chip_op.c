/*
************************************************************************************************************************
*                                                      eNand
*                                           Nand flash driver scan module
*
*                             Copyright(C), 2008-2009, SoftWinners Microelectronic Co., Ltd.
*                                                  All Rights Reserved
*
* File Name : nand0_super_chip_op.c
*
* Author :
*
* Version : v0.1
*
* Date : 2013-11-20
*
* Description :
*
* Others : None at present.
*
*
*
************************************************************************************************************************
*/
#define _NSCO0_C_

#include "../nand_physic_inc.h"

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m0_use_chip_function(struct _nand_physic_op_par* npo,unsigned int function)
{
    int ret,i;
    struct _nand_physic_op_par lnpo;
    struct _nand_chip_info* nci;
    struct _nand_super_chip_info* nsci = nsci_get_from_nssi(g_nssi,npo->chip);
    unsigned int chip[8];
    unsigned int block[8];
    unsigned int block_num;

    if(nsci->two_plane == 0)
    {
        if((nsci->vertical_interleave == 1) && (nsci->dual_channel == 1))
        {
            block_num = 4;
            chip[0] = nsci->d_channel_nci_1->chip_no;
            block[0] = npo->block;

            chip[1] = nsci->d_channel_nci_2->chip_no;
            block[1] = npo->block;

            chip[2] = nsci->v_intl_nci_2->chip_no;
            block[2] = npo->block;

            nci = nci_get_from_nctri(nsci->d_channel_nci_2->nctri,nsci->v_intl_nci_1->chip_no);
            chip[3] = nci->chip_no;
            block[3] = npo->block;
        }
        else if(nsci->vertical_interleave == 1)
        {
            block_num = 2;
            chip[0] = nsci->v_intl_nci_1->chip_no;
            block[0] = npo->block;

            chip[1] = nsci->v_intl_nci_2->chip_no;
            block[1] = npo->block;
        }
        else if(nsci->dual_channel == 1)
        {
            block_num = 2;
            chip[0] = nsci->d_channel_nci_1->chip_no;
            block[0] = npo->block;

            chip[1] = nsci->d_channel_nci_2->chip_no;
            block[1] = npo->block;
        }
        else
        {
            block_num = 1;
            chip[0] = npo->chip;
            block[0] = npo->block;
        }
    }
    else
    {
        if((nsci->vertical_interleave == 1) && (nsci->dual_channel == 1))
        {
            block_num = 8;
            chip[0] = nsci->d_channel_nci_1->chip_no;
            block[0] = npo->block << 1;
            chip[1] = nsci->d_channel_nci_1->chip_no;
            block[1] = (npo->block << 1) + 1;

            chip[2] = nsci->d_channel_nci_2->chip_no;
            block[2] = npo->block << 1;
            chip[3] = nsci->d_channel_nci_2->chip_no;
            block[3] = (npo->block << 1) + 1;

            chip[4] = nsci->v_intl_nci_2->chip_no;
            block[4] = npo->block << 1;
            chip[5] = nsci->v_intl_nci_2->chip_no;
            block[5] = (npo->block << 1) + 1;

            nci = nci_get_from_nctri(nsci->d_channel_nci_2->nctri,nsci->v_intl_nci_1->chip_no);
            chip[6] = nci->chip_no;
            block[6] = npo->block << 1;
            chip[7] = nci->chip_no;
            block[7] = (npo->block << 1) + 1;
        }
        else if(nsci->vertical_interleave == 1)
        {
            block_num = 4;
            chip[0] = nsci->v_intl_nci_1->chip_no;
            block[0] = npo->block << 1;
            chip[1] = nsci->v_intl_nci_1->chip_no;
            block[1] = (npo->block << 1) + 1;

            chip[2] = nsci->v_intl_nci_2->chip_no;
            block[2] = npo->block << 1;
            chip[3] = nsci->v_intl_nci_2->chip_no;
            block[3] = (npo->block << 1) + 1;
        }
        else if(nsci->dual_channel == 1)
        {
            block_num = 4;
            chip[0] = nsci->d_channel_nci_1->chip_no;
            block[0] = npo->block << 1;
            chip[1] = nsci->d_channel_nci_1->chip_no;
            block[1] = (npo->block << 1) + 1;

            chip[2] = nsci->d_channel_nci_2->chip_no;
            block[2] = npo->block << 1;
            chip[3] = nsci->d_channel_nci_2->chip_no;
            block[3] = (npo->block << 1) + 1;
        }
        else
        {
            block_num = 2;
            chip[0] = npo->chip;
            block[0] = npo->block << 1;
            chip[1] = npo->chip;
            block[1] = (npo->block << 1) + 1;
        }
    }

    for(i=0,ret=0; i<block_num; i++)
    {
        lnpo.chip = chip[i];
        lnpo.block = block[i];
        lnpo.page = 0;
        if(function == 0)
        {
            ret |= m0_erase_block(&lnpo);
            //ret |= m0_erase_block_start(&lnpo);
        }
        else if(function == 1)
        {
            ret |= m0_bad_block_check(&lnpo);
            if(ret != 0)
            {
                break;
            }
        }
        else if(function == 2)
        {
            ret |= m0_bad_block_mark(&lnpo);
        }
        else
        {
            ;
        }
    }

    nand_wait_all_rb_ready();

    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m0_rw_use_chip_function(struct _nand_physic_op_par* npo,unsigned int function)
{
    int ret = 0;
    struct _nand_physic_op_par lnpo;
    struct _nand_super_chip_info* nsci = nsci_get_from_nssi(g_nssi,npo->chip);

    unsigned int chip[4];
    unsigned int block[4];
    unsigned int page[4];
    unsigned char *mdata[4];
    unsigned char *sdata[4];
    unsigned int slen[4];
    unsigned int sect_bitmap[4];
    unsigned int block_num;
	unsigned char oob_temp[64];
	unsigned int i;

    if((nsci->dual_channel == 1)&&(nsci->two_plane == 1)&&(function == 1))
    {
        block_num = 4;
        chip[0] = nsci->d_channel_nci_1->chip_no;
        block[0] = npo->block << 1;
        page[0] = npo->page;
        mdata[0] = npo->mdata;
        sect_bitmap[0] = nsci->d_channel_nci_1->sector_cnt_per_page;
        sdata[0] = npo->sdata;
        slen[0] = npo->slen;

        chip[1] = nsci->d_channel_nci_2->chip_no;
        block[1] = npo->block << 1;
        page[1] = npo->page;
        mdata[1] = npo->mdata + (nsci->d_channel_nci_1->sector_cnt_per_page << 10);
        sect_bitmap[1] = nsci->d_channel_nci_2->sector_cnt_per_page;
        sdata[1] = npo->sdata;
        slen[1] = npo->slen;

        chip[2] = nsci->d_channel_nci_1->chip_no;
        block[2] = (npo->block << 1) + 1;
        page[2] = npo->page;
        mdata[2] = npo->mdata +  (nsci->d_channel_nci_1->sector_cnt_per_page << 9);
        sect_bitmap[2] = nsci->d_channel_nci_1->sector_cnt_per_page;
        sdata[2] = npo->sdata;
        slen[2] = npo->slen;

        chip[3] = nsci->d_channel_nci_2->chip_no;
        block[3] = (npo->block << 1) + 1;
        page[3] = npo->page;
        mdata[3] = npo->mdata + (nsci->d_channel_nci_2->sector_cnt_per_page << 10) + (nsci->d_channel_nci_2->sector_cnt_per_page << 9);
        sect_bitmap[3] = nsci->d_channel_nci_2->sector_cnt_per_page;
        sdata[3] = npo->sdata;
        slen[3] = npo->slen;

		if(nsci->d_channel_nci_1->sector_cnt_per_page == 4)
		{
			for(i=0;i<16;i++)
			{
				if(i<8)
				{
					oob_temp[i] = *((unsigned char *)npo->sdata + i);
				}
				else if(i == 8)
				{
					oob_temp[i] = 0xff;
				}
				else
				{
					oob_temp[i] = *((unsigned char *)npo->sdata + i - 1);
				}
			}
			sdata[0] = &oob_temp[0];
			sdata[1] = &oob_temp[0];
			sdata[2] = &oob_temp[8];
			sdata[3] = &oob_temp[8];
		}

        lnpo.chip = chip[0];
        lnpo.block = block[0];
        lnpo.page = page[0];
        lnpo.mdata = mdata[0];
        lnpo.sect_bitmap = sect_bitmap[0];
        lnpo.sdata = sdata[0];
        lnpo.slen = slen[0];
        ret |= m0_write_page_start(&lnpo,1);

        lnpo.chip = chip[1];
        lnpo.block = block[1];
        lnpo.page = page[1];
        lnpo.mdata = mdata[1];
        lnpo.sect_bitmap = sect_bitmap[1];
        lnpo.sdata = sdata[1];
        lnpo.slen = slen[1];
        ret |= m0_write_page_start(&lnpo,1);

        lnpo.chip = chip[0];
        lnpo.block = block[0];
        lnpo.page = page[0];
        lnpo.mdata = mdata[0];
        lnpo.sect_bitmap = sect_bitmap[0];
        lnpo.sdata = sdata[0];
        lnpo.slen = slen[0];
        ret |= m0_write_page_end(&lnpo);

        lnpo.chip = chip[2];
        lnpo.block = block[2];
        lnpo.page = page[2];
        lnpo.mdata = mdata[2];
        lnpo.sect_bitmap = sect_bitmap[2];
        lnpo.sdata = sdata[2];
        lnpo.slen = slen[2];
        ret |= m0_write_page_start(&lnpo,2);

        lnpo.chip = chip[1];
        lnpo.block = block[1];
        lnpo.page = page[1];
        lnpo.mdata = mdata[1];
        lnpo.sect_bitmap = sect_bitmap[1];
        lnpo.sdata = sdata[1];
        lnpo.slen = slen[1];
        ret |= m0_write_page_end(&lnpo);

        lnpo.chip = chip[3];
        lnpo.block = block[3];
        lnpo.page = page[3];
        lnpo.mdata = mdata[3];
        lnpo.sect_bitmap = sect_bitmap[3];
        lnpo.sdata = sdata[3];
        lnpo.slen = slen[3];
        ret |= m0_write_page_start(&lnpo,2);

        lnpo.chip = chip[2];
        lnpo.block = block[2];
        lnpo.page = page[2];
        lnpo.mdata = mdata[2];
        lnpo.sect_bitmap = sect_bitmap[2];
        lnpo.sdata = sdata[2];
        lnpo.slen = slen[2];
        ret |= m0_write_page_end(&lnpo);

        lnpo.chip = chip[3];
        lnpo.block = block[3];
        lnpo.page = page[3];
        lnpo.mdata = mdata[3];
        lnpo.sect_bitmap = sect_bitmap[3];
        lnpo.sdata = sdata[3];
        lnpo.slen = slen[3];
        ret |= m0_write_page_end(&lnpo);

        return ret;
    }

    if((nsci->dual_channel == 1)&&(nsci->two_plane == 1)&&(function == 0))
    {
        block_num = 4;
        chip[0] = nsci->d_channel_nci_1->chip_no;
        block[0] = npo->block << 1;
        page[0] = npo->page;
        mdata[0] = npo->mdata;
        sect_bitmap[0] = nsci->d_channel_nci_1->sector_cnt_per_page;
        sdata[0] = npo->sdata;
        slen[0] = npo->slen;

        chip[1] = nsci->d_channel_nci_2->chip_no;
        block[1] = npo->block << 1;
        page[1] = npo->page;
        mdata[1] = npo->mdata + (nsci->d_channel_nci_1->sector_cnt_per_page << 10);
        sect_bitmap[1] = sect_bitmap[0];
        sdata[1] = NULL;
        slen[1] = 0;

        chip[2] = nsci->d_channel_nci_1->chip_no;
        block[2] = (npo->block << 1) + 1;
        page[2] = npo->page;
        mdata[2] = npo->mdata +  (nsci->d_channel_nci_1->sector_cnt_per_page << 9);
        sect_bitmap[2] = sect_bitmap[0];
        sdata[2] = NULL;
        slen[2] = 0;

        chip[3] = nsci->d_channel_nci_2->chip_no;
        block[3] = (npo->block << 1) + 1;
        page[3] = npo->page;
        mdata[3] = npo->mdata + (nsci->d_channel_nci_2->sector_cnt_per_page << 10) + (nsci->d_channel_nci_2->sector_cnt_per_page << 9);
        sect_bitmap[3] = sect_bitmap[0];
        sdata[3] = NULL;
        slen[3] = 0;

		if(nsci->d_channel_nci_1->sector_cnt_per_page == 4)
		{
			sdata[0] = &oob_temp[0];
			sdata[1] = NULL;
			sdata[2] = &oob_temp[8];
			sdata[3] = NULL;
		}
		
        lnpo.chip = chip[0];
        lnpo.block = block[0];
        lnpo.page = page[0];
        lnpo.mdata = mdata[0];
        lnpo.sect_bitmap = sect_bitmap[0];
        lnpo.sdata = sdata[0];
        lnpo.slen = slen[0];
        ret |= m0_read_page_start(&lnpo);

        lnpo.chip = chip[1];
        lnpo.block = block[1];
        lnpo.page = page[1];
        lnpo.mdata = mdata[1];
        lnpo.sect_bitmap = sect_bitmap[0];
        lnpo.sdata = sdata[1];
        lnpo.slen = slen[1];
        if(mdata[0] != NULL)
            ret |= m0_read_page_start(&lnpo);

        lnpo.chip = chip[0];
        lnpo.block = block[0];
        lnpo.page = page[0];
        lnpo.mdata = mdata[0];
        lnpo.sect_bitmap = sect_bitmap[0];
        lnpo.sdata = sdata[0];
        lnpo.slen = slen[0];
        ret |= m0_read_page_end(&lnpo);

        lnpo.chip = chip[2];
        lnpo.block = block[2];
        lnpo.page = page[2];
        lnpo.mdata = mdata[2];
        lnpo.sect_bitmap = sect_bitmap[0];
        lnpo.sdata = sdata[2];
        lnpo.slen = slen[2];
        if(mdata[0] != NULL)
            ret |= m0_read_page_start(&lnpo);

        lnpo.chip = chip[1];
        lnpo.block = block[1];
        lnpo.page = page[1];
        lnpo.mdata = mdata[1];
        lnpo.sect_bitmap = sect_bitmap[0];
        lnpo.sdata = sdata[1];
        lnpo.slen = slen[1];
        if(mdata[0] != NULL)
            ret |= m0_read_page_end(&lnpo);

        lnpo.chip = chip[3];
        lnpo.block = block[3];
        lnpo.page = page[3];
        lnpo.mdata = mdata[3];
        lnpo.sect_bitmap = sect_bitmap[0];
        lnpo.sdata = sdata[3];
        lnpo.slen = slen[3];
        if(mdata[0] != NULL)
            ret |= m0_read_page_start(&lnpo);

        lnpo.chip = chip[2];
        lnpo.block = block[2];
        lnpo.page = page[2];
        lnpo.mdata = mdata[2];
        lnpo.sect_bitmap = sect_bitmap[0];
        lnpo.sdata = sdata[2];
        lnpo.slen = slen[2];
        if(mdata[0] != NULL)
            ret |= m0_read_page_end(&lnpo);

        lnpo.chip = chip[3];
        lnpo.block = block[3];
        lnpo.page = page[3];
        lnpo.mdata = mdata[3];
        lnpo.sect_bitmap = sect_bitmap[0];
        lnpo.sdata = sdata[3];
        lnpo.slen = slen[3];
        if(mdata[0] != NULL)
            ret |= m0_read_page_end(&lnpo);

		if(nsci->d_channel_nci_1->sector_cnt_per_page == 4)
		{
			for(i=0;i<16;i++)
			{
				if(i<8)
				{
					*((unsigned char *)npo->sdata + i) = oob_temp[i];
				}
				else if(i == 16)
				{
					*((unsigned char *)npo->sdata + i) = 0xff;
				}
				else
				{
					*((unsigned char *)npo->sdata + i) = oob_temp[i+1];
				}
			}
		}

        return ret;
    }

    if(nsci->dual_channel == 1)
    {
        block_num = 2;
        chip[0] = nsci->d_channel_nci_1->chip_no;
        block[0] = npo->block;
        page[0] = npo->page;
        mdata[0] = npo->mdata;
        sect_bitmap[0] = nsci->d_channel_nci_1->sector_cnt_per_page;
        sdata[0] = npo->sdata;
        slen[0] = npo->slen;

        chip[1] = nsci->d_channel_nci_2->chip_no;
        block[1] = npo->block;
        page[1] = npo->page;
        mdata[1] = npo->mdata + (nsci->d_channel_nci_1->sector_cnt_per_page << 9);
        sect_bitmap[1] = sect_bitmap[0];
        sdata[1] = npo->sdata;
        slen[1] = npo->slen;
        if(function == 0)
        {
            sdata[1] = NULL;
            slen[1] = 0;
        }

        lnpo.chip = chip[0];
        lnpo.block = block[0];
        lnpo.page = page[0];
        lnpo.mdata = mdata[0];
        lnpo.sect_bitmap = sect_bitmap[0];
        lnpo.sdata = sdata[0];
        lnpo.slen = slen[0];
        if(function == 1)
        {
            ret |= m0_write_page_start(&lnpo,0);
        }
        else
        {
            ret |= m0_read_page_start(&lnpo);
        }

        lnpo.chip = chip[1];
        lnpo.block = block[1];
        lnpo.page = page[1];
        lnpo.mdata = mdata[1];
        lnpo.sect_bitmap = sect_bitmap[1];
        lnpo.sdata = sdata[1];
        lnpo.slen = slen[1];
        if(function == 1)
        {
            ret |= m0_write_page_start(&lnpo,0);
        }
        else
        {
            if(mdata[0] != NULL)
                ret |= m0_read_page_start(&lnpo);
        }

        lnpo.chip = chip[0];
        lnpo.block = block[0];
        lnpo.page = page[0];
        lnpo.mdata = mdata[0];
        lnpo.sect_bitmap = sect_bitmap[0];
        lnpo.sdata = sdata[0];
        lnpo.slen = slen[0];
        if(function == 1)
        {
            ret |= m0_write_page_end(&lnpo);
        }
        else
        {
            ret |= m0_read_page_end(&lnpo);
        }

        lnpo.chip = chip[1];
        lnpo.block = block[1];
        lnpo.page = page[1];
        lnpo.mdata = mdata[1];
        lnpo.sect_bitmap = sect_bitmap[1];
        lnpo.sdata = sdata[1];
        lnpo.slen = slen[1];
        if(function == 1)
        {
            ret |= m0_write_page_end(&lnpo);
        }
        else
        {
            if(mdata[0] != NULL)
                ret |= m0_read_page_end(&lnpo);
        }

        return ret;
    }

    if(nsci->vertical_interleave == 1)
    {
        if(npo->page & 0x01)
        {
            lnpo.chip = nsci->v_intl_nci_2->chip_no;
        }
        else
        {
            lnpo.chip = nsci->v_intl_nci_1->chip_no;
        }
        lnpo.page = npo->page >> 1;
    }
    else
    {
        lnpo.chip = nsci->nci_first->chip_no;
        lnpo.page = npo->page;
    }
    lnpo.block = npo->block;
    lnpo.mdata = npo->mdata;
    lnpo.sect_bitmap = nsci->nci_first->sector_cnt_per_page;
    lnpo.sdata = npo->sdata;
    lnpo.slen = npo->slen;

    if (lnpo.mdata == NULL)
        lnpo.sect_bitmap = 0;

    if((function == 0)&&(nsci->two_plane == 0))
    {
        ret |= m0_read_page(&lnpo);
    }
    else if((function == 0)&&(nsci->two_plane == 1))
    {
        ret |= m0_read_two_plane_page(&lnpo);
    }
    else if((function == 1)&&(nsci->two_plane == 0))
    {
        ret |= m0_write_page(&lnpo);
    }
    else if((function == 1)&&(nsci->two_plane == 1))
    {
        ret |= m0_write_two_plane_page(&lnpo);
    }
    else
    {
        ;
    }

    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m0_erase_super_block(struct _nand_physic_op_par* npo)
{
    int ret;

    ret = m0_use_chip_function(npo,0);

    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m0_read_super_page(struct _nand_physic_op_par* npo)
{
    int ret;

    ret = m0_rw_use_chip_function(npo,0);

    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m0_write_super_page(struct _nand_physic_op_par* npo)
{
    int ret;

    ret = m0_rw_use_chip_function(npo,1);

    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m0_super_bad_block_check(struct _nand_physic_op_par* npo)
{

    int ret;

    ret = m0_use_chip_function(npo,1);

    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m0_super_bad_block_mark(struct _nand_physic_op_par* npo)
{
    int ret;

    ret = m0_use_chip_function(npo,2);

    return ret;
}

