/*
************************************************************************************************************************
*                                                      eNand
*                                           Nand flash driver scan module
*
*                             Copyright(C), 2008-2009, SoftWinners Microelectronic Co., Ltd.
*                                                  All Rights Reserved
*
* File Name : nand_chip_for_boot.c
*
* Author :
*
* Version : v0.1
*
* Date : 2013-11-20
*
* Description :
*
* Others : None at present.
*
*
*
************************************************************************************************************************
*/
#define _NCFRR4_C_

#include "../nand_physic_inc.h"

u8 m4_read_retry_mode = 0;
u8 m4_read_retry_cycle = 0;

u8 m4_p1_1[10] = {0,0,0,1,2,3,4,5,6,7};
u8 m4_p1_2[12] = {0,0,0,1,2,3,4,5,6,7,8,0x0c};
u8 m4_p1_3[7] = {0x1,0x2,0x3,0x0,0x1,0x2,0x3};
static u8* m4_p1 = NULL;

extern int nand_set_feature(struct _nand_chip_info *nci, u8 *addr, u8 *feature);
extern int nand_get_feature(struct _nand_chip_info *nci, u8 *addr, u8 *feature);

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m4_read_page_end(struct _nand_physic_op_par* npo)
{
	int i=0, ret = 0;
    struct _nand_chip_info *nci =  nci_get_from_nsi(g_nsi, npo->chip);

    ret = m0_read_page_end_not_retry(npo);

//    if(nand_physic_temp1 == 1)
//       ret = ERR_ECC;

    if(ret == ERR_ECC)
	{
	    PHY_DBG("m4 retry!\n");
	    for(i=0; i<m4_read_retry_cycle; i++)
	    {
	        nci->retry_count = i;
	        ret = m4_set_readretry(nci);
	        if(ret != 0)
	        {
	            continue;
	        }

	        ret = m0_read_page_start(npo);
	        ret |= m0_read_page_end_not_retry(npo);

//            if(nand_physic_temp1 == 1)
//                ret = ERR_ECC;

	        if((ret == ECC_LIMIT) || (ret == 0))
	        {
				ret = ECC_LIMIT;
	            PHY_DBG("m4 ReadRetry ok! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
	            break;
	        }
	    }
	    nci->retry_count = 0xff;
        m4_set_readretry(nci);
	}
//	if((ret != 0) && (ret != ECC_LIMIT))
//	{
//	    PHY_DBG("m4 ReadRetry fail! ch =%d, chip = %d  block = %d, page = %d\n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page);
//	}
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int micron_intel_set_feature(struct _nand_chip_info *nci,u8 addr,u8 dat)
{
	u8 maddr;
	u8 p[4];
	u8 pr[4]; //read back value

    maddr = addr;
	p[0] = dat;
	p[1] = 0x0;
	p[2] = 0x0;
	p[3] = 0x0;

	nand_set_feature(nci, &addr, p);
	nand_get_feature(nci, &addr, pr);
	if (pr[0] != p[0])
	{
		PHY_ERR("set feature(addr %d) p0: %d readretry Configuration failed!\n",maddr,dat);
		return ERR_NO_72;
	}
	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m4_readretry_init(struct _nand_chip_info *nci)
{
	nci->retry_count = 0;
    m4_read_retry_mode = 0;

    m4_p1 = m4_p1_2;

    m4_read_retry_mode = (nci->npi->read_retry_type >> 16) & 0xff;
    m4_read_retry_cycle = (nci->npi->read_retry_type >> 8) & 0xff;

    micron_intel_set_feature(nci,0x89,0);

    if(m4_read_retry_mode == 0x41)
    {
            m4_read_retry_cycle = 12;
            m4_p1 = m4_p1_2;
    }
    else if(m4_read_retry_mode == 0x40)
    {
        m4_p1 = m4_p1_1;
        m4_read_retry_cycle = 10;
    }
    else if(m4_read_retry_mode == 0x50)  //intel
    {
        m4_p1 = m4_p1_3;
        m4_read_retry_cycle = 7;
        micron_intel_set_feature(nci,0x93,0);
    }
    else
    {
        PHY_ERR("micron read retry cannot support!\n");
        return ERR_NO_71;
    }
    PHY_DBG("micron read retry conut: %d !\n",m4_read_retry_cycle);

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m4_readretry_exit(struct _nand_chip_info *nci)
{
    m4_read_retry_mode = 0;
    m4_read_retry_cycle = 0;
	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m4_set_readretry(struct _nand_chip_info *nci)
{
    s32 ret=0;
    u8 count;

    nand_enable_chip(nci);

    ndfc_disable_randomize(nci->nctri);

    if((m4_read_retry_mode != 0x40) && (m4_read_retry_mode != 0x41) && (m4_read_retry_mode != 0x50))
    {
        goto m4_readretry_end;
    }

    count = nci->retry_count;
    if(count == 0xff)
    {
        count = 0;
    }

    if(m4_read_retry_mode == 0x50)
    {
        if(m4_p1[count] == 0)
        {
            ret = micron_intel_set_feature(nci,0x93,1);
            if(ret != 0)
            {
                goto m4_readretry_end;
            }
        }
        if(nci->retry_count == 0xff)
        {
            ret = micron_intel_set_feature(nci,0x89,0);
            ret |= micron_intel_set_feature(nci,0x93,0);
            goto m4_readretry_end;
        }
    }
    ret = micron_intel_set_feature(nci,0x89,m4_p1[count]);

m4_readretry_end:

    if(nci->retry_count == 0xff)
    {
        nci->retry_count = 0;
    }

	nand_disable_chip(nci);

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m4_special_init(void)
{
    int ret = 0;
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
        ret |= m4_readretry_init(nci);
		nci = nci->nsi_next;
	}

    if(ret == 0)
    {
        function_read_page_end = m4_read_page_end;
	    PHY_DBG(" m4_special_init m4_read_retry_mode:%d m4_read_retry_cycle:%d \n",m4_read_retry_mode,m4_read_retry_cycle);
    }
	else
	{
	    PHY_ERR(" m4_special_init error m4_read_retry_mode:%d m4_read_retry_cycle:%d \n",m4_read_retry_mode,m4_read_retry_cycle);
	}
    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m4_special_exit(void)
{
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
		m4_readretry_exit(nci);
		nci = nci->nsi_next;
	}
	PHY_DBG(" m4_special_exit \n");
    return 0;
}

int m4_0x40_is_lsb_page(__u32 page_num)
{	
	struct _nand_chip_info *nci = g_nsi->nci;

	if((page_num==0)||(page_num==1))
		return 1;
	if((page_num==nci->npi->page_cnt_per_blk-2)||(page_num==nci->npi->page_cnt_per_blk-1))
		return 0;
	if((page_num%4 == 2)||(page_num%4 == 3))
		return 1;
	return 0;	
}

int m4_0x41_is_lsb_page(__u32 page_num)
{	
	struct _nand_chip_info *nci = g_nsi->nci;

	//micron 20nm L83A L84A L84C L84D L85A
	if((page_num==2)||(page_num==3))
		return 1;
	if((page_num==nci->npi->page_cnt_per_blk-2)||(page_num==nci->npi->page_cnt_per_blk-1))
		return 1;
	if((page_num%4 == 0)||(page_num%4 == 1))
		return 1;
	return 0;	
}

int m4_0x42_is_lsb_page(__u32 page_num)
{	
	struct _nand_chip_info *nci = g_nsi->nci;

	//micron 16nm l95b
	if((page_num==0)||(page_num==1)||(page_num==2)||(page_num==3)||(page_num==4)||(page_num==5)||(page_num==7)||(page_num==8)||(page_num==509))
		return 1;
	if((page_num==6)||(page_num==508)||(page_num==511))
		return 0;
	if((page_num%4 == 2)||(page_num%4 == 3))
		return 1;
	return 0;	
}


