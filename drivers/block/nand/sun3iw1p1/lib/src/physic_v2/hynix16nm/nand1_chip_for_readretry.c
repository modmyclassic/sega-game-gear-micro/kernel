/*
************************************************************************************************************************
*                                                      eNand
*                                           Nand flash driver scan module
*
*                             Copyright(C), 2008-2009, SoftWinners Microelectronic Co., Ltd.
*                                                  All Rights Reserved
*
* File Name : nand_chip_for_boot.c
*
* Author :
*
* Version : v0.1
*
* Date : 2013-11-20
*
* Description :
*
* Others : None at present.
*
*
*
************************************************************************************************************************
*/
#define _NCFRR1_C_

#include "../nand_physic_inc.h"

u8 m1_read_retry_mode = 0;
u8 m1_read_retry_cycle = 0;
u8 m1_read_retry_reg_num = 0;
u8 m1_read_retry_reg_adr[4] = {0};

//u8 m1_read_retry_value[8][4] = {{0}, {0}};

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m1_read_page_end(struct _nand_physic_op_par* npo)
{
	int i=0, ret = 0;
    struct _nand_chip_info *nci =  nci_get_from_nsi(g_nsi, npo->chip);

    ret = m0_read_page_end_not_retry(npo);
    if(ret == ERR_ECC)
	{
	    PHY_DBG("m1 retry!\n");
	    for(i=0; i<m1_read_retry_cycle; i++)
	    {
	        ret = 0;
	        nci->retry_count ++;
	        if(nci->retry_count > m1_read_retry_cycle)
	        {
	            nci->retry_count = 0;
	        }

	        ret = m1_set_readretry(nci);
	        if(ret != 0)
	        {
                nci->retry_count = 0;
	            m1_set_readretry(nci);
	            break;
	        }

	        ret = m0_read_page_start(npo);
	        ret |= m0_read_page_end_not_retry(npo);
	        if((ret == ECC_LIMIT) || (ret == 0))
	        {
			    ret = ECC_LIMIT;
	            PHY_DBG("m1 ReadRetry ok! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
	            break;
	        }
	    }
	    //if(i == m1_read_retry_cycle)
	    {
	        nci->retry_count = 0;
	        m1_set_readretry(nci);
	    }
	}
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m1_write_page_FF(struct _nand_physic_op_par* npo, u32 page_size_k)
{
	u32 i, n;
	u32 spare_size;
	int ret=0;
	unsigned int row_addr = 0, col_addr = 0;
	struct _nand_chip_info* nci = nci_get_from_nsi(g_nsi,npo->chip);
	struct _nand_controller_info *nctri = nci->nctri;
	struct _nctri_cmd_seq* cmd_seq = &nci->nctri->nctri_cmd_seq;

	//PHY_DBG("%s: ch: %d  chip: %d/%d  block: %d/%d \n", __func__, nctri->channel_id, nci->nctri_chip_no, nctri->chip_cnt, npo->block, nci->blk_cnt_per_chip);

	if ((nci->nctri_chip_no >= nctri->chip_cnt) || (npo->block >= nci->blk_cnt_per_chip))
	{
		PHY_ERR("fatal err -0, wrong input parameter, ch: %d  chip: %d/%d  block: %d/%d \n", nctri->channel_id, nci->nctri_chip_no, nctri->chip_cnt, npo->block, nci->blk_cnt_per_chip);
		return ERR_NO_108;
	}

	nand_read_chip_status_ready(nci);

	nand_enable_chip(nci);

	ndfc_disable_randomize(nci->nctri);

	ndfc_disable_ecc(nci->nctri);

	ndfc_clean_cmd_seq(cmd_seq);

	if(page_size_k == 8)
	{
		n = 9;
		spare_size = 832;
	}
	else if(page_size_k == 16)
	{
		n = 18;
		spare_size = 640;
	}

	for(i=0;i<n;i++)
	{
		if(i==0)
		{
		    // cmd1: 0x80
		    cmd_seq->cmd_type = CMD_TYPE_NORMAL;
		    cmd_seq->nctri_cmd[0].cmd = CMD_WRITE_PAGE_CMD1;
		    cmd_seq->nctri_cmd[0].cmd_valid = 1;
		    cmd_seq->nctri_cmd[0].cmd_send = 1;
		    cmd_seq->nctri_cmd[0].cmd_direction = 1;
		    cmd_seq->nctri_cmd[0].cmd_swap_data = 1;
		    cmd_seq->nctri_cmd[0].cmd_swap_data_dma = 1;
		    cmd_seq->nctri_cmd[0].cmd_mdata_addr = npo->mdata;
		    cmd_seq->nctri_cmd[0].cmd_mdata_len = 1024;
		    cmd_seq->nctri_cmd[0].cmd_trans_data_nand_bus = 1;

		    row_addr = get_row_addr(nci->page_offset_for_next_blk, npo->block, npo->page);
		    cmd_seq->nctri_cmd[0].cmd_acnt = 5;
		    fill_cmd_addr(col_addr, 2, row_addr, 3, cmd_seq->nctri_cmd[0].cmd_addr);

		    ret = ndfc_execute_cmd(nci->nctri, cmd_seq);
		}
		else if(i==(n-1))
		{
			 // cmd1: 0x85
			cmd_seq->cmd_type = CMD_TYPE_NORMAL;
			cmd_seq->nctri_cmd[0].cmd = CMD_CHANGE_WRITE_ADDR_CMD;
			cmd_seq->nctri_cmd[0].cmd_valid = 1;
			cmd_seq->nctri_cmd[0].cmd_send = 1;
			cmd_seq->nctri_cmd[0].cmd_direction = 1;
			cmd_seq->nctri_cmd[0].cmd_swap_data = 1;
			cmd_seq->nctri_cmd[0].cmd_swap_data_dma = 1;
			cmd_seq->nctri_cmd[0].cmd_mdata_addr = (npo->mdata + 1024*i);
			cmd_seq->nctri_cmd[0].cmd_mdata_len = spare_size;
			cmd_seq->nctri_cmd[0].cmd_trans_data_nand_bus = 1;

			//row_addr = get_row_addr(nci->page_offset_for_next_blk, npo->block, npo->page);
			cmd_seq->nctri_cmd[0].cmd_acnt = 2;
			fill_cmd_addr(col_addr, 2, row_addr, 0, cmd_seq->nctri_cmd[0].cmd_addr);

			ret = ndfc_execute_cmd(nci->nctri, cmd_seq);
		}
		else
		{
			 // cmd1: 0x85
			cmd_seq->cmd_type = CMD_TYPE_NORMAL;
			cmd_seq->nctri_cmd[0].cmd = CMD_CHANGE_WRITE_ADDR_CMD;
			cmd_seq->nctri_cmd[0].cmd_valid = 1;
			cmd_seq->nctri_cmd[0].cmd_send = 1;
			cmd_seq->nctri_cmd[0].cmd_direction = 1;
			cmd_seq->nctri_cmd[0].cmd_swap_data = 1;
			cmd_seq->nctri_cmd[0].cmd_swap_data_dma = 1;
			cmd_seq->nctri_cmd[0].cmd_mdata_addr = (npo->mdata + 1024*i);
			cmd_seq->nctri_cmd[0].cmd_mdata_len = 1024;
			cmd_seq->nctri_cmd[0].cmd_trans_data_nand_bus = 1;

			//row_addr = get_row_addr(nci->page_offset_for_next_blk, npo->block, npo->page);
			cmd_seq->nctri_cmd[0].cmd_acnt = 2;
			fill_cmd_addr(col_addr, 2, row_addr, 0, cmd_seq->nctri_cmd[0].cmd_addr);

			ret = ndfc_execute_cmd(nci->nctri, cmd_seq);
		}
		col_addr += 1024;
	}

	 // cmd1: 0x10
	cmd_seq->cmd_type = CMD_TYPE_NORMAL;
	cmd_seq->nctri_cmd[0].cmd = CMD_WRITE_PAGE_CMD2;
	cmd_seq->nctri_cmd[0].cmd_valid = 1;
	cmd_seq->nctri_cmd[0].cmd_send = 1;
	cmd_seq->nctri_cmd[1].cmd_wait_rb = 1;

	ret = ndfc_execute_cmd(nci->nctri, cmd_seq);

	ret = nand_read_chip_status_ready(nci);
	if(ret != 0)
	{
		PHY_ERR("m1 write page FF wrong\n");
	}
	nand_disable_chip(nci);
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m1_vender_get_param(struct _nand_chip_info *nci, u8 *para, u8 *addr, u32 count)
{
    __u32 i;
    __s32 ret = 0;
    __u8 cmd_r = 0x37; //H27UCG8T2ETR

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

    for (i=0; i<count; i++)
	{
//    	*nci->nctri->nreg.reg_byte_cnt = 1;
//		_set_addr(nci->nctri, 1, &addr[i]);
//		cfg = cmd_r;
//		cfg |= (NDFC_SEND_ADR | NDFC_DATA_TRANS | NDFC_SEND_CMD1 );
//		ret = ndfc_wait_cmdfifo_free(nci->nctri);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ret = ndfc_wait_cmd_finish(nci->nctri);
//		*(para+i) = *((u8 *)nci->nctri->nreg.reg_ram0_base);

		get_data_with_nand_bus_one_cmd(nci,&cmd_r,addr+i,para+i,1);
	}
	nand_disable_chip(nci);
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m1_vender_set_param(struct _nand_chip_info *nci, u8 *para, u8 *addr, u32 count)
{
    u32 i;
    u8 cmd_w, cmd_end;
    s32 ret = 0;

    cmd_w = 0x36;
    cmd_end = 0x16;

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

    for(i=0; i<count; i++)
	{
//        *nci->nctri->nreg.reg_byte_cnt = 1;
//        *nci->nctri->nreg.reg_ram0_base = para[i];
//        _set_addr(nci->nctri, 1, &addr[i]);
//        cfg = cmd_w;
//        cfg |= (NDFC_SEND_ADR | NDFC_DATA_TRANS | NDFC_ACCESS_DIR | NDFC_SEND_CMD1 | NDFC_WAIT_FLAG);
//        ret = ndfc_wait_cmdfifo_free(nci->nctri);
//        *nci->nctri->nreg.reg_cmd = cfg;
//        ret = ndfc_wait_cmd_finish(nci->nctri);
//
//        /*set NFC_REG_CMD*/
//        cfg = cmd_end;
//        cfg |= ( NDFC_SEND_CMD1);
//        ret = ndfc_wait_cmdfifo_free(nci->nctri);
//        *nci->nctri->nreg.reg_cmd = cfg;
//        ret = ndfc_wait_cmd_finish(nci->nctri);

		set_cmd_with_nand_bus(nci,&cmd_w,0,&addr[i],para+i,1,1);
		set_one_cmd(nci,cmd_end,0);
	}

	nand_disable_chip(nci);
    PHY_DBG("rr value %x %x %x %x!\n",para[0],para[1],para[2],para[3]);
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m1_major_check_byte(u8 *out, u32 mode, u32 level, u8 *in, u8 *in_inverse, u32 len)
{
	u32 bit, byte;
	u32 cnt_1; /* the total number of bit '1' on specified bit position in all input bytes */
	u32 cnt_0;
	u32 get_bit, get_total_bit;
	u8 byte_ok = 0;

	if (level < len/2) {
		PHY_ERR("_major_check_byte, wrong input para, level %d, len %d\n", level, len);
		*out = 0xff;
		return ERR_NO_107;
	}

	get_total_bit = 0;
	for (bit=0; bit<8; bit++)
	{
		cnt_1 = 0;
		cnt_0 = 0;
		get_bit = 0;
		for (byte=0; byte<len; byte++)
		{
			if ( in[byte] & (1U<<bit) )
				cnt_1++;
			else
				cnt_0++;
		}

		if (cnt_1 > level) {
			byte_ok |= (1U<<bit);
			get_bit = 1;
			//msg("%d:  '1'-'0' : %d - %d --> 1\n", bit, cnt_1, cnt_0);
		}

		if (cnt_0 > level) {
			get_bit = 1;
			//msg("%d:  '1'-'0' : %d - %d --> 0\n", bit, cnt_1, cnt_0);
		}

		if ((get_bit==0) && (mode==1)) {
			/* try 2nd group of input data */
			cnt_1 = 0;
			cnt_0 = 0;
			get_bit = 0;
			for (byte=0; byte<len; byte++)
			{
				if ( in_inverse[byte] & (1U<<bit) )
					cnt_0++;
				else
					cnt_1++;
			}

			/* get a correct bit, but it's a inverse one */
			if (cnt_0 > level) {
				//msg("inverse %d:  '1'-'0' : %d - %d --> 0\n", bit, cnt_1, cnt_0);
				get_bit = 1;
			}
			if (cnt_1 > level) {
				//msg("inverse %d:  '1'-'0' : %d - %d --> 0\n", bit, cnt_1, cnt_0);
				byte_ok |= (1U<<bit);
				get_bit = 1;
			}
		}

		if (get_bit)
			get_total_bit++;
		else {
			PHY_ERR("%d:  '1'-'0' : %d - %d\n", bit, cnt_1, cnt_0);
			PHY_ERR("get bit %d failed!\n", bit);
		}
	}

	if (get_total_bit == 8) {
		*out = byte_ok;
		//msg("get byte: 0x%x\n", *out);
		return 0;
	} else {
		*out = 0xff;
		return ERR_NO_80;
	}
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m1_get_read_retry_cfg(u8 *rr_cnt, u8 *rr_reg_cnt, u8 *rr_tab, u8 *otp)
{
	__s32 err_flag=0, ret=0;
	__u32 i, nbyte, nset;
	__u8 buf[32]={0}, buf_inv[32]={0};
	__u32 rr_tab_size = 32; //RR_CNT_IN_OTP * RR_REG_CNT_IN_OTP

	/* read retry count */
	for (i=0; i<8; i++)
		buf[i] = otp[i];

	ret = m1_major_check_byte(rr_cnt, 0, 4, buf, buf_inv, 8);
	if (ret<0)
	{
		PHY_ERR("_get_read_retry_parameters, get rr count failed!\n");
		return ret;
	}

	else
		PHY_DBG("rr cnt: %d\n", (* rr_cnt));

	/* read retry register count */
	for (i=0; i<8; i++)
		buf[i] = otp[8 + i];

	ret = m1_major_check_byte(rr_reg_cnt, 0, 4, buf, buf_inv, 8);
	if (ret<0) {
		PHY_ERR("_get_read_retry_parameters, get rr reg count failed!\n");
		return ret;
	} else
		PHY_DBG("rr reg cnt: %d\n", (* rr_reg_cnt));

	if(((* rr_cnt) != 8) || ((* rr_reg_cnt) != 4))
	{
		PHY_ERR("read retry value from otp error: rr_cnt %d rr_reg_cnt %d!\n",(* rr_cnt),(* rr_reg_cnt));
		return ERR_NO_81;
	}

	/* read retry table */
	for (nbyte=0; nbyte<rr_tab_size; nbyte++)
	{
		for (nset=0; nset< 8; nset++)
		{
			buf[nset] = 0;
			buf_inv[nset] = 0;
			buf[nset] = otp[16 + nset*rr_tab_size*2 + nbyte];
			buf_inv[nset] = otp[16 + nset*rr_tab_size*2 + rr_tab_size + nbyte];
		}
		/*
		for (nset=0; nset<RR_TAB_BACKUP_CNT; nset++)
		{
			msg("%02x - %02x\n", buf[nset], buf_inv[nset]);
		}
		*/

		ret = m1_major_check_byte(&rr_tab[nbyte], 1, 4, buf, buf_inv, 8);
		if (ret<0) {
			PHY_ERR("_get_read_retry_parameters, get the %d-th byte of rr table failed!\n", nbyte);
			err_flag = ret;
			break;
		}
	}

	for (nbyte=0; nbyte<rr_tab_size; nbyte++)
	{
		if (((nbyte%8)==0) && nbyte)
			PHY_DBG("\n");
		PHY_DBG("%02x ", rr_tab[nbyte]);
	}
	PHY_DBG("\n");

	if (err_flag)
		ret = err_flag;
	else
		ret = 0;

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m1_read_otp_info_hynix(struct _nand_chip_info *nci, u8 *otp_chip)
{
	u32 i;
	u8 *otp;
	//u8 abuf[8]={0};
	u8 address[5];
	u8 cmd[1];
	u8 data[1];
	u32 ret;

	nand_enable_chip(nci);
	ndfc_disable_randomize(nci->nctri);

	PHY_DBG("start get read retry param from: ce %d, rb %d...\n", nci->nctri->ce[nci->nctri_chip_no],nci->nctri->rb[nci->nctri_chip_no]);

	otp = otp_chip;
	if (otp == NULL) {
	    nand_disable_chip(nci);
		PHY_ERR("invalid buffer for otp info!\n");
		return ERR_NO_79;
	}

//	 // send 0xFF cmd
//	cfg = 0;
//	cfg = (NDFC_SEND_CMD1 | NDFC_WAIT_FLAG| 0xff);
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	{
//		cfg = 0;
//		abuf[0] = 0x38;
//		//send cmd 0x36, addr 0x38, data 0x52
//		*nci->nctri->nreg.reg_byte_cnt = 1;
//		*nci->nctri->nreg.reg_ram0_base = 0x52;
//		_set_addr(nci->nctri, 1, &abuf[0]);
//		cfg = (NDFC_SEND_CMD1 | NDFC_DATA_TRANS |NDFC_ACCESS_DIR | NDFC_SEND_ADR |0x36);
//		ndfc_wait_cmdfifo_free(nci->nctri);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ndfc_wait_cmd_finish(nci->nctri);
//
//		//send cmd 0x16, 0x17, 0x04, 0x19, 0x00
//		cfg = 0;
//		ndfc_wait_cmdfifo_free(nci->nctri);
//		cfg = (NDFC_SEND_CMD1|0x16);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ndfc_wait_cmd_finish(nci->nctri);
//
//		cfg = 0;
//		ndfc_wait_cmdfifo_free(nci->nctri);
//		cfg = (NDFC_SEND_CMD1|0x17);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ndfc_wait_cmd_finish(nci->nctri);
//
//		cfg = 0;
//		ndfc_wait_cmdfifo_free(nci->nctri);
//		cfg = (NDFC_SEND_CMD1|0x04);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ndfc_wait_cmd_finish(nci->nctri);
//
//		cfg = 0;
//		ndfc_wait_cmdfifo_free(nci->nctri);
//		cfg = (NDFC_SEND_CMD1|0x19);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ndfc_wait_cmd_finish(nci->nctri);
//
//		cfg = 0;
//		ndfc_wait_cmdfifo_free(nci->nctri);
//		cfg = (NDFC_SEND_CMD1|0x00);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ndfc_wait_cmd_finish(nci->nctri);
//
//		//send addr 00, 00, 00, 02
//		cfg = 0;
//		abuf[0] = 0x00;
//		abuf[1] = 0x00;
//		abuf[2] = 0x00;
//		abuf[3] = 0x02;
//		_set_addr(nci->nctri, 4, &abuf[0]);
//		cfg = (NDFC_SEND_ADR|(0x3<<16));
//		ndfc_wait_cmdfifo_free(nci->nctri);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ndfc_wait_cmd_finish(nci->nctri);
//	}
//
//	{
//
//		abuf[0] = 0x00;
//		//send addr
//		cfg = 0;
//		_set_addr(nci->nctri, 1, &abuf[0]);
//		cfg = (NDFC_SEND_ADR);
//		ndfc_wait_cmdfifo_free(nci->nctri);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ndfc_wait_cmd_finish(nci->nctri);
//	}
//
//	//send cmd 0x30, read data
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	cfg = 0;
//	*nci->nctri->nreg.reg_byte_cnt = 528;
//	cfg = (NDFC_SEND_CMD1|NDFC_WAIT_FLAG|NDFC_DATA_TRANS|0x30);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	/* read otp data from ndfc fifo */
//	for (i=0; i<528; i++)
//	{
//		otp[i] = *((u8 *)nci->nctri->nreg.reg_ram0_base + i);
//	}
//
//	 // send 0xFF cmd
//	cfg = 0;
//	cfg = (NDFC_SEND_CMD1 | NDFC_WAIT_FLAG| 0xff);
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	cfg = 0;
//	abuf[0] = 0x38;
//	//send cmd 0x36, addr 0x38, data 0x00
//	*nci->nctri->nreg.reg_byte_cnt = 1;
//	*nci->nctri->nreg.reg_ram0_base = 0x00;
//	_set_addr(nci->nctri, 1, &abuf[0]);
//	cfg = (NDFC_SEND_CMD1 | NDFC_DATA_TRANS |NDFC_ACCESS_DIR | NDFC_SEND_ADR |0x36);
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	//send 0x16 cmd
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	cfg = 0;
//	cfg = (NDFC_SEND_CMD1|0x16);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	/* dummy read(address don't care) 0x00 cmd + 0x0 addr + 0x30 cmd*/
//	abuf[0] = 0x00;
//	abuf[1] = 0x00;
//	abuf[2] = 0x00;
//	abuf[3] = 0x00;
//	abuf[4] = 0x00;
//	_set_addr(nci->nctri, 5, &abuf[0]);
//	cfg = 0;
//	cfg = (NDFC_SEND_CMD1 | NDFC_SEND_ADR |(0x4<<16)|0x00);
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	//send 0x30 cmd
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	cfg = 0;
//	cfg = (NDFC_SEND_CMD1 | NDFC_WAIT_FLAG |0x30);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);


	set_one_cmd(nci,0xff,1);

	cmd[0] = 0x36;
	address[0] = 0x38;
	data[0] = 0x52;
	set_cmd_with_nand_bus(nci,cmd,0,address,data,1,1);
    set_one_cmd(nci,0x16,0);
    set_one_cmd(nci,0x17,0);
    set_one_cmd(nci,0x04,0);
    set_one_cmd(nci,0x19,0);
    set_one_cmd(nci,0x00,0);

    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x02);
    set_one_addr(nci,0x00);

    cmd[0] = 0x30;
    get_data_with_nand_bus_one_cmd(nci,cmd,NULL,otp,528);
    set_one_cmd(nci,0xff,1);

	cmd[0] = 0x36;
	address[0] = 0x38;
	data[0] = 0x00;
	set_cmd_with_nand_bus(nci,cmd,0,address,data,1,1);

	set_one_cmd(nci,0x16,0);
	
	//dumy read
	set_one_cmd(nci,0x00,0);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_cmd(nci,0x30,1);

#if 0
	for (i=1; i<529; i++)
	{
		PHY_DBG(" %x ", otp[i-1]);
		if (((i%8)==0) && i)
		{
			PHY_DBG(" \n");
		}
	}
	PHY_DBG(" \n");
#endif

    if((otp[0] != 0x08) || (otp[1] != 0x08))
    {
        PHY_ERR("hynix OTP RegCount value error: 0x%x, 0x%x \n",otp[0], otp[1]);
        nand_disable_chip(nci);
        ret = ERR_NO_96;
		return ret;
    }

	nand_disable_chip(nci);
	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m1_get_rr_value_otp_hynix(struct _nand_chip_info *nci)
{
	s32 ret = 0;
	u8*  otp_info_hynix_16nm;
	u8 rr_cnt_hynix_16nm;
	u8 rr_reg_cnt_hynix_16nm;

	otp_info_hynix_16nm = nand_get_temp_buf(528);
	if (!otp_info_hynix_16nm)
	{
		PHY_ERR("otp_info_hynix_16nm : allocate memory fail\n");
		return ERR_NO_78;
	}

	ret =m1_read_otp_info_hynix(nci, otp_info_hynix_16nm);
	if (ret<0) {
		PHY_ERR("m1 CH %d chip %d get otp info failed!\n",nci->nctri->channel_id,nci->nctri_chip_no);
	}

	ret = m1_get_read_retry_cfg(&rr_cnt_hynix_16nm,&rr_reg_cnt_hynix_16nm, (u8 *)(nci->readretry_value),otp_info_hynix_16nm);
	if (ret<0) {
		PHY_ERR("m1 CH %d chip %d get read retry cfg from otp info failed!\n",nci->nctri->channel_id,nci->nctri_chip_no);
	}

	nand_free_temp_buf(otp_info_hynix_16nm,528);

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
void m1_saveotpvalue(struct _nand_chip_info *nci, __u8* otp_value)
{
    u32 i;
    for(i = 0;i<32; i++)
    {
        nci->readretry_value[i] = otp_value[i];
    }
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m1_getotpparam(struct _nand_chip_info *nci, u8* default_value)
{
    s32 ret;
    u32 i, j, Count;
	u32 flag;

    Count = 0;
    flag = 0;
    while(flag == 0)
    {
    	PHY_DBG("_vender_get_param_otp_hynix time %d!\n", Count);
    	ret = m1_get_rr_value_otp_hynix(nci);
    	if(ret == 0)
    	{
    		flag = 1;
    	}
    	Count ++;
    }

    for(i=0;i<8;i++)
    {
    	for(j=0; j<4;j++)
    	{
    		default_value[4*i+j] = nci->readretry_value[4*i+j];
    	}
    }

    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m1_setdefaultparam(struct _nand_chip_info *nci)
{
    s32 ret = 0;
    u32 i;
	u8 default_value[8];

    for(i=0; i<m1_read_retry_reg_num; i++)
    {
    	default_value[i] = nci->readretry_value[i];
    }
    ret =m1_vender_set_param(nci, (u8 *)&default_value[0], (u8 *)&m1_read_retry_reg_adr[0], m1_read_retry_reg_num);

    PHY_DBG("set retry default value: ");
    for(i=0;i<m1_read_retry_reg_num;i++)
    {
    	PHY_DBG(" %x",default_value[i]);
    }
    PHY_DBG("\n");

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m1_init_readretry_param(struct _nand_chip_info *nci)
{
	u32 i, j;
	u8 default_value[64];
	u8 oob_buf[64];
	u8 *oob=NULL, *pdata=NULL;
	s32 ret, otp_ok_flag = 0;
	u8 *readbuf;
	struct _nand_physic_op_par npo;

    oob = (__u8 *)(oob_buf);

	pdata = nand_get_temp_buf(nci->npi->sect_cnt_per_page<<9);
	if (!pdata)
		return ERR_NO_77;

	readbuf = nand_get_temp_buf(PHY_INFO_SIZE);
	if (!readbuf)
	{
		nand_free_temp_buf(pdata,nci->npi->sect_cnt_per_page<<9);
		PHY_ERR("[PHY_GetDefaultParam]:readbuf malloc fail\n");
		return ERR_NO_76;
	}

    while(1)
    {
    	otp_ok_flag = 0;
    	otp_ok_flag = m1_get_hynix_special_info(readbuf,pdata,32,nci->chip_no);
    	if(otp_ok_flag == 0)
    	{
    		PHY_DBG("ch %d, chip %d Read Retry value Table from uboot\n", nci->nctri->channel_id,npo.chip);

    		for(j = 0;j<32; j++)
    		{
    			PHY_DBG("0x%x ", pdata[j]);
    			if(j%8 == 7)
    				PHY_DBG("\n");
    		}

    		m1_saveotpvalue(nci, pdata);

    		m1_setdefaultparam(nci);

    		break;
    	}
    	else
    	{
    		PHY_DBG("[PHY_DBG] ch %d, can't get right otp value from nand otp blocks, then use otp command\n", nci->nctri->channel_id);
    		m1_getotpparam(nci,(u8 *)&default_value);
    		m1_setdefaultparam(nci);

    		PHY_DBG("[PHY_DBG] repair ch %d chip %d otp value end\n", nci->nctri->channel_id,npo.chip);
			break;
    	}
    }

	nand_free_temp_buf(readbuf,PHY_INFO_SIZE);
    nand_free_temp_buf(pdata,nci->npi->sect_cnt_per_page<<9);
    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m1_readretry_init(struct _nand_chip_info *nci)
{
	u32 i;
	//init
	m1_read_retry_mode = ((nci->npi->read_retry_type)>>16)&0xff;
	m1_read_retry_cycle =((nci->npi->read_retry_type)>>8)&0xff;
	m1_read_retry_reg_num = ((nci->npi->read_retry_type)>>0)&0xff;

	if(0x4 == m1_read_retry_mode) //mode3  H27UCG8T2ETR
	{
		m1_read_retry_reg_adr[0] = 0x38;
		m1_read_retry_reg_adr[1] = 0x39;
		m1_read_retry_reg_adr[2] = 0x3A;
		m1_read_retry_reg_adr[3] = 0x3B;
	}
	else
	{
		PHY_ERR("NFC_ReadRetryInit, unknown read retry mode 0x%x\n", m1_read_retry_mode);
		return ERR_NO_75;
	}

	nci->retry_count = 0;
	for(i=0;i<128;i++)
	{
		nci->readretry_value[i] = 0;
	}

	m1_init_readretry_param(nci);

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m1_readretry_exit(struct _nand_chip_info *nci)
{

	PHY_DBG("m1_readretry_exit. \n");

	m1_setdefaultparam(nci);

	nci->retry_count = 0;

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m1_set_readretry(struct _nand_chip_info *nci)
{
    u32 i;
    s32 ret=0;
    u8 param[8];
    //u8 param_debug[8];

	if(nci->retry_count > m1_read_retry_cycle)
	{
		return ERR_NO_74;
	}

    for(i=0; i<m1_read_retry_reg_num; i++)
    	param[i] = nci->readretry_value[4*nci->retry_count+i];

    ret =m1_vender_set_param(nci, &param[0], &m1_read_retry_reg_adr[0], m1_read_retry_reg_num);
#if 0
    PHY_DBG("rr_para:");
    for(i=0; i<m1_read_retry_reg_num; i++)
    {
    	PHY_DBG(" %x ", param[i]);
    }
    PHY_DBG("\n");

//    for(j=0;j<1;j++)
//    {
//    	m1_vender_get_param(nci, &param_debug[0], &m1_read_retry_reg_adr[0], m1_read_retry_reg_num);
//    	PHY_DBG("rr_para_debug:");
//    	for(i=0; i<m1_read_retry_reg_num; i++)
//    	{
//    		PHY_DBG(" %x ", param_debug[i]);
//    	}
//    	PHY_DBG("\n");
//    }

#endif

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m1_special_init(void)
{
    int ret = 0;
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
        ret |= m1_readretry_init(nci);
		nci = nci->nsi_next;
	}

    if(ret == 0)
    {
        function_read_page_end = m1_read_page_end;
	    PHY_DBG(" m1_special_init m1_read_retry_mode: %d m1_read_retry_cycle:%d m1_read_retry_reg_num:%d\n",m1_read_retry_mode,m1_read_retry_cycle,m1_read_retry_reg_num);
	}
	else
	{
	    PHY_ERR(" m1_special_init error m1_read_retry_mode: %d m1_read_retry_cycle:%d m1_read_retry_reg_num:%d\n",m1_read_retry_mode,m1_read_retry_cycle,m1_read_retry_reg_num);
	}
    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m1_special_exit(void)
{
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
		m1_readretry_exit(nci);
		nci = nci->nsi_next;
	}
	PHY_DBG(" m1_special_exit \n");
    return 0;
}

int m1_is_lsb_page(__u32 page_num)
{
	__u32 pages_per_block;
	__u32 read_retry_type,read_retry_mode;
	struct _nand_chip_info *nci = g_nsi->nci;

	pages_per_block = nci->page_cnt_per_blk;

	if(page_num==0)
		return 1;
	if(page_num==pages_per_block-1)
		return 0;
	if(page_num%2 == 1)
		return 1;
	return 0;
}

s32 m1_get_hynix_special_info(u8 * readbuf,u8 * dstbuf,u32 len,u32 chip)
{
	u32 copy,i;
	s32 ret,ret_sum,ret_flag ;
	u32 pages_offset;
	u32 start_block,block_per_copy;
	struct _boot_info *tboot;
	u8 sum;

	PHY_DBG("get_hynix_special_info start!!\n");

	ret = physic_info_get_offset(&pages_offset);
	if(ret)
	{
		PHY_ERR("hynix_special_info:can't find uboot head\n");
		return 1;
	}

    ret_flag = -1;

	for(start_block=UBOOT_START_BLOCK_BIGNAND; start_block <= UBOOT_MAX_BLOCK_NUM; start_block++)
	{
		PHY_DBG("physic info start_block %d\n",start_block);

		tboot = (struct _boot_info*)readbuf;
		if(tboot->nand_special_info.data[0] != 0xa5)
		{
		    physic_info_get_one_copy(start_block,pages_offset,&block_per_copy,(unsigned int  *)readbuf);  //cheney modify
		}
		
//		PHY_DBG("start_block %d pages_offset %d block_per_copy %d\n",start_block,pages_offset,block_per_copy);
		#if 0
		{
			__u32 i;
			for(i=1;i<2049;i++)
			{
				if((i%8)==0)
					PHY_DBG("\n");
				PHY_DBG(" %08x",*((__u32 *)phyinfo_buf+i-1));
			}
			PHY_DBG("\n");
			PHY_DBG("block_per_copy %d\n",block_per_copy);
		}
		#endif

		tboot = (struct _boot_info*)readbuf;
		if(tboot->nand_special_info.data[0] == 0xa5)
		{
			for(sum=0,i=2;i<1024;i++)
			{
				sum += tboot->nand_special_info.data[i];
			}
			if(sum == tboot->nand_special_info.data[1])
			{
				ret_flag = 0;
				MEMCPY(dstbuf,&tboot->nand_special_info.data[2+chip*len],len);
				#if 0
				{
					__u32 j;
					for(j=0;j<128;j++)
					{
						if((j%8)==0)
							PHY_DBG("\n");
						PHY_DBG(" %08x",tboot->nand_special_info.data[j]);
					}
					PHY_DBG("\n");
					//PHY_DBG("block_per_copy %d\n",block_per_copy);
				}
				#endif
				break;
			}
			else
			{
				tboot->nand_special_info.data[0] = 0;
			}
		}
	}
	
	return ret_flag;
}


