/*
************************************************************************************************************************
*                                                      eNand
*                                           Nand flash driver scan module
*
*                             Copyright(C), 2008-2009, SoftWinners Microelectronic Co., Ltd.
*                                                  All Rights Reserved
*
* File Name : nand_chip_for_boot.c
*
* Author :
*
* Version : v0.1
*
* Date : 2013-11-20
*
* Description :
*
* Others : None at present.
*
*
*
************************************************************************************************************************
*/
#define _NCFRR2_C_

#include "../nand_physic_inc.h"

u8 m2_read_retry_mode = 0;
u8 m2_read_retry_cycle = 0;
u8 m2_read_retry_reg_num = 0;

u8 m2_read_retry_reg_adr[8] = {0};

u8 m2_read_retry_reg_adr_1[8] = {0xCC,0xBF,0xAA,0xAB,0xCD,0xAD,0xAE,0xAF};
u8 m2_read_retry_reg_adr_2[8] = {0xB0,0xB1,0xB2,0xB3,0xB4,0xB5,0xB6,0xB7};

u8 m2_lsb_mode_reg_adr[4] = {0};
u8 m2_lsb_mode_default_val[4] = {0};
u8 m2_lsb_mode_val[4] = {0};
u8 m2_lsb_mode_reg_num = 0;

extern int is_nouse_page(u8* buf);

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0/ECC_LIMIT:ok
*Note         :
*****************************************************************************/
int m2_read_page_end(struct _nand_physic_op_par* npo)
{
	int i=0, ret = 0;
    struct _nand_chip_info *nci =  nci_get_from_nsi(g_nsi, npo->chip);

    ret = m0_read_page_end_not_retry(npo);

//    if(nand_physic_temp1 == 1)
//       ret = ERR_ECC;

    if(ret == ERR_ECC)
	{
	    PHY_DBG("m2 retry!\n");
	    for(i=0; i<m2_read_retry_cycle; i++)
	    {
	        ret = 0;
	        nci->retry_count ++;
	        if(nci->retry_count > m2_read_retry_cycle)
	        {
	            nci->retry_count = 0;
	        }

	        ret = m2_set_readretry(nci);
	        if(ret != 0)
	        {
                nci->retry_count = 0;
	            m2_set_readretry(nci);
	            break;
	        }

	        ret = m0_read_page_start(npo);
	        ret |= m0_read_page_end_not_retry(npo);

//            if(nand_physic_temp1 == 1)
//                ret = ERR_ECC;
            if(is_nouse_page(npo->sdata) == 1)
            {
                ret = ERR_ECC;
                PHY_DBG("retry spare all 0xff! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
                continue;
            }

	        if((ret == ECC_LIMIT) || (ret == 0))
	        {
				ret = ECC_LIMIT;
	            PHY_DBG("sdata:0x%x %x %x %x ",npo->sdata[1],npo->sdata[2],npo->sdata[3],npo->sdata[4]);
	            PHY_DBG("m2 ReadRetry ok! ch =%d, chip = %d  block = %d, page = %d, RetryCount = %d  \n", (u32)nci->nctri->channel_id, (u32)nci->nctri_chip_no, (u32)npo->block, (u32)npo->page, (u32)nci->retry_count);
	            break;
	        }
	    }

	    //if(i == m2_read_retry_cycle)
	    {
	        nci->retry_count = 0;
	        m2_set_readretry(nci);
	    }
	}
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m2_vender_get_param(struct _nand_chip_info *nci, u8 *para, u8 *addr, u32 count)
{
    __u32 i;
    __s32 ret = 0;
    u8 cmd_r;

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

    cmd_r = 0x37;

//    for (i=0; i<count; i++)
//	{
//    	*nci->nctri->nreg.reg_byte_cnt = 1;
//		_set_addr(nci->nctri, 1, &addr[i]);
//		cfg = cmd_r;
//		cfg |= (NDFC_SEND_ADR | NDFC_DATA_TRANS | NDFC_SEND_CMD1 );
//		ret |= ndfc_wait_cmdfifo_free(nci->nctri);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ret = ndfc_wait_cmd_finish(nci->nctri);
//		*(para+i) = *((u8 *)nci->nctri->nreg.reg_ram0_base);
//	}

    for (i=0; i<count; i++)
	{
        get_data_with_nand_bus_one_cmd(nci,&cmd_r,addr+i,para+i,1);
	}

    nand_disable_chip(nci);

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m2_vender_set_param(struct _nand_chip_info *nci, u8 *para, u8 *addr, u32 count)
{
    u32 i;
    u8 cmd_w, cmd_end;
    s32 ret = 0;

    cmd_w = 0x36;
    cmd_end = 0x16;

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

    for(i=0; i<count; i++)
	{
//		*nci->nctri->nreg.reg_byte_cnt = 1;
//		*nci->nctri->nreg.reg_ram0_base = para[i];
//		_set_addr(nci->nctri, 1, &addr[i]);
//		cfg = cmd_w;
//		cfg |= (NDFC_SEND_ADR | NDFC_DATA_TRANS | NDFC_ACCESS_DIR | NDFC_SEND_CMD1 | NDFC_WAIT_FLAG);
//		ret = ndfc_wait_cmdfifo_free(nci->nctri);
//		*nci->nctri->nreg.reg_cmd = cfg;
//		ret = ndfc_wait_cmd_finish(nci->nctri);

		set_cmd_with_nand_bus(nci,&cmd_w,0,&addr[i],para+i,1,1);
		set_one_cmd(nci,cmd_end,0);
	}
    nand_disable_chip(nci);

    //PHY_DBG("rr value %x %x %x %x!\n",para[0],para[1],para[2],para[3]);
	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m2_lsb_init(struct _nand_chip_info *nci)
{
	//init
	m2_read_retry_mode = ((nci->npi->read_retry_type)>>16)&0xff;
	m2_read_retry_cycle =((nci->npi->read_retry_type)>>8)&0xff;
	m2_read_retry_reg_num = ((nci->npi->read_retry_type)>>0)&0xff;

	if(m2_read_retry_mode == 2) //mode2
	{
		//set lsb mode
		m2_lsb_mode_reg_num = 4;

		m2_lsb_mode_reg_adr[0] = 0xB0;
		m2_lsb_mode_reg_adr[1] = 0xB1;
		m2_lsb_mode_reg_adr[2] = 0xA0;
		m2_lsb_mode_reg_adr[3] = 0xA1;

		m2_lsb_mode_val[0] = 0x0A;
		m2_lsb_mode_val[1] = 0x0A;
		m2_lsb_mode_val[2] = 0x0A;
		m2_lsb_mode_val[3] = 0x0A;
	}
	else if(m2_read_retry_mode == 3) //mode2
	{
		//set lsb mode
		m2_lsb_mode_reg_num = 4;

		m2_lsb_mode_reg_adr[0] = 0xA0;
		m2_lsb_mode_reg_adr[1] = 0xA1;
		m2_lsb_mode_reg_adr[2] = 0xA7;
		m2_lsb_mode_reg_adr[3] = 0xA8;

		m2_lsb_mode_val[0] = 0x0A;
		m2_lsb_mode_val[1] = 0x0A;
		m2_lsb_mode_val[2] = 0x0A;
		m2_lsb_mode_val[3] = 0x0A;
	}

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m2_lsb_enable(struct _nand_chip_info *nci)
{
    u32 i;
    u8 value[4];

    m2_vender_get_param(nci,&m2_lsb_mode_default_val[0], &m2_lsb_mode_reg_adr[0], m2_lsb_mode_reg_num);

    for(i=0;i<m2_lsb_mode_reg_num;i++)
    {
    	value[i] = m2_lsb_mode_default_val[i];
    	value[i] += m2_lsb_mode_val[i];
    }

    m2_vender_set_param(nci,&value[0], &m2_lsb_mode_reg_adr[0], m2_lsb_mode_reg_num);

    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m2_lsb_disable(struct _nand_chip_info *nci)
{
    m2_vender_set_param(nci,&m2_lsb_mode_default_val[0], &m2_lsb_mode_reg_adr[0], m2_lsb_mode_reg_num);

    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m2_lsb_exit(struct _nand_chip_info *nci)
{
	s32 timeout = 0xfffff;

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

//    cfg = 0;
//    _set_addr(nci->nctri, 5, &addr[0]);
//    cfg = (NDFC_SEND_CMD1 | NDFC_SEND_ADR |(4<<16) | 0x00);
//    ndfc_wait_cmdfifo_free(nci->nctri);
//    *nci->nctri->nreg.reg_cmd = cfg;
//    ndfc_wait_cmd_finish(nci->nctri);
//
//    /*set 0x30*/
//    cfg = 0;
//    cfg = (NDFC_SEND_CMD1 | NDFC_WAIT_FLAG | 0x30);
//    ndfc_wait_cmdfifo_free(nci->nctri);
//    *nci->nctri->nreg.reg_cmd = cfg;
//    ndfc_wait_cmd_finish(nci->nctri);

    //dumy read
    set_one_cmd(nci,0x00,0);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_cmd(nci,0x30,1);

    while(timeout-- > 0) ;
    PHY_DBG("m2_lsb_exit\n");

    nand_disable_chip(nci);

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m2_vender_get_param_otp_hynix(struct _nand_chip_info *nci)
{
    u32 i, j;
    s32 error_flag,ret = 0;
    u8 cmd[1];
    u8 param_reverse[64];
    u8 reg_addr[2] = {0x0, 0x0};
    u8 w_data[2] = {0x0, 0x0};

    u8* buf;

	if(m2_read_retry_mode == 2)
	{
		reg_addr[0] = 0xFF;
		reg_addr[1] = 0xCC;
		w_data[0] = 0x40;
		w_data[1] = 0x4D;
	}
	else if(m2_read_retry_mode == 3)
	{
		reg_addr[0] = 0xAE;
		reg_addr[1] = 0xB0;
		w_data[0] = 0x00;
		w_data[1] = 0x4D;
	}
	else
	{
	    return ERR_NO_97;
	}

    buf = nand_get_temp_buf(1024);

    nand_enable_chip(nci);
    ndfc_disable_randomize(nci->nctri);

	 // send 0xFF cmd
//	cfg = 0;
//	cfg = (NDFC_SEND_CMD1 | NDFC_WAIT_FLAG| 0xff);
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	cfg = 0;
//	address[0] = reg_addr[0];
//	//send cmd 0x36, addr 0xae/0xff, data 0x00/0x40
//	*nci->nctri->nreg.reg_byte_cnt = 1;
//	*nci->nctri->nreg.reg_ram0_base = w_data[0];
//	_set_addr(nci->nctri, 1, &address[0]);
//	cfg = (NDFC_SEND_CMD1 | NDFC_DATA_TRANS |NDFC_ACCESS_DIR | NDFC_SEND_ADR |0x36);
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	//send addr 0xCC
//	cfg = 0;
//	address[0] = reg_addr[1];
//	_set_addr(nci->nctri, 1, &address[0]);
//	cfg = (NDFC_SEND_ADR);
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	//send data 0x4D
//	cfg = 0;
//	*nci->nctri->nreg.reg_byte_cnt = 1;
//	*nci->nctri->nreg.reg_ram0_base = w_data[1];
//	cfg = (NDFC_DATA_TRANS | NDFC_ACCESS_DIR);
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	//send cmd 0x16, 0x17, 0x04, 0x19, 0x00
//	cfg = 0;
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	cfg = (NDFC_SEND_CMD1|0x16);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	cfg = 0;
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	cfg = (NDFC_SEND_CMD1|0x17);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	cfg = 0;
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	cfg = (NDFC_SEND_CMD1|0x04);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	cfg = 0;
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	cfg = (NDFC_SEND_CMD1|0x19);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	cfg = 0;
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	cfg = (NDFC_SEND_CMD1|0x00);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	 //send addr 00, 00, 00, 02, 00
//	cfg = 0;
//	address[0] = 0x00;
//	address[1] = 0x00;
//	address[2] = 0x00;
//	address[3] = 0x02;
//	address[4] = 0x00;
//	_set_addr(nci->nctri, 5, &address[0]);
//	cfg = (NDFC_SEND_ADR|(0x4<<16));
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	cfg = 0;
//	//send cmd 0x30, read data
//	*nci->nctri->nreg.reg_byte_cnt = 2;
//	cfg = (NDFC_SEND_CMD1|NDFC_WAIT_FLAG|NDFC_DATA_TRANS|0x30);
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);
//
//	//get param data
//    if ((*((u8 *)nci->nctri->nreg.reg_ram0_base)!=0x08)||(*((u8 *)nci->nctri->nreg.reg_ram0_base + 1)!=0x08))
//    {
//        PHY_ERR("hynix OTP RegCount value error: 0x%x, 0x%x \n",*((u8 *)nci->nctri->nreg.reg_ram0_base), *((u8 *)nci->nctri->nreg.reg_ram0_base + 1));
//        ret = ERR_NO_96;
//    }
//
//    cfg = 0;
//	*nci->nctri->nreg.reg_byte_cnt = 1024;
//	cfg = (NDFC_DATA_TRANS);
//	ndfc_wait_cmdfifo_free(nci->nctri);
//	*nci->nctri->nreg.reg_cmd = cfg;
//	ndfc_wait_cmd_finish(nci->nctri);


	set_one_cmd(nci,0xff,1);
	cmd[0] = 0x36;
	set_cmd_with_nand_bus(nci,cmd,0,reg_addr,w_data,1,1);
    set_cmd_with_nand_bus(nci,NULL,0,&reg_addr[1],&w_data[1],1,1);
    set_one_cmd(nci,0x16,0);
    set_one_cmd(nci,0x17,0);
    set_one_cmd(nci,0x04,0);
    set_one_cmd(nci,0x19,0);
    set_one_cmd(nci,0x00,0);

    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x00);
    set_one_addr(nci,0x02);
    set_one_addr(nci,0x00);

    cmd[0] = 0x30;
    get_data_with_nand_bus_one_cmd(nci,cmd,NULL,buf,2);

    if((buf[0] != 0x08) || (buf[1] != 0x08))
    {
        PHY_ERR("hynix OTP RegCount value error: 0x%x, 0x%x \n",buf[0], buf[1]);
        ret = ERR_NO_96;
    }

    get_data_with_nand_bus_one_cmd(nci,NULL,NULL,buf,1024);

    //check data vaild
    for(j=0;j<8;j++)
    {
        error_flag = 0;
        for(i=0;i<64;i++)
        {
            nci->readretry_value[i] = *(buf+128*j+i);
            param_reverse[i] = *(buf+128*j+64+i);
            if((nci->readretry_value[i] + param_reverse[i])!= 0xff)
            {
                error_flag = 1;
                break;
            }
        }
        if(error_flag == 0)
        {
        	PHY_DBG("otp copy %d is ok!\n",j);
        	break;
        }
    }

    if(error_flag)
    {
        PHY_DBG("otp copy failed!\n");
        ret = ERR_NO_95;
    }

    set_one_cmd(nci,0xff,1);
    set_one_cmd(nci,0x38,1);

    nand_disable_chip(nci);

    nand_free_temp_buf(buf,1024);

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
void m2_saveotpvalue(struct _nand_chip_info *nci, __u8* otp_value)
{
    u32 i;

    for(i = 0;i<64; i++)
    {
        nci->readretry_value[i] = otp_value[i];
    }
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m2_getotpparam(struct _nand_chip_info *nci, u8* default_value)
{
    s32 ret;
    u32 i, j, Count;

    for(Count =0; Count<5; Count++)
    {
    	PHY_DBG("_vender_get_param_otp_hynix time %d!\n", Count);
    	ret = m2_vender_get_param_otp_hynix(nci);
    	if(!ret)
    		break;
    }
    if(ret)
    {
    	PHY_ERR("_vender_get_param_otp_hynix error!\n");
    	return ret;
    }

    //set read retry level
    for(i=0;i<8;i++)
    {
    	for(j=0; j<8;j++)
    	{
    		default_value[8*i+j] = nci->readretry_value[8*i+j];
    	}
    }

    return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m2_setdefaultparam(struct _nand_chip_info *nci)
{
    s32 ret = 0;
    u32 i;
	u8 default_value[8];

    for(i=0; i<m2_read_retry_reg_num; i++)
    {
    	default_value[i] = nci->readretry_value[i];
    }
    ret =m2_vender_set_param(nci, default_value, m2_read_retry_reg_adr, m2_read_retry_reg_num);
    PHY_DBG("set retry default value: ");
    for(i=0;i<m2_read_retry_reg_num;i++)
    {
    	PHY_DBG(" %x",default_value[i]);
    }
    PHY_DBG("\n");

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m2_init_readretry_param(struct _nand_chip_info *nci)
{
	u32 i, j;
	u8 default_value[64];
	u8 oob_buf[64];
	u8 *oob=NULL, *pdata=NULL;
	u8 *readbuf=NULL;
	s32 ret = 0, otp_ok_flag = 0;
	struct _nand_physic_op_par npo;

    oob = (__u8 *)(oob_buf);
	pdata = (__u8 *)nand_get_temp_buf(nci->npi->sect_cnt_per_page * 512);
	if (!pdata)
	{
	    PHY_DBG("m2 get default param error !\n");
		return ERR_NO_94;
	}

	readbuf = nand_get_temp_buf(PHY_INFO_SIZE);
	if (!readbuf)
	{
		nand_free_temp_buf(pdata,nci->npi->sect_cnt_per_page<<9);
		PHY_ERR("[PHY_GetDefaultParam]:readbuf malloc fail\n");
		return ERR_NO_76;
	}
	
    while(1)
    {
    	otp_ok_flag = 0;
    	otp_ok_flag = m1_get_hynix_special_info(readbuf,pdata,64,nci->chip_no);

    	if(otp_ok_flag == 0)
    	{
    		PHY_DBG("ch %d, chip %d Read Retry value Table from uboot:\n", nci->nctri->channel_id,npo.chip);

    		for(j = 0;j<64; j++)
    		{
    			PHY_DBG("0x%x ", pdata[j]);
    			if(j%8 == 7)
    				PHY_DBG("\n");
    		}

    		m2_saveotpvalue(nci, pdata);

    		ret = m2_setdefaultparam(nci);

    		break;
    	}
    	else
    	{
    		PHY_DBG("[PHY_DBG] ch %d, can't get right otp value from nand otp blocks, then use otp command\n", nci->nctri->channel_id);
    		ret = m2_getotpparam(nci,(u8 *)&default_value);
    		if(ret != 0)
    		{
    		    goto m2_get_param_end;
    		}

    		m2_setdefaultparam(nci);

    		PHY_DBG("[PHY_DBG] repair ch %d otp value end\n", nci->nctri->channel_id);
			break;
    	}
    }

m2_get_param_end:

    nand_free_temp_buf(pdata,nci->npi->sect_cnt_per_page * 512);

    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m2_readretry_init(struct _nand_chip_info *nci)
{
	s32 i,ret;

	//init
	m2_read_retry_mode = ((nci->npi->read_retry_type)>>16)&0xff;
	m2_read_retry_cycle =((nci->npi->read_retry_type)>>8)&0xff;
	m2_read_retry_reg_num = ((nci->npi->read_retry_type)>>0)&0xff;

	if(0x2 == m2_read_retry_mode) //mode2  H27UCG8T2ATR
	{
		m2_read_retry_reg_adr[0] = 0xCC;
		m2_read_retry_reg_adr[1] = 0xBF;
		m2_read_retry_reg_adr[2] = 0xAA;
		m2_read_retry_reg_adr[3] = 0xAB;
		m2_read_retry_reg_adr[4] = 0xCD;
		m2_read_retry_reg_adr[5] = 0xAD;
		m2_read_retry_reg_adr[6] = 0xAE;
		m2_read_retry_reg_adr[7] = 0xAF;
	}
	else if(0x3 == m2_read_retry_mode) //mode2  H27UCG8T2ATR
	{
		m2_read_retry_reg_adr[0] = 0xB0;
		m2_read_retry_reg_adr[1] = 0xB1;
		m2_read_retry_reg_adr[2] = 0xB2;
		m2_read_retry_reg_adr[3] = 0xB3;
		m2_read_retry_reg_adr[4] = 0xB4;
		m2_read_retry_reg_adr[5] = 0xB5;
		m2_read_retry_reg_adr[6] = 0xB6;
		m2_read_retry_reg_adr[7] = 0xB7;
	}
	else
	{
		PHY_ERR("NFC_ReadRetryInit, unknown read retry mode 0x%x\n", m2_read_retry_mode);
		return ERR_NO_93;
	}

	nci->retry_count = 0;
	for(i=0;i<128;i++)
	{
		nci->readretry_value[i] = 0;
	}

	ret = m2_init_readretry_param(nci);

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m2_readretry_exit(struct _nand_chip_info *nci)
{
	PHY_DBG("m2_readretry_exit. \n");

	m2_setdefaultparam(nci);

	nci->retry_count = 0;

	return 0;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
s32 m2_set_readretry(struct _nand_chip_info *nci)
{
    u32 i;
    s32 ret=0;
    u8 param[8];

	if(nci->retry_count > m2_read_retry_cycle)
	{
		return ERR_NO_92;
	}

    for(i=0; i<m2_read_retry_reg_num; i++)
    {
    	param[i] = nci->readretry_value[8*nci->retry_count+i];
    }

    ret =m2_vender_set_param(nci, param, m2_read_retry_reg_adr, m2_read_retry_reg_num);

    PHY_DBG("retry para:");
    for(i=0; i<m2_read_retry_reg_num; i++)
    {
    	PHY_DBG(" %x ", param[i]);
    }
    PHY_DBG("\n");

//    for(j=0;j<1;j++)
//    {
//    	m2_vender_get_param(nci, &param_debug[0], &m2_read_retry_reg_adr[0], m2_read_retry_reg_num);
//    	for(i=0; i<m2_read_retry_reg_num; i++)
//    	{
//    		PHY_DBG("rr_para_debug %x \n", param_debug[i]);
//    	}
//    }

	return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m2_special_init(void)
{
    int ret = 0;
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
        ret |= m2_readretry_init(nci);
		nci = nci->nsi_next;
	}

    if(ret == 0)
    {
	    function_read_page_end = m2_read_page_end;
	    PHY_DBG(" m2_special_init m2_read_retry_mode: %d m2_read_retry_cycle:%d m2_read_retry_reg_num:%d\n",m2_read_retry_mode,m2_read_retry_cycle,m2_read_retry_reg_num);
	}
	else
	{
	    PHY_ERR(" m2_special_init error m2_read_retry_mode: %d m2_read_retry_cycle:%d m2_read_retry_reg_num:%d\n",m2_read_retry_mode,m2_read_retry_cycle,m2_read_retry_reg_num);
	}
    return ret;
}

/*****************************************************************************
*Name         :
*Description  :
*Parameter    :
*Return       : 0:ok  -1:fail
*Note         :
*****************************************************************************/
int m2_special_exit(void)
{
	struct _nand_chip_info *nci = g_nsi->nci;

	while(nci != NULL)
	{
		m2_readretry_exit(nci);
		nci = nci->nsi_next;
	}
	PHY_DBG(" m2_special_exit \n");
    return 0;
}

int m2_is_lsb_page(__u32 page_num)
{
	struct _nand_chip_info *nci = g_nsi->nci;

	//hynix 20nm
	if((page_num==0)||(page_num==1))
		return 1;
	if((page_num==nci->npi->page_cnt_per_blk-2)||(page_num==nci->npi->page_cnt_per_blk-1))
		return 0;
	if((page_num%4 == 2)||(page_num%4 == 3))
		return 1;
	return 0;	
}


