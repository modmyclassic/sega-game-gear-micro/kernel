#include <linux/module.h>
#include <linux/init.h>
#include <linux/input.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <asm/irq.h>
#include <linux/io.h>
#include <linux/timer.h>
#include <linux/clk.h>
#include <linux/irq.h>
#include <linux/platform_device.h>
#include <linux/of_platform.h>
#include <linux/of_irq.h>
#include <linux/of_address.h>

#include "sunxi_misc_tpadc.h"

struct sunxi_tpadc_data {
	struct clk		*clk;
	struct platform_device *pdev;
	void __iomem *reg_base;
	int irq_num;
	u32 channel;
	u32 value;
};
static struct sunxi_tpadc_data *tpadc_data;

static u32 tpadc_read(void __iomem *reg_offset)
{
	u32 reg_val = 0;

	reg_val = readl(reg_offset);

	return reg_val;
}

static void tpadc_write(void __iomem *reg_base, u32 val)
{
	writel(val, reg_base);
}

static u32 tpadc_channel_select(void __iomem *reg_base, unsigned int index)
{
	u32 val;

	val = tpadc_read((void __iomem *)(reg_base + TP_CTRL1));
	val &= ~(0x0f);
	
	switch (index) {
	case 0:
		val |= ADC_CHAN0_SELECT;
		break;
	case 1:
		val |= ADC_CHAN1_SELECT;
		break;
	case 2:
		val |= ADC_CHAN2_SELECT;
		break;
	case 3:
		val |= ADC_CHAN3_SELECT;
		break;
	default:
		pr_err("%s, invalid channel index!", __func__);
		return -EINVAL;
	}
	tpadc_write((void __iomem *)(reg_base + TP_CTRL1), val);

	return 0;
}

static ssize_t channel_store(struct device *dev,
		struct device_attribute *attr, const char *buf, size_t count)
{
	unsigned int index;
	ssize_t ret;

	ret = kstrtouint(buf, 10, &index);
	if (ret < 0)
		return ret;

	ret = tpadc_channel_select(tpadc_data->reg_base, index);
	if (ret < 0)
		return ret;

	tpadc_data->channel = index;
	ret = count;
	return ret;
}

static ssize_t channel_show(struct device *dev, struct device_attribute *attr,
			char *buf)
{
	u32 data = 0;
	memset(buf, 0, PAGE_SIZE);

	data = tpadc_data->channel;
	return snprintf(buf, PAGE_SIZE, "%u\n", data);
}

static ssize_t value_show(struct device *dev, struct device_attribute *attr,
			char *buf)
{
	u32 data = 0;
	memset(buf, 0, PAGE_SIZE);

	data = tpadc_data->value;
	return snprintf(buf, PAGE_SIZE, "%u\n", data);
}

static struct device_attribute channel_attr = __ATTR_RW(channel);
static struct device_attribute value_attr = __ATTR_RO(value);

static int tpadc_data_hw_init(void __iomem *reg_base)
{
	u32 val = 0;

	val = 0XF<<24;
	val |= 1<<23;
	val &= ~(1<<22); /*sellect HOSC(24MHz)*/
	val |= 0x3<<20; /*00:CLK_IN/2,01:CLK_IN/3,10:CLK_IN/6,11:CLK_IN/1*/
	val |= 0x7f<<0; /*FS DIV*/
	tpadc_write((void __iomem *)(reg_base + TP_CTRL0), val);

	val = TP_MODE_SELECT | TP_EN;
	tpadc_write((void __iomem *)(reg_base + TP_CTRL1), val);

	val = 1<<16; /* enable date irq */
	tpadc_write((void __iomem *)(reg_base + TP_INT_FIFO_CTRL), val);

	/* select X1 as default channel */
	tpadc_data->channel = 0;
	tpadc_channel_select(tpadc_data->reg_base, tpadc_data->channel);

	/* clear fifo */
	val = tpadc_read((void __iomem *)(reg_base + TP_INT_FIFO_CTRL));
	val |= 1<<4;
	tpadc_write((void __iomem *)(reg_base + TP_INT_FIFO_CTRL), val);

	return 0;
}

static int sunxi_tpadc_dts_parse(struct device *pdev,
		struct sunxi_tpadc_data *tpadc_data)
{
	struct device_node *np = NULL;
	int ret = 0;

	pr_info("%s:sunxi_tpadc_dts_parse ++++++\n", __func__);

	if (!pdev) {
		pr_info("%s:invalid device pointer\n", __func__);
		return -EINVAL;
	}
	if (!tpadc_data) {
		pr_info("%s:invalid tp key data pointer\n", __func__);
		return -EINVAL;
	}

	np = pdev->of_node;
	if (!np) {
		pr_info("%s:invalid device node\n", __func__);
		return -EINVAL;
	}

	tpadc_data->reg_base = of_iomap(np, 0);
	if (NULL == tpadc_data->reg_base) {
		pr_info("%s:Failed to ioremap io memory region.\n", __func__);
		ret = -ENODEV;
	} else
		pr_info("key base: %p\n", tpadc_data->reg_base);

	tpadc_data->irq_num = irq_of_parse_and_map(np, 0);
	if (0 == tpadc_data->irq_num) {
		pr_info("%s:Failed to map irq.\n", __func__);
		ret = -ENODEV;
	} else
		pr_info("irq num: %d !\n", tpadc_data->irq_num);

	return 0;
}

static irqreturn_t sunxi_tpadc_isr(int irq, void *dummy)
{
	struct sunxi_tpadc_data *tpadc_data = (struct sunxi_tpadc_data *)dummy;
	u32  reg_val = 0, reg_data = 0;

	reg_val = tpadc_read((void __iomem *)
		(tpadc_data->reg_base + TP_INT_FIFO_STAT));
	reg_data = tpadc_read((void __iomem *)
		(tpadc_data->reg_base + TP_DATA));
	writel(reg_val, (void __iomem *)
		(tpadc_data->reg_base + TP_INT_FIFO_STAT));

	tpadc_data->value = reg_data;
	return IRQ_HANDLED;
}

static int isr_register(struct sunxi_tpadc_data *tpadc_data)
{
	pr_info("%s:%d\n", __func__, tpadc_data->irq_num);

	if (!tpadc_data)
		return -EINVAL;

	return request_irq(tpadc_data->irq_num,
			sunxi_tpadc_isr, 0, "sunxi-misc-tpadc", tpadc_data);
}

static void isr_unregister(struct sunxi_tpadc_data *tpadc_data)
{
	if (NULL == tpadc_data)
		return;
	free_irq(tpadc_data->irq_num, tpadc_data);
}

static int sunxi_tpadc_probe(struct platform_device *pdev)
{
	int ret = 0;

	pr_info("%s:sunxi_tpadc_probe ++++++\n", __func__);
	tpadc_data = (struct sunxi_tpadc_data *)devm_kzalloc(&pdev->dev,
			sizeof(struct sunxi_tpadc_data), GFP_KERNEL);
	if (IS_ERR_OR_NULL(tpadc_data)) {
		pr_info("tpadc_data: not enough memory for battery data\n");
		return -ENOMEM;
	}

	ret = sunxi_tpadc_dts_parse(&pdev->dev, tpadc_data);
	if (ret != 0)
		return ret;

	/* hardware setting */
	if (tpadc_data->clk)
		clk_prepare_enable(tpadc_data->clk);
	tpadc_data_hw_init(tpadc_data->reg_base);
	platform_set_drvdata(pdev, tpadc_data);

	device_create_file(&pdev->dev, &channel_attr);
	device_create_file(&pdev->dev, &value_attr);

	if (isr_register(tpadc_data)) {
		return -EINVAL;
	}
	pr_info("%s:sunxi_tpadc_probe ------\n", __func__);
	return 0;
}

static int sunxi_tpadc_remove(struct platform_device *pdev)
{
	pr_info("%s:sunxi_tpadc_remove ------\n", __func__);

	struct sunxi_tpadc_data *tpadc_data = platform_get_drvdata(pdev);

	isr_unregister(tpadc_data);
	if (NULL != tpadc_data)
		kfree(tpadc_data);
	tpadc_data = NULL;

	return 0;
}

#ifdef CONFIG_OF
static struct of_device_id sunxi_tpadc_of_match[] = {
	{ .compatible = "allwinner,sunxi-misc-tpadc",},
	{ },
};
MODULE_DEVICE_TABLE(of, sunxi_tpadc_of_match);
#else /* !CONFIG_OF */
#endif

static struct platform_driver sunxi_tpadc_driver = {
	.probe = sunxi_tpadc_probe,
	.remove = sunxi_tpadc_remove,
	.driver = {
		   .name = "sunxi-misc-tpadc",
		   .owner = THIS_MODULE,
		   .of_match_table = of_match_ptr(sunxi_tpadc_of_match),
		   },
};

module_platform_driver(sunxi_tpadc_driver);

MODULE_AUTHOR("Zuiki <yo_bokuhei@zuiki.co.jp>");
MODULE_DESCRIPTION("sunxi-misc-tpadc driver");
MODULE_LICENSE("GPL");
