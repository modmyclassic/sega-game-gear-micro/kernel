/*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
*
* Copyright (c) 2018
*
* ChangeLog
*
*
*/
#include <linux/module.h>
#include <linux/init.h>
#include <linux/input.h>
#include <linux/delay.h>
#include <linux/slab.h>
#include <linux/interrupt.h>
#include <linux/ioport.h>
#include <asm/irq.h>
#include <linux/io.h>
#include <linux/timer.h>
#include <linux/clk.h>
#include <linux/irq.h>
#include <linux/platform_device.h>
#include <linux/of_platform.h>
#include <linux/of_irq.h>
#include <linux/of_address.h>
#include <linux/of_gpio.h>
#include <linux/gpio.h>


#if defined(CONFIG_PM)
#include <linux/pm.h>
#endif

#include "lradc_battery.h"

#define WORK_DELAY (5*1000)
#define POWER_ADC_SPMPLE_NUM 4

typedef struct capacity_voltage_table {
	u32 capacity;
	u32 voltage;
} capacity_voltage_table_t;

struct lradc_bat_data {
	u32 voltage;
	u32 capacity;
	struct platform_device *pdev;
	struct delayed_work work;
	void __iomem *reg_base;
	int	irq_num;
	int charging_gpio;
	int charging_irq;
	int is_charging;
	int high_resistor;
	int low_resistor;
	u32 cv_table_size;
	capacity_voltage_table_t *cv_table;
};

enum LRADC_CHARGE_TYPE {
	LRADC_CHARGE_AC = 1,
	LRADC_CHARGE_USB_20,
	LRADC_CHARGE_USB_30,
};

static u32 g_usb_flag = LRADC_CHARGE_AC;

static struct lradc_bat_data *battery_data;

void lradc_set_usb_type(u32 type)
{
	g_usb_flag = type;
}
EXPORT_SYMBOL_GPL(lradc_set_usb_type);

static u32 lradc_read(void __iomem *reg_offset)
{
	u32 reg_val = 0;

	reg_val = readl(reg_offset);

	return reg_val;
}

static void lradc_write(void __iomem *reg_base, u32 val)
{
	writel(val, reg_base);
}

static ssize_t voltage_show(struct device *dev, struct device_attribute *attr,
			    char *buf)
{
	return snprintf(buf, PAGE_SIZE, "%umv\n", battery_data->voltage);
}

static ssize_t capacity_show(struct device *dev, struct device_attribute *attr,
			    char *buf)
{
	return snprintf(buf, PAGE_SIZE, "%u\n", battery_data->capacity);
}

static ssize_t charging_show(struct device *dev, struct device_attribute *attr,
			     char *buf)
{
	return 0;
#if 0
	return snprintf(buf, PAGE_SIZE, "%d\n",
			(gpio_get_value(battery_data->charging_gpio)
			? g_usb_flag : 0));
#endif
}

static ssize_t adc_show(struct device *dev, struct device_attribute *attr,
			char *buf)
{
	u32 data = 0;
	memset(buf, 0, PAGE_SIZE);

	data = lradc_read(battery_data->reg_base + LRADC_DATA0);
	return snprintf(buf, PAGE_SIZE, "%u\n", data);
}

static struct device_attribute voltage_attr = __ATTR_RO(voltage);

static struct device_attribute charging_attr = __ATTR_RO(charging);

static struct device_attribute adc_attr = __ATTR_RO(adc);

static struct device_attribute capacity_attr = __ATTR_RO(capacity);

static u32 power_get_average(u32 *val, u32 num)
{
	u32 total;
	u32 i;

	if (num <= 0)
		return 0;


	total = 0;
	for (i = 0; i < num; i++)
		total += val[i];


	return total / num;
}

static u32 calc_voltage(u32 adc_val, int hr, int lr)
{
	return adc_val * 2000 / 63 * (hr + lr) / lr;
}

static u32 calc_capacity(u32 voltage, u32 table_size, capacity_voltage_table_t *cv_table)
{
	u32 cap = 0;
	int i = 0;
	for (i = 0; i < table_size; i++) {
		if ( voltage > (cv_table + i)->voltage )
			cap = (cv_table + i)->capacity;
	}

	return cap;
}

static void process_adc_val(struct lradc_bat_data *battery_data, u32 val)
{
	static u32 cur_adc_sample_num;
	static u32 adc_val[POWER_ADC_SPMPLE_NUM] = { 0 };
	u32 tmp_data = 0;
	int i = 0;

	if (cur_adc_sample_num < POWER_ADC_SPMPLE_NUM)
		adc_val[cur_adc_sample_num++] = val;
	else {
		for (i = 0; i < POWER_ADC_SPMPLE_NUM - 1; i++)
			adc_val[i] = adc_val[i + 1];
		adc_val[POWER_ADC_SPMPLE_NUM - 1] = val;
	}

	tmp_data = power_get_average(adc_val, cur_adc_sample_num);
	battery_data->voltage = calc_voltage(tmp_data, battery_data->high_resistor, battery_data->low_resistor);
	battery_data->capacity = calc_capacity(battery_data->voltage, battery_data->cv_table_size, battery_data->cv_table);
}

static void bat_work_func(struct work_struct *work)
{
	struct lradc_bat_data *battery_data =
	    container_of((struct delayed_work *)work,
			 struct lradc_bat_data, work);
	u32 data = 0;

	data = lradc_read(battery_data->reg_base + LRADC_DATA0);
	process_adc_val(battery_data, data);
	schedule_delayed_work(&battery_data->work,
		msecs_to_jiffies(WORK_DELAY));
}

static int battery_data_hw_init(void __iomem *reg_base)
{
	u32 val = 0;
	pr_info("%s:battery_data_hw_init ++++++\n", __func__);

    /* initalize hardware */
	/* initalize interrutp control regitster */
	val = 0;
	lradc_write((void __iomem *)(reg_base + LRADC_INTC), val);

	/* initalize control regitster */
	val = ADC_CHAN_SELECT | LRADC_SAMPLE_32HZ | LRADC_EN | LRADC_HOLD_EN;
	lradc_write((void __iomem *)(reg_base + LRADC_CTRL), val);

	return 0;
}

static int lradc_battery_dts_parse(struct device *pdev,
		struct lradc_bat_data *battery_data)
{
	struct device_node *np = NULL;
	int ret = 0;

	pr_info("%s:lradc_battery_dts_parse ++++++\n", __func__);

	if (!pdev) {
		pr_info("%s:invalid device pointer\n", __func__);
		return -EINVAL;
	}
	if (!battery_data) {
		pr_info("%s:invalid tp key data pointer\n", __func__);
		return -EINVAL;
	}

	np = pdev->of_node;
	if (!np) {
		pr_info("%s:invalid device node\n", __func__);
		return -EINVAL;
	}

	battery_data->reg_base = of_iomap(np, 0);
	if (NULL == battery_data->reg_base) {
		pr_info("%s:Failed to ioremap io memory region.\n", __func__);
		ret = -ENODEV;
	} else
		pr_info("key base: %p\n", battery_data->reg_base);

	battery_data->irq_num = irq_of_parse_and_map(np, 0);
	if (0 == battery_data->irq_num) {
		pr_info("%s:Failed to map irq.\n", __func__);
		ret = -ENODEV;
	} else
		pr_info("irq num: %d !\n", battery_data->irq_num);

	return 0;
}

static int fetch_sysconfig_para(struct lradc_bat_data *battery_data)
{
	int index = 0;
	u32 high_resistor = 0;
	u32 low_resistor = 0;
	u32 table_size = 0;
	u32 capacity = 0;
	u32 voltage = 0;
	char key[16]= {0};
	struct device_node *np = NULL;
	np = of_find_node_by_name(NULL,"lradc_battery");
	if (!np) {
		pr_err("ERROR! get lradc_battery failed, func:%s, line:%d\n",__FUNCTION__, __LINE__);
		return -EINVAL;
	}

	if (of_property_read_u32(np, "high_resistor", &high_resistor)) {
		pr_err("get scale failed\n");
		return -EINVAL;
	}
	if (of_property_read_u32(np, "low_resistor", &low_resistor)) {
		pr_err("get scale failed\n");
		return -EINVAL;
	}
	if (of_property_read_u32(np, "LV_count", &table_size)) {
		pr_err("get LV_count failed\n");
		return -EINVAL;
	}
	battery_data->high_resistor= high_resistor;
	battery_data->low_resistor= low_resistor;
	battery_data->cv_table_size = table_size;
	battery_data->cv_table = (capacity_voltage_table_t *) kmalloc (table_size * sizeof(capacity_voltage_table_t), GFP_KERNEL);
	if (battery_data->cv_table == NULL) {
		pr_err("create capacity voltage table failed\n");
		return -ENOMEM;
	}

	for (index = 0; index < table_size; index++) {
		sprintf(key, "LV%d_cap", index + 1);
		if (of_property_read_u32(np, key, &capacity)) {
			pr_err("get %s failed\n", key);
			return -EINVAL;
		}
		sprintf(key, "LV%d_volt", index + 1);
		if (of_property_read_u32(np, key, &voltage)) {
			pr_err("get %s failed\n", key);
			return -EINVAL;
		}
		((battery_data->cv_table) + index)->capacity = capacity;
		((battery_data->cv_table) + index)->voltage = voltage;
	}
	return 0;
}

static int lradc_battery_probe(struct platform_device *pdev)
{
	int ret = 0;
	u32 val = 0;

	pr_info("%s:lradc_battery_probe ++++++\n", __func__);
	battery_data = (struct lradc_bat_data *)devm_kzalloc(&pdev->dev,
			sizeof(struct lradc_bat_data), GFP_KERNEL);
	if (IS_ERR_OR_NULL(battery_data)) {
		pr_info("battery_data: not enough memory for battery data\n");
		return -ENOMEM;
	}

	ret = lradc_battery_dts_parse(&pdev->dev, battery_data);
	if (ret != 0)
		return ret;

	ret = fetch_sysconfig_para(battery_data);
	if (ret != 0)
		return ret;

	battery_data_hw_init(battery_data->reg_base);
	platform_set_drvdata(pdev, battery_data);

	INIT_DELAYED_WORK(&battery_data->work, bat_work_func);


	device_create_file(&pdev->dev, &voltage_attr);
	device_create_file(&pdev->dev, &adc_attr);
	device_create_file(&pdev->dev, &capacity_attr);
	device_create_file(&pdev->dev, &charging_attr);

	schedule_delayed_work(&battery_data->work,
				msecs_to_jiffies(WORK_DELAY));

	pr_info("%s:lradc_battery_probe ------\n", __func__);
	return 0;

}

static int lradc_battery_remove(struct platform_device *pdev)
{
	pr_info("%s:lradc_battery_remove ------\n", __func__);

	struct lradc_bat_data *battery_data = platform_get_drvdata(pdev);

	cancel_delayed_work_sync(&battery_data->work);
	lradc_write(battery_data->reg_base + LRADC_CTRL, 0);
	lradc_write(battery_data->reg_base + LRADC_INTC, 0);

	if (NULL != battery_data)
		kfree(battery_data);
	battery_data = NULL;

	return 0;
}

#ifdef CONFIG_OF
static struct of_device_id lradc_battery_of_match[] = {
	{ .compatible = "allwinner,LRADC_battery",},
	{ },
};
MODULE_DEVICE_TABLE(of, lradc_battery_of_match);
#else /* !CONFIG_OF */
#endif

static struct platform_driver lradc_battery_driver = {
	.probe = lradc_battery_probe,
	.remove = lradc_battery_remove,
	.driver = {
		   .name = "LRADC_battery",
		   .owner = THIS_MODULE,
		   .of_match_table = of_match_ptr(lradc_battery_of_match),
		   },
};

module_platform_driver(lradc_battery_driver);

MODULE_AUTHOR("Orange Yang <yangmiansi@allwinnertech.com>");
MODULE_DESCRIPTION("lradc-battery driver");
MODULE_LICENSE("GPL");
