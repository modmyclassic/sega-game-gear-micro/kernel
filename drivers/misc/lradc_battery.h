#ifndef __LRADC_BATTERY_H_
#define __LRADC_BATTERY_H_

#define LRADC_BASE_ADDR 0x01c23400

#define LRADC_CTRL              (0x00)
#define LRADC_INTC              (0x04)
#define LRADC_INT_STA           (0x08)
#define LRADC_DATA0             (0x0c)

/* control register */
#define FIRST_CONCERT_DLY       (0<<24)
#define CHAN                    (0x0)
#define ADC_CHAN_SELECT         (CHAN<<22)
#define LRADC_KEY_MODE          (0)
#define KEY_MODE_SELECT         (LRADC_KEY_MODE<<12)
#define LEVELB_VOL              (0<<4)
#define LRADC_HOLD_KEY_EN		(1<<7)
#define LRADC_HOLD_EN           (1<<6)
#define LRADC_SAMPLE_32HZ       (3<<2)
#define LRADC_SAMPLE_62HZ       (2<<2)
#define LRADC_SAMPLE_125HZ      (1<<2)
#define LRADC_SAMPLE_250HZ      (0<<2)
#define LRADC_EN                (1<<0)

/* interrutp control regitster */
#define LRADC_INT_DATA_IRQ_EN		(1<<0)
#define LRADC_INT_DOWN_IRQ_EN		(1<<1)
#define LRADC_INT_HOLD_IRQ_EN		(1<<2)
#define LRADC_INT_ALRDY_HOLD_IRQ_EN	(1<<3)
#define LRADC_INT_UP_IRQ_EN			(1<<4)


/* interrutp status regitster */
#define LRADC_INT_DATA_IRQ_ST		(1<<0)
#define LRADC_INT_DOWN_IRQ_ST		(1<<1)
#define LRADC_INT_HOLD_IRQ_ST		(1<<2)
#define LRADC_INT_ALRDY_HOLD_IRQ_ST	(1<<3)
#define LRADC_INT_UP_IRQ_ST			(1<<4)







#endif



