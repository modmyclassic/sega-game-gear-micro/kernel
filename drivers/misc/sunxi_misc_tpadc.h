#ifndef __SUNXI_TPADC_MISC_H_
#define __SUNXI_TPADC_MISC_H_

#define TP_BASE_ADDR 0x01c24800

#define TP_CTRL0					(0x00)
#define TP_CTRL1					(0x04)
#define TP_CTRL2					(0x08)
#define TP_CTRL3					(0x0c)
#define TP_INT_FIFO_CTRL		(0x10)
#define TP_INT_FIFO_STAT		(0x14)
#define TP_COM_DATA			(0x1c)
#define TP_DATA					(0x24)

/* control register 1 */
#define STYLUS_UP_DEBOUNCE		(1<<12)
#define STYLUS_UP_DEBOUCE_EN	(1<<9)
#define TOUCH_PAN_CALI_EN			(1<<7)
#define TP_DUAL_EN						(1<<6)
#define TP_EN								(1<<5)
#define TP_MODE_SELECT				(1<<4)
#define ADC_CHAN3_SELECT			(1<<3)
#define ADC_CHAN2_SELECT			(1<<2)
#define ADC_CHAN1_SELECT			(1<<1)
#define ADC_CHAN0_SELECT			(1<<0)

#endif



