/* display driver
 *
 * Copyright (c) 2017 Allwinnertech Co., Ltd.
 * Author: Tyle <tyle@allwinnertech.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#include "disp_tv.h"
#include "disp_display.h"

struct disp_tv_private_data {
	u32 enabled;
	disp_tv_mode mode;
	struct disp_tv_func tv_func;
	disp_video_timing *video_info;

	struct clk *tcon_clk;
	struct clk *tcon_clk_parent;

	struct clk *tve_clk;
	struct clk *tve_clk_parent;
};

static u32 tv_used;

#if defined(__LINUX_PLAT__)
static spinlock_t tv_data_lock;
#endif

static struct disp_tv *tvs;
static struct disp_tv_private_data *tv_private;

struct disp_tv *disp_get_tv(u32 screen_id)
{
	u32 num_screens;

	num_screens = bsp_disp_feat_get_num_screens();
	if (screen_id >= num_screens) {
		printk("screen_id %d out of range\n", screen_id);
		return NULL;
	}

	if (!disp_al_query_tv_mod(screen_id)) {
		DE_INF("tv %d is not registered\n", screen_id);
		return NULL;
	}

	return &tvs[screen_id];
}

static struct disp_tv_private_data *disp_tv_get_priv(struct disp_tv *tv)
{
	if (tv == NULL) {
		printk("NULL hdl!\n");
		return NULL;
	}

	return &tv_private[tv->channel_id];
}

static s32 tv_clk_init(struct disp_tv *tv)
{
	struct disp_tv_private_data *tvp = disp_tv_get_priv(tv);

	if (!tv || !tvp) {
		printk("tv clk init null hdl!\n");
		return DIS_FAIL;
	}

	printk("tcon_clk=0x%x, tcon_clk_parent=0x%x\n", tvp->tcon_clk, tvp->tcon_clk_parent);
	if (tvp->tcon_clk) {
		tvp->tcon_clk_parent = clk_get_parent(tvp->tcon_clk);
		printk("tcon_clk=0x%x, tcon_clk_parent=0x%x\n", tvp->tcon_clk, tvp->tcon_clk_parent);
		if (__clk_get_enable_count(tvp->tcon_clk) != 0) {
			clk_disable(tvp->tcon_clk);
			printk("tcon_clk disable\n");
			}
	}

	if (tvp->tve_clk) {
		tvp->tve_clk_parent = clk_get_parent(tvp->tve_clk);
		printk("tve_clk=0x%x, tve_clk_parent=0x%x\n", tvp->tve_clk, tvp->tve_clk_parent);
		if (__clk_get_enable_count(tvp->tve_clk) != 0) {
			clk_disable(tvp->tve_clk);
			printk("tve_clk disable\n");
		}
	}

	return 0;
}

static s32 tv_clk_exit(struct disp_tv *tv)
{
	struct disp_tv_private_data *tvp = disp_tv_get_priv(tv);
	if (!tv || !tvp) {
		printk("tv clk init null hdl!\n");
		return DIS_FAIL;
	}

	if (tvp->tcon_clk) {
		clk_disable(tvp->tcon_clk);
		clk_put(tvp->tcon_clk);
	}

	if (tvp->tve_clk) {
		clk_disable(tvp->tve_clk);
		clk_put(tvp->tve_clk);
	}

	return 0;
}

static s32 tv_clk_enable(struct disp_tv *tv)
{
	u32 pll_freq;
	u32 tve_freq;
	u32 tcon_freq;
	struct disp_tv_private_data *tvp = disp_tv_get_priv(tv);
	if (!tv || !tvp) {
	    printk("tv clk init null hdl!\n");
	    return DIS_FAIL;
	}

	pll_freq = 297 * 1000000;
	tve_freq = 27 * 1000000;
	tcon_freq = pll_freq;

	if (tvp->tcon_clk_parent) {
		clk_set_rate(tvp->tcon_clk_parent, pll_freq);
		printk("tcon_clk_parent=%dMHZ\n", clk_get_rate(tvp->tcon_clk_parent)/1000000);
	}

	if (tvp->tcon_clk) {
		clk_set_rate(tvp->tcon_clk, tcon_freq);
		printk("tcon_clk=%dMHZ\n", clk_get_rate(tvp->tcon_clk)/1000000);
	}

	if (tvp->tve_clk_parent) {
		clk_set_rate(tvp->tve_clk_parent, pll_freq);
		printk("tve_clk_parent=%dMHZ\n", clk_get_rate(tvp->tve_clk_parent)/1000000);
	}

	if (tvp->tve_clk) {
		clk_set_rate(tvp->tve_clk, tve_freq);
		printk("tve_clk=%dMHZ\n", clk_get_rate(tvp->tve_clk)/1000000);
	}

	if (tvp->tcon_clk)
		clk_prepare_enable(tvp->tcon_clk);

	if (tvp->tve_clk)
		clk_prepare_enable(tvp->tve_clk);

	*((volatile __u32 *)(0xF1C20120)) = 0x8000810a;
	printk("reg[0xF1C20120]=0x%x\n", *((volatile __u32 *)(0xF1C20120)));

	return 0;

}

static s32 tv_clk_disable(struct disp_tv *tv)
{

	struct disp_tv_private_data *tvp = disp_tv_get_priv(tv);
	if (!tv || !tvp) {
	    printk("tv clk init null hdl!\n");
	    return DIS_FAIL;
	}

	disp_al_tv_disable(tv->channel_id);

	if (tvp->tcon_clk)
		clk_disable(tvp->tcon_clk);

	if (tvp->tve_clk)
		clk_disable(tvp->tve_clk);

	return 0;
}

static s32 disp_tv_set_func(struct disp_tv *tv, struct disp_tv_func *func)
{
	struct disp_tv_private_data *tvp = disp_tv_get_priv(tv);
	if ((NULL == tv) || (NULL == tvp)) {
		printk("tv set func null  hdl!\n");
		return DIS_FAIL;
	}

	memcpy(&tvp->tv_func, func, sizeof(struct disp_tv_func));

	return 0;
}

static s32 disp_tv_init(struct disp_tv *tv)
{
	s32 ret;
	u32 value = 0;
	struct disp_tv_private_data *tvp = disp_tv_get_priv(tv);

	printk("disp tv init\n");

	if (!tv || !tvp) {
		printk("hdmi init null hdl!\n");
		return DIS_FAIL;
	}

	if (!disp_al_query_tv_mod(tv->channel_id)) {
		printk("tv %d is not register\n", tv->channel_id);
		return DIS_FAIL;
	}

	if ((NULL != tv->p_sw_init_flag) && (0 != *(tv->p_sw_init_flag))) {
		disp_al_tv_init_sw(tv->channel_id, 0);
	} else {
		tv_clk_init(tv);
		disp_al_tv_init(tv->channel_id, 0);
	}

	return 0;

}

static s32 disp_tv_exit(struct disp_tv *tv)
{
	struct disp_tv_private_data *tvp = disp_tv_get_priv(tv);
	if (!tv || !tvp) {
	    printk("tv init null hdl!\n");
	    return DIS_FAIL;
	}

	if (!disp_al_query_tv_mod(tv->channel_id)) {
		DE_WRN("tv %d is not register\n", tv->channel_id);
		return DIS_FAIL;
	}

	disp_al_tv_exit(tv->channel_id);
	tv_clk_exit(tv);

  return 0;
}

static s32 disp_tv_enable(struct disp_tv *tv)
{
	s32 index;
	struct disp_tv_private_data *tvp = disp_tv_get_priv(tv);

	if ((tv == NULL) || (tvp == NULL)) {
		DE_WRN("tvi set func null  hdl!\n");
		return DIS_FAIL;
	}

	if (tvp->tv_func.tv_get_video_timing_info == NULL) {
		DE_WRN("tv_get_video_timing_info func is null\n");
		return DIS_FAIL;
	}

	tvp->tv_func.tv_get_video_timing_info(tv->channel_id, &(tvp->video_info));

	if (tvp->video_info == NULL) {
		printk("video info is null\n");
		return DIS_FAIL;
	}

	printk("video_info x_res=%d, y_res=%d\n", tvp->video_info->x_res, tvp->video_info->y_res);

	if ((tv->p_sw_init_flag != NULL) && (0 != *(tv->p_sw_init_flag))) {
		disp_al_tv_init_sw(tv->channel_id, 0);
#if defined(CONFIG_HOMLET_PLATFORM)
		/* hdmi_clk_enable(hdmi); */
		disp_al_tv_cfg_sw(tv->channel_id, tvp->video_info);
		disp_al_tv_enable_sw(tv->channel_id);
#endif
	} else {
		tv_clk_enable(tv);
		disp_al_tv_init(tv->channel_id, 0);
		disp_al_tv_cfg(tv->channel_id, tvp->video_info);
		disp_al_tv_enable(tv->channel_id);
	}

	if (tvp->tv_func.tv_enable == NULL)
		return -1;

	tvp->tv_func.tv_enable(tv->channel_id);

#if defined(__LINUX_PLAT__)
	{
		unsigned long flags;

		spin_lock_irqsave(&tv_data_lock, flags);
#endif
		tvp->enabled = 1;
#if defined(__LINUX_PLAT__)
		spin_unlock_irqrestore(&tv_data_lock, flags);
	}
#endif

	return 0;
}

static s32 disp_tv_disable(struct disp_tv *tv)
{
	struct disp_tv_private_data *tvp = disp_tv_get_priv(tv);
	if ((NULL == tv) || (NULL == tvp)) {
	    DE_WRN("tv set func null  hdl!\n");
	    return DIS_FAIL;
	}

	if (tvp->enabled == 0) {
		DE_WRN("tv%d is already closed\n", tv->channel_id);
		return DIS_FAIL;
	}


	if (tvp->tv_func.tv_disable == NULL)
	    return -1;

	tvp->tv_func.tv_disable(tv->channel_id);

	disp_al_tv_disable(tv->channel_id);
	tv_clk_disable(tv);

#if defined(__LINUX_PLAT__)
	{
		unsigned long flags;
		spin_lock_irqsave(&tv_data_lock, flags);
#endif
		tvp->enabled = 0;
#if defined(__LINUX_PLAT__)
		spin_unlock_irqrestore(&tv_data_lock, flags);
	}
#endif

	return 0;
}

s32 disp_init_tv(struct __disp_bsp_init_para *para)
{
	s32 ret;
	u32 value;

	tv_used = 1;

	if (tv_used) {
		u32 num_screens;
		u32 screen_id;
		struct disp_tv *tv;
		struct disp_tv_private_data *tvp;

#if defined(__LINUX_PLAT__)
		spin_lock_init(&tv_data_lock);
#endif

		num_screens = bsp_disp_feat_get_num_screens();
		printk("num_screens=%d\n", num_screens);

		tvs = kmalloc_array(num_screens,
				      sizeof(struct disp_tv),
				      GFP_KERNEL | __GFP_ZERO);
		if (tvs == NULL) {
			printk("malloc tvs fail!\n");
			return DIS_FAIL;
		}

		tv_private = kmalloc_array(num_screens,
					     sizeof(struct
						    disp_tv_private_data),
					     GFP_KERNEL | __GFP_ZERO);
		if (tv_private == NULL) {
			printk("malloc tv_private fail!\n");
			return DIS_FAIL;
		}

		for (screen_id = 0; screen_id < num_screens; screen_id++) {
			tv = &tvs[screen_id];
			tvp = &tv_private[screen_id];

			if (!disp_al_query_tv_mod(screen_id)) {
				printk("tv mod %d is not registered\n", screen_id);
				continue;
			}

			printk("screen_id=%d\n", screen_id);
			switch (screen_id) {
			case 0:
				tv->channel_id = 0;
				tv->name = "tv0";
				tv->type = DISP_OUTPUT_TYPE_TV;

				tvp->mode = DISP_TV_MOD_NTSC;

				printk("para->mclk[MOD_CLK_LCD1CH0]=0x%x\n", para->mclk[MOD_CLK_LCD0CH0]);
				printk("para->mclk[MOD_CLK_LCD1CH1]=0x%x\n", para->mclk[MOD_CLK_LCD0CH1]);
				tvp->tcon_clk = para->mclk[MOD_CLK_LCD0CH0];
				tvp->tve_clk = para->mclk[MOD_CLK_LCD0CH1];
				break;

			default:
				break;
			}

			tv->p_sw_init_flag = NULL;

			tv->init = disp_tv_init;
			tv->exit = disp_tv_exit;

			tv->set_func = disp_tv_set_func;
			tv->enable = disp_tv_enable;
			tv->disable = disp_tv_disable;
			tv->is_enabled = NULL;
			tv->set_mode = NULL;
			tv->get_mode = NULL;
			tv->check_support_mode = NULL;
			tv->get_input_csc = NULL;
			tv->suspend = NULL;
			tv->resume = NULL;
			tv->hdmi_get_HPD_status = NULL;
			tv->get_vendor_id = NULL;
			tv->get_edid = NULL;
			tv->init(tv);
		}

	}
	return 0;
}
