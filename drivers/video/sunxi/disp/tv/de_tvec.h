#ifndef __DE_TVEC_H__
#define __DE_TVEC_H__

#include <linux/module.h>
#include <linux/uaccess.h>
#include <asm/memory.h>
#include <asm/unistd.h>
#include "asm-generic/int-ll64.h"
#include "linux/kernel.h"
#include "linux/mm.h"
#include "linux/semaphore.h"
#include <linux/vmalloc.h>
#include <linux/fs.h>
#include <linux/dma-mapping.h>
#include <linux/sched.h>	/* wake_up_process() */
#include <linux/kthread.h>	/* kthread_create()?��kthread_run() */
#include <linux/err.h>		/* IS_ERR()?��PTR_ERR() */
#include <linux/delay.h>
#include <linux/platform_device.h>
#include <linux/errno.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/init.h>
#include <linux/dma-mapping.h>
#include <linux/interrupt.h>
#include <linux/clk.h>
#include <linux/cdev.h>

#include <linux/types.h>
#include <video/drv_display.h>

__s32   TVE_set_reg_base(__u32 address);
__u32   TVE_get_reg_base(void);
__s32   TVE_init(void);
__s32   TVE_exit(void);
__s32   TVE_open(__u32 sel);
__s32   TVE_close(void);
__s32   TVE_set_vga_mode(__u32 sel);
__s32   TVE_set_tv_mode(__u32 sel, __u8 mode);
__u8    TVE_query_interface(__u8 index);
__u8    TVE_query_int(void);
__u8    TVE_clear_int (void);
__u8    TVE_dac_int_enable(__u8 index);
__u8    TVE_dac_int_disable(__u8 index);
__u8    TVE_dac_autocheck_enable(__u8 index);
__u8    TVE_dac_autocheck_disable(__u8 index);
__u8    TVE_dac_enable(__u8 index);
__u8    TVE_dac_disable(__u8 index);
__u8    TVE_dac_set_de_bounce(__u8 index, __u32 times);
__u8    TVE_dac_get_de_bounce(__u8 index);
__s32   TVE_get_dac_status(__u32 index);
__s32   TVE_dac_set_source(__u32 index, __u32 source);

#endif
