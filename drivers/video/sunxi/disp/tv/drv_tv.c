#include "drv_tv_i.h"
#include "dev_tv.h"
#include "de_tvec.h"

#if 0
#include "./../disp/de/lowlevel_v1x/ebios_lcdc_tve.h"
#include "./../ebios_lcdc_tve.h"
#endif

extern s32 disp_set_tv_func(u32 screen_id, struct disp_tv_func *func);

/*
#define TV_SOURCE 	"pll_de"
#define TVE_CLK	 	"tve"
*/

static bool g_used;
static struct mutex mlock;
/*
static void tve_clk_enable(u32 sel);
static void tve_clk_init(u32 sel);
static void tve_clk_disable(u32 sel);
static void tve_clk_config(u32 sel, u32 tv_mode);
*/

static disp_video_timing video_timing[] = {
	/*vic         tv_mode          PCLK   AVI    x     y    HT  HBP HFP HST VT VBP VFP VST*/
#if 0
	{0,	DISP_TV_MOD_PAL, 27000000, 0, 720, 576, 864, 137, 3,   2,  625, 20, 25, 2, 0, 0, 1, 0, 0},
	{0,	DISP_TV_MOD_NTSC, 13500000, 0,  720,   480,   858,   57,   19,   62,  525,   15,  4,  3,  0,   0,   1,   0,   0},
	{0, DISP_TV_MOD_NTSC, 27000000,  0,  720,   480,   858,   60,   16,   62,  525,   30,  9,  6,  0,   1,   0,   0,   0},
	{0, DISP_TV_MOD_PAL, 27000000,  0,  720,   576,   864,   68,   12,   64,  625,   39,  5,  5,  0,   1,   0,   0,   0},
#endif
	{0,   DISP_TV_MOD_NTSC, 27000000,  0,  720,   480,   858,   116,   21,   2,  525,   17,  28,  2,  0,   1,   0,   0,   0},
	{0,   DISP_TV_MOD_PAL  , 27000000,  0,  720,   576,   864,   68,   12,   64,  625,   39,  5,  5,  0,   1,   0,   0,   0},
};

s32 tv_get_video_info(s32 mode)
{
	/*
	s32 i,count;
	count = sizeof(video_timing)/sizeof(disp_video_timings);
	for(i=0;i<count;i++) {
		if(mode == video_timing[i].tv_mode)
			return i;
	}
	return -1;
	*/
	return 0;
}

s32 tv_get_list_num(void)
{
	return sizeof(video_timing)/sizeof(disp_video_timing);
}

s32 tv_set_enhance_mode(u32 sel, u32 mode)
{
	/*return tve_low_enhance(sel, mode);*/
	return 0;
}

s32 tv_init(void)
{
	s32 i = 0;
	struct disp_tv_func disp_func;

	printk("tv init\n");
	g_used = 1;
	if (g_used) {
		mutex_init(&mlock);
		g_tv_info.screen[i].tv_mode = DISP_TV_MOD_NTSC;

		TVE_set_reg_base(g_tv_info.screen[0].base_address);
		TVE_init();

		memset(&disp_func, 0, sizeof(disp_func));
		disp_func.tv_enable = tv_enable;
		disp_func.tv_disable = tv_disable;
		disp_func.tv_resume = tv_resume;
		disp_func.tv_suspend = tv_suspend;
		disp_func.tv_get_mode = tv_get_mode;
		disp_func.tv_set_mode = tv_set_mode;
		disp_func.tv_get_video_timing_info = tv_get_video_timing_info;
		disp_func.tv_get_input_csc = tv_get_input_csc;
		disp_func.tv_mode_support = tv_mode_support;
		disp_func.tv_hot_plugging_detect = tv_hot_plugging_detect;
		disp_func.tv_set_enhance_mode = tv_set_enhance_mode;
		disp_set_tv_func(0, &disp_func);
	}

	return 0;
}

s32 tv_exit(void)
{
	TVE_exit();
	return 0;
}

s32 tv_enable(u32 sel)
{
	TVE_close();
	TVE_dac_set_de_bounce(0, 0);
	TVE_dac_int_disable(0);

	TVE_set_tv_mode(sel, g_tv_info.screen[0].tv_mode);
	TVE_dac_enable(sel);
	TVE_open(sel);
	return 0;
}

s32 tv_disable(u32 sel)
{
	TVE_close();
	TVE_dac_disable(sel);

	TVE_dac_int_disable(0);
	return 0;
}

s32 tv_get_mode(u32 sel)
{
	/*
	if(SCREEN_COUNT-1 < sel && SCREEN_COUNT > 0)
		sel--;
	return g_tv_info.screen[sel].tv_mode;
	*/
	return  0;
}

s32 tv_set_mode(u32 sel, disp_tv_mode tv_mode)
{
	/*
	if(tv_mode >= DISP_TV_MODE_NUM) {
		return -1;
	}

	if(SCREEN_COUNT-1 < sel && SCREEN_COUNT > 0)
		sel--;

	mutex_lock(&mlock);
	g_tv_info.screen[sel].tv_mode = tv_mode;
	mutex_unlock(&mlock);
	return  0;
	*/
	return  0;
}

s32 tv_get_input_csc(void)
{
	return 1; /*support yuv only*/
}

s32 tv_get_video_timing_info(u32 sel, disp_video_timing **video_info)
{
	disp_video_timing *info;
	int ret = -1;
	int i, list_num;
	info = video_timing;

	printk("tv get video timing info\n");

	list_num = tv_get_list_num();
	if (SCREEN_COUNT-1 < sel && SCREEN_COUNT > 0)
		sel--;
	for (i = 0; i < list_num; i++) {
		mutex_lock(&mlock);
		if (info->tv_mode == g_tv_info.screen[sel].tv_mode) {
			*video_info = info;
			ret = 0;
			mutex_unlock(&mlock);
			break;
		}
		mutex_unlock(&mlock);
		info++;
	}
	return ret;
	return 0;
}

s32 tv_suspend(void)
{
	/*
	int i = 0;

	mutex_lock(&mlock);
	if(g_used && (0 == g_suspend)) {
		g_suspend = true;
		tv_detect_disable();
		for(i=0; i<SCREEN_COUNT;i++)
			tve_clk_disable(i);
	}
	mutex_unlock(&mlock);

	return 0;
	*/

	return 0;
}

s32 tv_resume(void)
{
	/*
	int i = 0;

	mutex_lock(&mlock);
	if(g_used && (1 == g_suspend)) {
		g_suspend= false;
		for(i=0; i<SCREEN_COUNT;i++) {
			tve_clk_enable(i);
			tve_low_init(i, g_tv_info.screen[i].sid, g_tv_info.screen[i].cali_offset);

		}
		tv_detect_enable();
	}
	mutex_unlock(&mlock);
	return  0;
	*/
}

s32 tv_mode_support(disp_tv_mode mode)
{
	/*
	u32 i, list_num;
	disp_video_timings *info;


	info = video_timing;
	list_num = tv_get_list_num();
	for(i=0; i<list_num; i++) {
		if(info->tv_mode == mode) {
			return 1;
		}
		info ++;
	}
	return 0;
	*/
	return 0;
}

s32 tv_hot_plugging_detect (u32 state)
{
	/*
	int i = 0;
	for(i=0; i<SCREEN_COUNT;i++) {
		if(state == STATUE_OPEN) {
			return tve_low_dac_autocheck_enable(i,0);
		}
		else if(state == STATUE_CLOSE){
			return tve_low_dac_autocheck_disable(i,0);
		}
	}
	return 0;
	*/
}


/*
static void tve_clk_init(u32 sel)
{
	disp_sys_clk_set_parent("tve", TV_SOURCE);
}


static void tve_clk_enable(u32 sel)
{
	disp_sys_clk_enable("tve");
}

static void tve_clk_disable(u32 sel)
{
	disp_sys_clk_disable("tve");
}

static void tve_clk_config(u32 sel, u32 tv_mode)
{
	disp_sys_clk_set_rate("tve", 216000000);
}
*/

