#include "de_tvec.h"

__u32 tve_reg_base;

/*tv encoder registers offset*/
#define TVE_000    0x00
#define TVE_004    0X04
#define TVE_008    0X08
#define TVE_00C    0x0c
#define TVE_010    0x10
#define TVE_014    0x14
#define TVE_018    0x18
#define TVE_01C    0x1c
#define TVE_020    0x20
#define TVE_024    0x24
#define TVE_030    0X30
#define TVE_034    0x34
#define TVE_038    0x38
#define TVE_03C    0x3c
#define TVE_100    0x100
#define TVE_104    0x104
#define TVE_10C    0x10c
#define TVE_110    0x110
#define TVE_114    0x114
#define TVE_118    0x118
#define TVE_11C    0x11c
#define TVE_124    0x124
#define TVE_128    0x128
#define TVE_12C    0x12c
#define TVE_130    0x130
#define TVE_138    0x138
#define TVE_13C    0x13C

#define TVE_GET_REG_BASE()    (tve_reg_base)

#define TVE_WUINT8(offset, value)           (*((volatile __u8  *)(TVE_GET_REG_BASE() + offset)) = (value))
#define TVE_RUINT8(offset)                 (*((volatile __u8  *)(TVE_GET_REG_BASE() + offset)))
#define TVE_WUINT16(offset, value)          (*((volatile __u16 *)(TVE_GET_REG_BASE() + offset)) = (value))
#define TVE_RUINT16(offset)                (*((volatile __u16 *)(TVE_GET_REG_BASE() + offset)))
#define TVE_WUINT32(offset, value)          (*((volatile __u32 *)(TVE_GET_REG_BASE() + offset)) = (value))
#define TVE_RUINT32(offset)                (*((volatile __u32 *)(TVE_GET_REG_BASE() + offset)))

#define TVE_WUINT8IDX(offset, index, value)  (*((volatile __u8  *)(TVE_GET_REG_BASE() + offset + index)) = (value))
#define TVE_RUINT8IDX(offset, index)        (*((volatile __u8  *)(TVE_GET_REG_BASE()+offset+index)))
#define TVE_WUINT16IDX(offset, index, value) (*((volatile __u16 *)(TVE_GET_REG_BASE()+offset+2*index)) = (value))
#define TVE_RUINT16IDX(offset, index)       (*((volatile __u16 *)(TVE_GET_REG_BASE()+offset+2*index)))
#define TVE_WUINT32IDX(offset, index, value) (*((volatile __u32 *)(TVE_GET_REG_BASE()+offset+4*index)) = (value))
#define TVE_RUINT32IDX(offset, index)       (*((volatile __u32 *)(TVE_GET_REG_BASE()+offset+4*index)))

#define TVE_SET_BIT(offset, bit)            (*((volatile __u32 *)(TVE_GET_REG_BASE()+offset)) |= (bit))
#define TVE_CLR_BIT(offset, bit)            (*((volatile __u32 *)(TVE_GET_REG_BASE()+offset)) &= (~(bit)))

__s32 TVE_set_reg_base(__u32 address)
{
	tve_reg_base = address;
	return 0;
}

__u32 TVE_get_reg_base(void)
{
	return tve_reg_base;
}

__s32  TVE_init(void)
{
#if 0
	TVE_close();
	TVE_dac_set_de_bounce(0, 0);
	TVE_dac_int_disable(0);
#endif
	return 0;
}

__s32 TVE_exit(void)
{
	/*TVE_dac_int_disable(0);*/
	return 0;
}

/* open module */
__s32 TVE_open(__u32 sel)
{
	u32 readval = 0xFF;

    TVE_SET_BIT(TVE_000, 0x1<<0);

	readval =  *((volatile __u32 *)(0xF1C0A000));
	printk("reg[0xF1C0A000] = 0x%x\n", readval);

	TVE_CLR_BIT(TVE_000, 0x1<<0);

	readval =  *((volatile __u32 *)(0xF1C0A000));
	printk("reg[0xF1C00A00] = 0x%x\n", readval);

	TVE_SET_BIT(TVE_000, 0x1<<0);

#if 0
	{
		u32 readval = 0xFF;

		printk("###########_TVE_open_##########\n");

		readval =  *((volatile __u32 *)(0xF1C0A000));
		printk("reg[0xF1C0A000] = 0x%x\n", readval);

		readval |= (1<<0);
		*((volatile __u32 *)(0xF1C0A000)) = readval;

		readval =  *((volatile __u32 *)(0xF1C0A000));
		printk("reg[0xF1C00A00] = 0x%x\n", readval);

		readval &= (~(1<<0));
		*((volatile __u32 *)(0xF1C0A000)) = readval;

		readval =  *((volatile __u32 *)(0xF1C0A000));
		printk("reg[0xF1C00A00] = 0x%x\n", readval);
	}
#endif

	return 0;
}

__s32 TVE_close(void)
{
    TVE_CLR_BIT(TVE_000, 0x1<<0);

    return 0;
}

__s32 TVE_set_tv_mode(__u32 sel, __u8 mode)
{
    __u32 value_08 = TVE_RUINT32(TVE_008);
    value_08 &= ~(0x1ff<<17);/*clear bit 17~25*/
    value_08 |= (3<<24);/*37.5 ohms terminal mode*/
    value_08 |= (0x1c<<17);/*dac amplitude*/
    TVE_WUINT32(TVE_008, value_08);

	switch (mode) {
	case DISP_TV_MOD_PAL_SVIDEO:
	case DISP_TV_MOD_PAL:
		TVE_WUINT32(TVE_004, 0x07030001);
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/
		TVE_WUINT32(TVE_014, 0x008a0018);
		TVE_WUINT32(TVE_01C, 0x00160271);
		TVE_WUINT32(TVE_114, 0x0016447e);
		TVE_WUINT32(TVE_124, 0x000005a0);
		TVE_WUINT32(TVE_130, 0x800D000C);
		TVE_WUINT32(TVE_13C, 0x00000000);
		TVE_WUINT32(TVE_00C, 0x00000120);
		TVE_WUINT32(TVE_020, 0x00fc00fc);
		TVE_WUINT32(TVE_10C, 0x00002828);
		TVE_WUINT32(TVE_128, 0x00000000);
		TVE_WUINT32(TVE_118, 0x0000e0e0);
		TVE_WUINT32(TVE_12C, 0x00000101);
		break;

	case DISP_TV_MOD_PAL_M:
	case DISP_TV_MOD_PAL_M_SVIDEO:
		TVE_WUINT32(TVE_004, 0x07030000);/*ntsc*/
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/
		TVE_WUINT32(TVE_014, 0x00760020);
		TVE_WUINT32(TVE_01C, 0x0016020d);
		TVE_WUINT32(TVE_114, 0x0016447e);
		TVE_WUINT32(TVE_124, 0x000005a0);
		TVE_WUINT32(TVE_130, 0x000e000c);
		TVE_WUINT32(TVE_13C, 0x00000000);
		TVE_WUINT32(TVE_00C, 0x00000120);
		TVE_WUINT32(TVE_020, 0x00f0011a);
		TVE_WUINT32(TVE_10C, 0x0000004f);
		TVE_WUINT32(TVE_110, 0x00000000);
		TVE_WUINT32(TVE_118, 0x0000a0a0);
		TVE_WUINT32(TVE_11C, 0x001000f0);
		TVE_WUINT32(TVE_010, 0x21e6efe3);/*add for pal-m*/
		TVE_WUINT32(TVE_100, 0x00000000);/*add for pal-m*/
		TVE_WUINT32(TVE_128, 0x00000000);
		TVE_WUINT32(TVE_12C, 0x00000101);
		break;

	case DISP_TV_MOD_PAL_NC:
	case DISP_TV_MOD_PAL_NC_SVIDEO:
		TVE_WUINT32(TVE_004, 0x07030001);/*PAL*/
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/
		TVE_WUINT32(TVE_014, 0x008a0018);
		TVE_WUINT32(TVE_01C, 0x00160271);
		TVE_WUINT32(TVE_114, 0x0016447e);
		TVE_WUINT32(TVE_124, 0x000005a0);
		TVE_WUINT32(TVE_130, 0x800D000C);
		TVE_WUINT32(TVE_13C, 0x00000000);
		TVE_WUINT32(TVE_00C, 0x00000120);
		TVE_WUINT32(TVE_020, 0x00fc00fc);
		TVE_WUINT32(TVE_10C, 0x00002828);
		TVE_WUINT32(TVE_010, 0x21F69446);/*add for PAL-NC*/
		TVE_WUINT32(TVE_128, 0x00000000);
		TVE_WUINT32(TVE_118, 0x0000e0e0);
		TVE_WUINT32(TVE_12C, 0x00000101);
		break;

	case DISP_TV_MOD_NTSC:
	case DISP_TV_MOD_NTSC_SVIDEO:
		TVE_WUINT32(TVE_004, 0x07030000);
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/

		printk("TVE_014=0x%x\n", TVE_RUINT32(TVE_014));
		TVE_WUINT32(TVE_014, 0x0);
		printk("TVE_014=0x%x\n", TVE_RUINT32(TVE_014));
		TVE_WUINT32(TVE_014, 0x00760020);
		printk("TVE_014=0x%x\n", TVE_RUINT32(TVE_014));

		TVE_WUINT32(TVE_01C, 0x0016020d);
		TVE_WUINT32(TVE_114, 0x0016447e);
		TVE_WUINT32(TVE_124, 0x000005a0);
		TVE_WUINT32(TVE_130, 0x000e000c);
		TVE_WUINT32(TVE_13C, 0x00000000);
		TVE_WUINT32(TVE_00C, 0x00000120);
		TVE_WUINT32(TVE_020, 0x00f0011a);
		TVE_WUINT32(TVE_10C, 0x0000004f);
		TVE_WUINT32(TVE_110, 0x00000000);
		TVE_WUINT32(TVE_118, 0x0000a0a0);
		TVE_WUINT32(TVE_11C, 0x001000f0);
		TVE_WUINT32(TVE_128, 0x00000000);
		TVE_WUINT32(TVE_12C, 0x00000101);
		break;

	case DISP_TV_MOD_480I:
		TVE_WUINT32(TVE_004, 0x07040000);
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/
		TVE_WUINT32(TVE_014, 0x00760020);
		TVE_WUINT32(TVE_01C, 0x0016020d);
		TVE_WUINT32(TVE_114, 0x0016447e);
		TVE_WUINT32(TVE_124, 0x000005a0);
		TVE_WUINT32(TVE_130, 0x000e000c);
		TVE_WUINT32(TVE_13C, 0x00000000);
		TVE_WUINT32(TVE_00C, 0x00000120);
		TVE_WUINT32(TVE_020, 0x00fc00fc);
		TVE_WUINT32(TVE_10C, 0x0000004f);
		TVE_WUINT32(TVE_110, 0x00000000);
		TVE_WUINT32(TVE_118, 0x0000a0a0);
		TVE_WUINT32(TVE_11C, 0x001000fc);
		break;

	case DISP_TV_MOD_576I:
		TVE_WUINT32(TVE_004, 0x07040001);
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/
		TVE_WUINT32(TVE_014, 0x008a0018);
		TVE_WUINT32(TVE_01C, 0x00160271);
		TVE_WUINT32(TVE_114, 0x0016447e);
		TVE_WUINT32(TVE_124, 0x000005a0);
		TVE_WUINT32(TVE_130, 0x800D000C);
		TVE_WUINT32(TVE_13C, 0x00000000);
		TVE_WUINT32(TVE_00C, 0x00000120);
		TVE_WUINT32(TVE_020, 0x00fc00fc);
		TVE_WUINT32(TVE_10C, 0x00002828);
		break;

	case DISP_TV_MOD_480P:
		TVE_WUINT32(TVE_004, 0x07040002);
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/
		TVE_WUINT32(TVE_014, 0x00760020);
		TVE_WUINT32(TVE_01C, 0x002c020d);
		TVE_WUINT32(TVE_114, 0x0016447e);
		TVE_WUINT32(TVE_124, 0x000005a0);
		TVE_WUINT32(TVE_130, 0x000e000C);
		TVE_WUINT32(TVE_13C, 0x00000000);
		TVE_WUINT32(TVE_00C, 0x00000120);
		TVE_WUINT32(TVE_020, 0x00fc00fc);
		break;

	case DISP_TV_MOD_576P:
		TVE_WUINT32(TVE_004, 0x07040003);
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/
		TVE_WUINT32(TVE_014, 0x008a0018);
		TVE_WUINT32(TVE_01C, 0x002c0271);
		TVE_WUINT32(TVE_114, 0x0016447e);
		TVE_WUINT32(TVE_124, 0x000005a0);
		TVE_WUINT32(TVE_130, 0x800B000C);
		TVE_WUINT32(TVE_13C, 0x00000000);
		TVE_WUINT32(TVE_00C, 0x00000120);
		TVE_WUINT32(TVE_020, 0x00fc00fc);
		break;

	case DISP_TV_MOD_720P_50HZ:
		TVE_WUINT32(TVE_004, 0x0004000a);
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/
		TVE_WUINT32(TVE_014, 0x01040190);
		TVE_WUINT32(TVE_018, 0x05000190);
		TVE_WUINT32(TVE_01C, 0x001902ee);
		TVE_WUINT32(TVE_114, 0xdc280228);
		TVE_WUINT32(TVE_124, 0x00000500);
		TVE_WUINT32(TVE_130, 0x000e000c);
		TVE_WUINT32(TVE_13C, 0x07000000);
		TVE_WUINT32(TVE_00C, 0x01be0124);
		TVE_WUINT32(TVE_128, 0x00000000);
		TVE_WUINT32(TVE_020, 0x00fc00fc);
		TVE_WUINT32(TVE_118, 0x0000a0a0);
		break;

	case DISP_TV_MOD_720P_60HZ:
		TVE_WUINT32(TVE_004, 0x0004000a);
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/
		TVE_WUINT32(TVE_014, 0x01040046);
		TVE_WUINT32(TVE_018, 0x05000046);
		TVE_WUINT32(TVE_01C, 0x001902ee);
		TVE_WUINT32(TVE_114, 0xdc280228);
		TVE_WUINT32(TVE_124, 0x00000500);
		TVE_WUINT32(TVE_130, 0x000c0008);
		TVE_WUINT32(TVE_13C, 0x07000000);
		TVE_WUINT32(TVE_00C, 0x01be0124);
		TVE_WUINT32(TVE_128, 0x00000000);
		TVE_WUINT32(TVE_020, 0x00fc00fc);
		TVE_WUINT32(TVE_118, 0x0000a0a0);
		break;

	case DISP_TV_MOD_1080I_50HZ:
		TVE_WUINT32(TVE_004, 0x0004000c);
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/
		TVE_WUINT32(TVE_014, 0x00c001e4);
		TVE_WUINT32(TVE_018, 0x03700108);
		TVE_WUINT32(TVE_01C, 0x00140465);
		TVE_WUINT32(TVE_114, 0x582c442c);
		TVE_WUINT32(TVE_124, 0x00000780);
		TVE_WUINT32(TVE_130, 0x000e0008);
		TVE_WUINT32(TVE_13C, 0x07000000);
		TVE_WUINT32(TVE_00C, 0x01be0124);
		TVE_WUINT32(TVE_128, 0x00000000);
		TVE_WUINT32(TVE_020, 0x00fc00fc);
		TVE_WUINT32(TVE_118, 0x0000a0a0);
		TVE_WUINT32(TVE_104, 0x00000000);
		break;

	case DISP_TV_MOD_1080I_60HZ:
		TVE_WUINT32(TVE_004, 0x0004000c);
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/
		TVE_WUINT32(TVE_014, 0x00c0002c);
		TVE_WUINT32(TVE_018, 0x0370002c);
		TVE_WUINT32(TVE_01C, 0x00140465);
		TVE_WUINT32(TVE_114, 0x582c442c);
		TVE_WUINT32(TVE_124, 0x00000780);
		TVE_WUINT32(TVE_130, 0x000e0008);
		TVE_WUINT32(TVE_13C, 0x07000000);
		TVE_WUINT32(TVE_00C, 0x01be0124);
		TVE_WUINT32(TVE_128, 0x00000000);
		TVE_WUINT32(TVE_020, 0x00fc00fc);
		TVE_WUINT32(TVE_118, 0x0000a0a0);
		TVE_WUINT32(TVE_104, 0x00000000);
		break;

	case DISP_TV_MOD_1080P_50HZ:
		TVE_WUINT32(TVE_004, 0x0004000e);
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/
		TVE_WUINT32(TVE_014, 0x00c001e4);/*50hz*/
		TVE_WUINT32(TVE_018, 0x07bc01e4);/*50hz*/
		TVE_WUINT32(TVE_01C, 0x00290465);
		TVE_WUINT32(TVE_114, 0x582c022c);
		TVE_WUINT32(TVE_124, 0x00000780);
		TVE_WUINT32(TVE_130, 0x000e000c);
		TVE_WUINT32(TVE_13C, 0x07000000);
		TVE_WUINT32(TVE_00C, 0x01be0124);
		TVE_WUINT32(TVE_128, 0x00000000);
		TVE_WUINT32(TVE_020, 0x00fc00c0);
		TVE_WUINT32(TVE_118, 0x0000a0a0);
		break;

	case DISP_TV_MOD_1080P_60HZ:
		TVE_WUINT32(TVE_004, 0x0004000e);
		TVE_SET_BIT(TVE_004, sel<<30);/*set 0x004 reg first, because write it will change other regs*/
		TVE_WUINT32(TVE_00C, 0x01be0124);
		TVE_WUINT32(TVE_014, 0x00c0002c);/*60hz*/
		TVE_WUINT32(TVE_018, 0x07bc002c);/*60hz*/
		TVE_WUINT32(TVE_01C, 0x00290465);
		TVE_WUINT32(TVE_020, 0x00fc00c0);
		TVE_WUINT32(TVE_114, 0x582c022c);
		TVE_WUINT32(TVE_118, 0x0000a0a0);
		TVE_WUINT32(TVE_124, 0x00000780);
		TVE_WUINT32(TVE_128, 0x00000000);
		TVE_WUINT32(TVE_130, 0x000e000c);
		TVE_WUINT32(TVE_13C, 0x07000000);
		break;

	default:
		return 0;
	}

	return 0;
}
__s32 TVE_set_vga_mode(__u32 sel)
{
    TVE_WUINT32(TVE_004, 0x20000000);
	TVE_SET_BIT(TVE_004, sel<<30);
    TVE_WUINT32(TVE_008, 0x40031ac7);
    TVE_WUINT32(TVE_024, 0x00000000);

	return 0;
}

__u8 TVE_query_interface(__u8 index)
{
    __u8 sts = 0;
    __u32 readval;

    readval = TVE_RUINT32(TVE_038);
    sts = readval & (3<<(index*8));
    sts >>= (index*8);

    return sts;
}

__u8 TVE_query_int(void)
{
    __u8    sts = 0;
    __u32   readval;

    readval = TVE_RUINT32(TVE_034);
    sts = readval & 0x0f;

    return sts;
}

__u8  TVE_clear_int(void)
{
    __u32    sts = 0;
    __u32    readval;

    readval = TVE_RUINT32(TVE_034);
    sts = readval & 0x0f;
    TVE_WUINT32(TVE_034, sts);

    return 0;
}

/*0:unconnected; 1:connected; 3:short to ground*/
__s32 TVE_get_dac_status(__u32 index)
{
    __u32   readval;

    readval = TVE_RUINT32(TVE_038);

	if (index == 0) {
		readval = (readval & 0x00000003);
	} else {
       /*OSAL_printf("ERR: There is only one dac!\n");*/
    }

    return readval;
}

__u8 TVE_dac_int_enable(__u8 index)
{
    __u32   readval;

    readval = TVE_RUINT32(TVE_030);
    readval |= (1<<(16+index));
    TVE_WUINT32(TVE_030, readval);

    return 0;
}

__u8 TVE_dac_int_disable(__u8 index)
{
    __u32   readval;

    readval = TVE_RUINT32(TVE_030);
    readval &= (~(1<<(16+index)));
    TVE_WUINT32(TVE_030, readval);

    return 0;
}

__u8 TVE_dac_autocheck_enable(__u8 index)
{
    __u32   readval;

    readval = TVE_RUINT32(TVE_030);
    readval |= (1<<index);
    TVE_WUINT32(TVE_030, readval);

	readval = TVE_RUINT32(TVE_008);
    readval &= ~(0x1ff<<17);/*clear bit 17~25*/
    readval |= (3<<24);/*37.5 ohms terminal mode*/
    readval |= (0x1c<<17);/*dac amplitude*/
    TVE_WUINT32(TVE_008, readval);

    return 0;
}

__u8 TVE_dac_autocheck_disable(__u8 index)
{
	__u32   readval;

	readval = TVE_RUINT32(TVE_030);
	readval &= (~(1<<index));
	TVE_WUINT32(TVE_030, readval);

	return 0;
}

__u8 TVE_dac_enable(__u8 index)
{
	__u32   readval;

	if (index == 0) {
		readval = TVE_RUINT32(TVE_008);
		TVE_SET_BIT(TVE_008, readval | (1<<index));
	} else {
		/*OSAL_printf("ERR: There is only one DAC to enable!\n");*/
	}

	return 0;
}

__u8 TVE_dac_disable(__u8 index)
{
	__u32   readval;

	if (index == 0) {
		readval = TVE_RUINT32(TVE_008);
		TVE_WUINT32(TVE_008, readval & (~(1<<index)));
	} else {
		/*OSAL_printf("ERR: There is only one DAC to disable!\n");*/
	}

	return 0;
}

__s32 TVE_dac_set_source(__u32 index, __u32 source)
{
	/*OSAL_printf("ERR: DAC source can't be set\n");*/

    return 0;
}

__u8 TVE_dac_set_de_bounce(__u8 index, __u32 times)
{
	__u32   readval;

	readval = TVE_RUINT32(TVE_03C);

    if (index == 0) {
		readval = (readval & 0xfffffff0)|(times & 0xf);
    } else {
		/*OSAL_printf("ERR: There is only one DAC to set de-bounce!\n");*/
		return 0;
	}
	TVE_WUINT32(TVE_03C, readval);

	return 0;
}

__u8 TVE_dac_get_de_bounce(__u8 index)
{
	__u8    sts = 0;
	__u32   readval;

	readval = TVE_RUINT32(TVE_03C);

	if (index == 0) {
		sts = readval & 0xf;
	} else {
		/*OSAL_printf("ERR: There is only one DAC to get de-bounce!\n");*/
		return 0;
	}

	return sts;
}
