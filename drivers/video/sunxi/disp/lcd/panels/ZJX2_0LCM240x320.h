/* display driver
 *
 * Copyright (c) 2017 Allwinnertech Co., Ltd.
 * Author: Tyle <tyle@allwinnertech.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#ifndef __ZJX2_0LCM240x320_H__
#define  __ZJX2_0LCM240x320_H__

#include "panels.h"

extern struct __lcd_panel_t zjx2_0lcm_240x320;

#endif
