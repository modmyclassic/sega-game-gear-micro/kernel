/* display driver
 *
 * Copyright (c) 2017 Allwinnertech Co., Ltd.
 * Author: Tyle <tyle@allwinnertech.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#include "ST7789VW_240x240.h"
extern __s32 tcon0_cpu_wr_16b_index(__u32 sel, __u32 index);
extern __s32 tcon0_cpu_wr_16b_data(__u32 sel, __u32 data);
extern void disp_lcd_cpu_register_irq(__u32 sel, void (*Lcd_cpuisr_proc) (void));
extern void tcon0_cpu_flash(__u32 sel, __u8 en);

static void LCD_power_on(u32 sel);
static void LCD_power_off(u32 sel);
static void LCD_bl_open(u32 sel);
static void LCD_bl_close(u32 sel);

static void LCD_panel_init(u32 sel);
static void LCD_panel_exit(u32 sel);
static void lcd_cpu_isr_proc(void);


//--------------------------------------------------------------------------------------
#include <linux/kobject.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

int update = 0;
u32 lcdporch = 0;
static ssize_t update_show(struct kobject *kobj, struct kobj_attribute *attr,
            char *buf)
{
	return sprintf(buf, "%d\n", update);
}

static ssize_t update_store(struct kobject *kobj, struct kobj_attribute *attr,
         const char *buf, size_t count)
{
	/*
	 * according to redmine #2778
	 * need to put the tail of LCD_panel_init here
	 * for every frame we drawn
	 */
	tcon0_cpu_wr_16b_index(0, 0x2A);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0xEF);
	tcon0_cpu_wr_16b_index(0, 0x2B);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0xEF);
	tcon0_cpu_wr_16b_index(0, 0x00);
	tcon0_cpu_wr_16b_index(0, 0x2C);

	sscanf(buf, "%du", &update);
	tcon0_cpu_flash(0, true);
	return count;
}

static ssize_t lcdporch_show(struct kobject *kobj, struct kobj_attribute *attr,
            char *buf)
{
	//tcon0_cpu_wr_16b_index(0, 0xB2);
	//tcon0_cpu_rd_16b_data(0, &lcdporch);

	//dummy
	return sprintf(buf, "%d\n", lcdporch);
}

static ssize_t lcdporch_store(struct kobject *kobj, struct kobj_attribute *attr,
         const char *buf, size_t count)
{
	sscanf(buf, "%du", &lcdporch);
	u32 bpa=0xC, fpa=0xC;

	bpa = lcdporch/2 + lcdporch%2;
	fpa = lcdporch/2;

	tcon0_cpu_wr_16b_index(0, 0xB2);
	tcon0_cpu_wr_16b_data(0, bpa);
	tcon0_cpu_wr_16b_data(0, fpa);
	tcon0_cpu_wr_16b_data(0, 0x01);
	tcon0_cpu_wr_16b_data(0, 0x33);
	tcon0_cpu_wr_16b_data(0, 0x33);

	tcon0_cpu_wr_16b_index(0, 0x34);
	tcon0_cpu_wr_16b_index(0, 0x35);
	tcon0_cpu_wr_16b_data(0, 0x00);

	printk("%s %d bpa=0x%x, fpa=0x%x \n", __func__, __LINE__, bpa, fpa);

	return count;
}

static struct kobj_attribute update_attribute = __ATTR(update, 0666, update_show, update_store);
static struct kobj_attribute lcdporch_attribute = __ATTR(lcdporch, 0666, lcdporch_show, lcdporch_store);

static struct attribute *attrs[] = {
	&update_attribute.attr,
	&lcdporch_attribute.attr,
	NULL,
};

static struct attribute_group attr_group = {
	.attrs = attrs,
};

static struct kobject *update_kobj;

static int __init update_init(void)
{
	int retval;

	update_kobj = kobject_create_and_add("screen_update", kernel_kobj);
	if (!update_kobj)
    		return -ENOMEM;

	retval = sysfs_create_group(update_kobj, &attr_group);
	if (retval)
    		kobject_put(update_kobj);

	return retval;
}

static void __exit update_exit(void)
{
	kobject_put(update_kobj);
}

module_init(update_init);
module_exit(update_exit);
//--------------------------------------------------------------------------------------

static void LCD_cfg_panel_info(panel_extend_para *info)
{
	u32 i = 0, j = 0;
	u32 items;
	u8 lcd_gamma_tbl[][2] = {

		/* {input value, corrected value} */
		{0, 0},
		{15, 15},
		{30, 30},
		{45, 45},
		{60, 60},
		{75, 75},
		{90, 90},
		{105, 105},
		{120, 120},
		{135, 135},
		{150, 150},
		{165, 165},
		{180, 180},
		{195, 195},
		{210, 210},
		{225, 225},
		{240, 240},
		{255, 255},
	};

	u32 lcd_cmap_tbl[2][3][4] = {
		{
		 {LCD_CMAP_G0, LCD_CMAP_B1, LCD_CMAP_G2, LCD_CMAP_B3},
		 {LCD_CMAP_B0, LCD_CMAP_R1, LCD_CMAP_B2, LCD_CMAP_R3},
		 {LCD_CMAP_R0, LCD_CMAP_G1, LCD_CMAP_R2, LCD_CMAP_G3},
		 },
		{
		 {LCD_CMAP_B3, LCD_CMAP_G2, LCD_CMAP_B1, LCD_CMAP_G0},
		 {LCD_CMAP_R3, LCD_CMAP_B2, LCD_CMAP_R1, LCD_CMAP_B0},
		 {LCD_CMAP_G3, LCD_CMAP_R2, LCD_CMAP_G1, LCD_CMAP_R0},
		 },
	};

	items = sizeof(lcd_gamma_tbl) / 2;
	for (i = 0; i < items - 1; i++) {
		u32 num = lcd_gamma_tbl[i + 1][0] - lcd_gamma_tbl[i][0];

		for (j = 0; j < num; j++) {
			u32 value = 0;

			value = lcd_gamma_tbl[i][1] +
			    ((lcd_gamma_tbl[i + 1][1] -
			      lcd_gamma_tbl[i][1]) * j) / num;
			info->lcd_gamma_tbl[lcd_gamma_tbl[i][0] + j] =
			    (value << 16) + (value << 8) + value;
		}
	}
	info->lcd_gamma_tbl[255] = (lcd_gamma_tbl[items - 1][1] << 16) +
	    (lcd_gamma_tbl[items - 1][1] << 8) + lcd_gamma_tbl[items - 1][1];

	memcpy(info->lcd_cmap_tbl, lcd_cmap_tbl, sizeof(lcd_cmap_tbl));
}


static s32 LCD_open_flow(u32 sel)
{
	/* open lcd power, and delay 50ms */
	LCD_OPEN_FUNC(sel, LCD_power_on, 100);
	/* open lcd controller, and delay 100ms */
	LCD_OPEN_FUNC(sel, sunxi_lcd_tcon_enable, 10);
	/* open lcd power, than delay 200ms */
	LCD_OPEN_FUNC(sel, LCD_panel_init, 100);
	/* open lcd backlight, and delay 0ms */
	LCD_OPEN_FUNC(sel, LCD_bl_open, 10);

	return 0;
}

static s32 LCD_close_flow(u32 sel)
{
	/* close lcd backlight, and delay 0ms */
	LCD_CLOSE_FUNC(sel, LCD_bl_close, 0);
	/* close lcd controller, and delay 0ms */
	LCD_CLOSE_FUNC(sel, sunxi_lcd_tcon_disable, 0);
	/* open lcd power, than delay 200ms */
	LCD_CLOSE_FUNC(sel, LCD_panel_exit, 200);
	/* close lcd power, and delay 500ms */
	LCD_CLOSE_FUNC(sel, LCD_power_off, 500);

	return 0;
}

static void LCD_power_on(u32 sel)
{
	/* config lcd_power pin to open lcd power0 */
	sunxi_lcd_power_enable(sel, 0);
	sunxi_lcd_pin_cfg(sel, 1);

	/* do not reset LCD for keep u-boot bootlogo */
	//sunxi_lcd_gpio_set_direction(0, 0, 1);
	//sunxi_lcd_gpio_set_value(0, 0, 0);
}

static void LCD_power_off(u32 sel)
{
	sunxi_lcd_pin_cfg(sel, 0);
	/* config lcd_power pin to close lcd power0 */
	sunxi_lcd_power_disable(sel, 0);
}

static void LCD_bl_open(u32 sel)
{
	sunxi_lcd_pwm_enable(sel);	/* open pwm module */
	/* config lcd_bl_en pin to open lcd backlight */
	sunxi_lcd_backlight_enable(sel);
}

static void LCD_bl_close(u32 sel)
{
	/* config lcd_bl_en pin to close lcd backlight */
	sunxi_lcd_backlight_disable(sel);
	sunxi_lcd_pwm_disable(sel);	/* close pwm module */
}

static void LCD_panel_init(u32 sel)
{
	__u32 lcd_id = 0;

	/*
	 * Do not pull LCD pin in order to keep
	 * bootlogo display smoothly
	 */
	//sunxi_lcd_gpio_set_value(0, 0, 1);
	//sunxi_lcd_delay_ms(10);
	//sunxi_lcd_gpio_set_value(0, 0, 0);
	//sunxi_lcd_delay_ms(10);
	//sunxi_lcd_gpio_set_value(0, 0, 1);
	//sunxi_lcd_delay_ms(100);

	tcon0_cpu_wr_16b_index(0, 0x11);
	sunxi_lcd_delay_ms(120);
	tcon0_cpu_wr_16b_index(0, 0x36);
	tcon0_cpu_wr_16b_data(0, 0x0A);
	tcon0_cpu_wr_16b_index(0, 0x3A);
	tcon0_cpu_wr_16b_data(0, 0x05);
	tcon0_cpu_wr_16b_index(0, 0x34);
	tcon0_cpu_wr_16b_index(0, 0x35);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_index(0, 0xB2);
	tcon0_cpu_wr_16b_data(0, 0x0C);
	tcon0_cpu_wr_16b_data(0, 0x0C);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x33);
	tcon0_cpu_wr_16b_data(0, 0x33);
	tcon0_cpu_wr_16b_index(0, 0xB7);
	tcon0_cpu_wr_16b_data(0, 0x02);
	tcon0_cpu_wr_16b_index(0, 0xBB);
	tcon0_cpu_wr_16b_data(0, 0x30);
	tcon0_cpu_wr_16b_index(0, 0xC0);
	tcon0_cpu_wr_16b_data(0, 0x2C);
	tcon0_cpu_wr_16b_index(0, 0xC2);
	tcon0_cpu_wr_16b_data(0, 0x01);
	tcon0_cpu_wr_16b_index(0, 0xC3);
	tcon0_cpu_wr_16b_data(0, 0x17);
	tcon0_cpu_wr_16b_index(0, 0xC4);
	tcon0_cpu_wr_16b_data(0, 0x20);
	tcon0_cpu_wr_16b_index(0, 0xC6);
	tcon0_cpu_wr_16b_data(0, 0x0F);
	tcon0_cpu_wr_16b_index(0, 0xD0);
	tcon0_cpu_wr_16b_data(0, 0xA4);
	tcon0_cpu_wr_16b_data(0, 0xA1);
	tcon0_cpu_wr_16b_index(0, 0xD6);
	tcon0_cpu_wr_16b_data(0, 0xA1);
	tcon0_cpu_wr_16b_index(0, 0xE0);
	tcon0_cpu_wr_16b_data(0, 0xD0);
	tcon0_cpu_wr_16b_data(0, 0x04);
	tcon0_cpu_wr_16b_data(0, 0x0C);
	tcon0_cpu_wr_16b_data(0, 0x11);
	tcon0_cpu_wr_16b_data(0, 0x13);
	tcon0_cpu_wr_16b_data(0, 0x2C);
	tcon0_cpu_wr_16b_data(0, 0x3B);
	tcon0_cpu_wr_16b_data(0, 0x55);
	tcon0_cpu_wr_16b_data(0, 0x44);
	tcon0_cpu_wr_16b_data(0, 0x16);
	tcon0_cpu_wr_16b_data(0, 0x0D);
	tcon0_cpu_wr_16b_data(0, 0x0B);
	tcon0_cpu_wr_16b_data(0, 0x1F);
	tcon0_cpu_wr_16b_data(0, 0x23);
	tcon0_cpu_wr_16b_index(0, 0xE1);
	tcon0_cpu_wr_16b_data(0, 0xD0);
	tcon0_cpu_wr_16b_data(0, 0x04);
	tcon0_cpu_wr_16b_data(0, 0x0B);
	tcon0_cpu_wr_16b_data(0, 0x10);
	tcon0_cpu_wr_16b_data(0, 0x13);
	tcon0_cpu_wr_16b_data(0, 0x2C);
	tcon0_cpu_wr_16b_data(0, 0x3A);
	tcon0_cpu_wr_16b_data(0, 0x44);
	tcon0_cpu_wr_16b_data(0, 0x51);
	tcon0_cpu_wr_16b_data(0, 0x26);
	tcon0_cpu_wr_16b_data(0, 0x1F);
	tcon0_cpu_wr_16b_data(0, 0x1F);
	tcon0_cpu_wr_16b_data(0, 0x20);
	tcon0_cpu_wr_16b_data(0, 0x23);
	tcon0_cpu_wr_16b_index(0, 0x21);
	tcon0_cpu_wr_16b_index(0, 0x2A);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0xEF);
	tcon0_cpu_wr_16b_index(0, 0x2B);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0xEF);
	tcon0_cpu_wr_16b_index(0, 0x00);
	tcon0_cpu_wr_16b_index(0, 0x29);
	sunxi_lcd_delay_ms(120);

	/*
	 * according to redmine #2778
	 * need to replace following index with 0x2C
	 */
	tcon0_cpu_wr_16b_index(0, 0x2C);
	tcon0_cpu_auto_flash(0, false);
	disp_lcd_cpu_register_irq(0, lcd_cpu_isr_proc);

	/*
	 * Do not enable tcon0 flush in order to keep
	 * bootlogo display smoothly
	 */
	tcon0_cpu_flash(0, false);
}

static void LCD_panel_exit(u32 sel)
{

}

void  lcd_cpu_isr_proc(void)
{
	tcon0_cpu_flash(0, false);
	update = 0;
}

/* sel: 0:lcd0; 1:lcd1 */
static s32 LCD_user_defined_func(u32 sel, u32 para1, u32 para2, u32 para3)
{
	return 0;
}

struct __lcd_panel_t ST7789VW_240x240_panel = {
	/* panel driver name, must mach the name of */
	/*lcd_drv_name in sys_config.fex */
	.name = "ST7789VW_240x240",
	.func = {

		 .cfg_panel_info = LCD_cfg_panel_info,
		 .cfg_open_flow = LCD_open_flow,
		 .cfg_close_flow = LCD_close_flow,
		 .lcd_user_defined_func = LCD_user_defined_func,
		 },
};
