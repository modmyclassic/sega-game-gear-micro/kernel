/* display driver
 *
 * Copyright (c) 2017 Allwinnertech Co., Ltd.
 * Author: Tyle <tyle@allwinnertech.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#include "ZJX2_0LCM240x320.h"
extern __s32 tcon0_cpu_wr_16b_index(__u32 sel, __u32 index);
extern __s32 tcon0_cpu_wr_16b_data(__u32 sel, __u32 data);
extern __s32 tcon0_cpu_rd_16b_data(__u32 sel, __u32 *data);
extern void disp_lcd_cpu_register_irq(__u32 sel, void (*Lcd_cpuisr_proc) (void));
extern void tcon0_cpu_auto_flash(__u32 sel, __u8 en);

static void LCD_power_on(u32 sel);
static void LCD_power_off(u32 sel);
static void LCD_bl_open(u32 sel);
static void LCD_bl_close(u32 sel);

static void LCD_panel_init(u32 sel);
static void LCD_panel_exit(u32 sel);
static void  lcd_cpu_isr_proc(void);

static void LCD_cfg_panel_info(panel_extend_para *info)
{
	u32 i = 0, j = 0;
	u32 items;
	u8 lcd_gamma_tbl[][2] = {

		/* {input value, corrected value} */
		{0, 0},
		{15, 15},
		{30, 30},
		{45, 45},
		{60, 60},
		{75, 75},
		{90, 90},
		{105, 105},
		{120, 120},
		{135, 135},
		{150, 150},
		{165, 165},
		{180, 180},
		{195, 195},
		{210, 210},
		{225, 225},
		{240, 240},
		{255, 255},
	};

	u32 lcd_cmap_tbl[2][3][4] = {
		{
		 {LCD_CMAP_G0, LCD_CMAP_B1, LCD_CMAP_G2, LCD_CMAP_B3},
		 {LCD_CMAP_B0, LCD_CMAP_R1, LCD_CMAP_B2, LCD_CMAP_R3},
		 {LCD_CMAP_R0, LCD_CMAP_G1, LCD_CMAP_R2, LCD_CMAP_G3},
		 },
		{
		 {LCD_CMAP_B3, LCD_CMAP_G2, LCD_CMAP_B1, LCD_CMAP_G0},
		 {LCD_CMAP_R3, LCD_CMAP_B2, LCD_CMAP_R1, LCD_CMAP_B0},
		 {LCD_CMAP_G3, LCD_CMAP_R2, LCD_CMAP_G1, LCD_CMAP_R0},
		 },
	};

	items = sizeof(lcd_gamma_tbl) / 2;
	for (i = 0; i < items - 1; i++) {
		u32 num = lcd_gamma_tbl[i + 1][0] - lcd_gamma_tbl[i][0];

		for (j = 0; j < num; j++) {
			u32 value = 0;

			value = lcd_gamma_tbl[i][1] +
			    ((lcd_gamma_tbl[i + 1][1] -
			      lcd_gamma_tbl[i][1]) * j) / num;
			info->lcd_gamma_tbl[lcd_gamma_tbl[i][0] + j] =
			    (value << 16) + (value << 8) + value;
		}
	}
	info->lcd_gamma_tbl[255] = (lcd_gamma_tbl[items - 1][1] << 16) +
	    (lcd_gamma_tbl[items - 1][1] << 8) + lcd_gamma_tbl[items - 1][1];

	memcpy(info->lcd_cmap_tbl, lcd_cmap_tbl, sizeof(lcd_cmap_tbl));

}


static s32 LCD_open_flow(u32 sel)
{
	/* open lcd power, and delay 50ms */
	LCD_OPEN_FUNC(sel, LCD_power_on, 100);
	/* open lcd controller, and delay 100ms */
	LCD_OPEN_FUNC(sel, sunxi_lcd_tcon_enable, 10);
	/* open lcd power, than delay 200ms */
	LCD_OPEN_FUNC(sel, LCD_panel_init, 100);
	/* open lcd backlight, and delay 0ms */
	LCD_OPEN_FUNC(sel, LCD_bl_open, 10);

	return 0;
}

static s32 LCD_close_flow(u32 sel)
{
	/* close lcd backlight, and delay 0ms */
	LCD_CLOSE_FUNC(sel, LCD_bl_close, 0);
	/* close lcd controller, and delay 0ms */
	LCD_CLOSE_FUNC(sel, sunxi_lcd_tcon_disable, 0);
	/* open lcd power, than delay 200ms */
	LCD_CLOSE_FUNC(sel, LCD_panel_exit, 200);
	/* close lcd power, and delay 500ms */
	LCD_CLOSE_FUNC(sel, LCD_power_off, 500);

	return 0;
}

static void LCD_power_on(u32 sel)
{
	/* config lcd_power pin to open lcd power0 */
	sunxi_lcd_power_enable(sel, 0);
	sunxi_lcd_pin_cfg(sel, 1);
#ifdef CONFIG_IO_EXPAND
	sunxi_lcd_delay_ms(1);
	sunxi_lcd_gpio_set_direction(0, 0, 1);
	sunxi_lcd_gpio_set_value(0, 0, 0);
#endif
}

static void LCD_power_off(u32 sel)
{
	sunxi_lcd_pin_cfg(sel, 0);
	/* config lcd_power pin to close lcd power0 */
	sunxi_lcd_power_disable(sel, 0);
}

static void LCD_bl_open(u32 sel)
{
	sunxi_lcd_pwm_enable(sel);	/* open pwm module */
	/* config lcd_bl_en pin to open lcd backlight */
	sunxi_lcd_backlight_enable(sel);
}

static void LCD_bl_close(u32 sel)
{
	/* config lcd_bl_en pin to close lcd backlight */
	sunxi_lcd_backlight_disable(sel);
	sunxi_lcd_pwm_disable(sel);	/* close pwm module */
}

static void LCD_panel_init(u32 sel)
{
	/*printk("[LWH] --- Line:%d , Func:%s , File:%s ======SEL = %d====\n", __LINE__, __FUNCTION__, __FILE__, sel);*/
	__u32 lcd_id = 0;

	/*--------------	硬件复位	--------------*/
	sunxi_lcd_gpio_set_value(0, 0, 0);
	sunxi_lcd_delay_ms(20);
	sunxi_lcd_gpio_set_value(0, 0, 1);
	sunxi_lcd_delay_ms(200);

	/*tcon0_cpu_wr_16b_index(sel, 0x01);*/	/*sw reset*/
	/*sunxi_lcd_delay_ms(120);*/
	#if 0 /*test read id*/
		tcon0_cpu_wr_16b_index(sel, 0x04);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id0:[0x%X]========0===========\n", lcd_id);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id1:[0x%X]========1===========\n", lcd_id);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id2:[0x%X]========2===========\n", lcd_id);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id3:[0x%X]========3===========\n", lcd_id);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id4:[0x%X]========4===========\n", lcd_id);
	#elif 0
		tcon0_cpu_wr_16b_index(sel, 0xDA);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id:[0x%X]===================\n", lcd_id);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id:[0x%X]===================\n", lcd_id);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id:[0x%X]===================\n", lcd_id);
		tcon0_cpu_wr_16b_index(sel, 0xDB);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id:[0x%X]===================\n", lcd_id);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id:[0x%X]===================\n", lcd_id);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id:[0x%X]===================\n", lcd_id);
		tcon0_cpu_wr_16b_index(sel, 0xDC);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id:[0x%X]===================\n", lcd_id);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id:[0x%X]===================\n", lcd_id);
		tcon0_cpu_rd_16b_data(sel, &lcd_id);
		printk("[LWH] --- lcd_id:[0x%X]===================\n", lcd_id);
		sunxi_lcd_delay_ms(100);
	#else
	#endif

	/*--------------	指令和数据初始化	--------------*/
	tcon0_cpu_wr_16b_index(0, 0x11);	/*sleep out*/
	sunxi_lcd_delay_ms(200);
	#if 0
		tcon0_cpu_wr_16b_index(sel, 0xB1);		/*frame Rate Control (In normal mode/ Full colors)*/
		tcon0_cpu_wr_16b_data (sel, 0x02);
		tcon0_cpu_wr_16b_data (sel, 0x35);
		tcon0_cpu_wr_16b_data (sel, 0x36);
	#endif

	tcon0_cpu_wr_16b_index(0, 0x36);
	tcon0_cpu_wr_16b_data(0, 0x00);

	tcon0_cpu_wr_16b_index(0, 0x21);

	tcon0_cpu_wr_16b_index(0, 0x3A);
	tcon0_cpu_wr_16b_data(0, 0x66);

	tcon0_cpu_wr_16b_index(0, 0xB2);	/*PORCTRL*/
	tcon0_cpu_wr_16b_data(0, 0x05);
	tcon0_cpu_wr_16b_data(0, 0x05);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x33);
	tcon0_cpu_wr_16b_data(0, 0x33);

	tcon0_cpu_wr_16b_index(0, 0xB7);	/*GCTRL*/
	tcon0_cpu_wr_16b_data(0, 0x75);		/*VGH=14.97V, VGL=-10.43V*/

	tcon0_cpu_wr_16b_index(0, 0xBB);	/*VCOM default*/
	tcon0_cpu_wr_16b_data(0, 0x22);

	tcon0_cpu_wr_16b_index(0, 0xC0);	/*LCMCTRL default*/
	tcon0_cpu_wr_16b_data(0, 0x2C);

	#if 0
		tcon0_cpu_wr_16b_index(0, 0xC2);	/*VDVVRHEN default*/
		tcon0_cpu_wr_16b_data(0, 0x01);
		tcon0_cpu_wr_16b_data(0, 0xFF);

		tcon0_cpu_wr_16b_index(0, 0xC3);	/*VRHS default*/
		tcon0_cpu_wr_16b_data(0, 0x0B);

		tcon0_cpu_wr_16b_index(0, 0xC4);	/*VDVS default*/
		tcon0_cpu_wr_16b_data(0, 0x20);

		tcon0_cpu_wr_16b_index(0, 0xC6);	/*FRCTRL2 default*/
		tcon0_cpu_wr_16b_data(0, 0x0F);

		tcon0_cpu_wr_16b_index(0, 0xD0);	/*PWCTRL1 default*/
		tcon0_cpu_wr_16b_data(0, 0xA4);
		tcon0_cpu_wr_16b_data(0, 0xA1);
	#endif

	tcon0_cpu_wr_16b_index(0, 0xE0);	/*PVGAMCTRL*/
	tcon0_cpu_wr_16b_data(0, 0xD0);
	tcon0_cpu_wr_16b_data(0, 0x1A);
	tcon0_cpu_wr_16b_data(0, 0x1E);
	tcon0_cpu_wr_16b_data(0, 0x0A);
	tcon0_cpu_wr_16b_data(0, 0x0A);
	tcon0_cpu_wr_16b_data(0, 0x27);
	tcon0_cpu_wr_16b_data(0, 0x3B);
	tcon0_cpu_wr_16b_data(0, 0x44);
	tcon0_cpu_wr_16b_data(0, 0x4A);
	tcon0_cpu_wr_16b_data(0, 0x2B);
	tcon0_cpu_wr_16b_data(0, 0x16);
	tcon0_cpu_wr_16b_data(0, 0x15);
	tcon0_cpu_wr_16b_data(0, 0x1A);
	tcon0_cpu_wr_16b_data(0, 0x1E);

	tcon0_cpu_wr_16b_index(0, 0xE1);	/*NVGAMCTRL*/
	tcon0_cpu_wr_16b_data(0, 0xD0);
	tcon0_cpu_wr_16b_data(0, 0x1A);
	tcon0_cpu_wr_16b_data(0, 0x1E);
	tcon0_cpu_wr_16b_data(0, 0x0A);
	tcon0_cpu_wr_16b_data(0, 0x0A);
	tcon0_cpu_wr_16b_data(0, 0x27);
	tcon0_cpu_wr_16b_data(0, 0x3A);
	tcon0_cpu_wr_16b_data(0, 0x43);
	tcon0_cpu_wr_16b_data(0, 0x49);
	tcon0_cpu_wr_16b_data(0, 0x2B);
	tcon0_cpu_wr_16b_data(0, 0x16);
	tcon0_cpu_wr_16b_data(0, 0x15);
	tcon0_cpu_wr_16b_data(0, 0x1A);
	tcon0_cpu_wr_16b_data(0, 0x1D);

	tcon0_cpu_wr_16b_index(0, 0x2A);	/*Column*/
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0xEF);
	tcon0_cpu_wr_16b_index(0, 0x2B);	/*Row*/
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x01);
	tcon0_cpu_wr_16b_data(0, 0x3F);

	tcon0_cpu_wr_16b_index(0, 0x29);	/*Display On*/
	sunxi_lcd_delay_ms(50);
	tcon0_cpu_wr_16b_index(0, 0x2C);	/*Memory Write*/
	sunxi_lcd_delay_ms(50);
	/*--------------	设置中断处理刷屏	--------------*/
	disp_lcd_cpu_register_irq(sel, lcd_cpu_isr_proc);
	tcon0_cpu_auto_flash(sel, true);
}

static void LCD_panel_exit(u32 sel)
{

}

void  lcd_cpu_isr_proc(void)
{
	tcon0_cpu_wr_16b_index(0, 0x2A);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0xEF);

	tcon0_cpu_wr_16b_index(0, 0x2B);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x00);
	tcon0_cpu_wr_16b_data(0, 0x01);
	tcon0_cpu_wr_16b_data(0, 0x3F);

	tcon0_cpu_wr_16b_index(0, 0x2C);
}

/* sel: 0:lcd0; 1:lcd1 */
static s32 LCD_user_defined_func(u32 sel, u32 para1, u32 para2, u32 para3)
{
	return 0;
}

struct __lcd_panel_t zjx2_0lcm_240x320 = {
	/* panel driver name, must mach the name of */
	/*lcd_drv_name in sys_config.fex */
	.name = "zjx2_0lcm_240x320",
	.func = {

		 .cfg_panel_info = LCD_cfg_panel_info,

		 .cfg_open_flow = LCD_open_flow,
		 .cfg_close_flow = LCD_close_flow,
		 .lcd_user_defined_func = LCD_user_defined_func,
		 },
};
