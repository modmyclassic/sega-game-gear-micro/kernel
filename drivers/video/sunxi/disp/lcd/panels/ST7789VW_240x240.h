/* display driver
 *
 * Copyright (c) 2017 Allwinnertech Co., Ltd.
 * Author: Tyle <tyle@allwinnertech.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
*/

#ifndef __ST7789VM_240X240_H__
#define  __ST7789VM_240X240_H__

#include "panels.h"

extern struct __lcd_panel_t ST7789VW_240x240_panel;

#endif
