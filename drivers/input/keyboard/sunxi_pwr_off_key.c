/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Copyright (c) 2014
 *
 * ChangeLog
 */

#include <linux/module.h>
#include <linux/init.h>
#include <linux/input.h>
#include <linux/errno.h>
#include <linux/kernel.h>
#include <linux/sys_config.h>
#include <linux/of_platform.h>
#include <linux/platform_device.h>

#include <linux/io.h>
#include <linux/of_gpio.h>
#include <linux/gpio.h>
#include <linux/ioport.h>

#include <linux/interrupt.h>

#include <linux/of_address.h>
#include <linux/slab.h>

static struct of_device_id const sunxi_pwr_off_of_match[] = {
	{ .compatible = "allwinner,pwr_off_ctrl" },
};

struct pwroff_data {
	int irq_num;
	struct gpio_config pwr_off_gpio;
	struct timer_list timer;
	struct input_dev *input_dev;
	int key_value;
	int gpio_is_irq;
	struct timer_list timer_auto;
};
struct pwroff_data *key_data;

static irqreturn_t buttons_irq(int irq, void *dev_id)
{
	struct pwroff_data *key_data = (struct pwroff_data *)dev_id;
	mod_timer(&key_data->timer, jiffies + HZ / 10);
	return IRQ_RETVAL(IRQ_HANDLED);
}

static void buttons_timer_function(unsigned long data)
{
	u32 pinval;
	static int flag;
	pinval = __gpio_get_value(key_data->pwr_off_gpio.gpio);
	if (pinval) {
		if (flag == 0) {
			flag++;
			input_report_key(key_data->input_dev,
				key_data->key_value, 1);
			input_sync(key_data->input_dev);
		}
		mod_timer(&key_data->timer, jiffies + HZ / 10);
	} else {
		input_report_key(key_data->input_dev,  key_data->key_value, 0);
		input_sync(key_data->input_dev);
		flag = 0;
	}
}

static void buttons_timer_function_auto(unsigned long data)
{
	u32 pinval;
	static int flag;
	static int down_flag;

	pinval = __gpio_get_value(key_data->pwr_off_gpio.gpio);

	if (pinval) {
		pr_info("down\n");
		down_flag = 1;
		input_report_key(key_data->input_dev,
			key_data->key_value, 1);
		input_sync(key_data->input_dev);
		pr_info("down\n");
	} else {
		if (down_flag == 1) {
			pr_info("up\n");
			down_flag = 0;
			input_report_key(key_data->input_dev,
				key_data->key_value, 0);
			input_sync(key_data->input_dev);
		}
	}
	mod_timer(&key_data->timer_auto, jiffies + HZ / 10);
}

static int sunxi_pwr_off_probe(struct platform_device *pdev)
{
	int err = 0;
	int value = 0;
	struct device_node *np = NULL;
	struct input_dev *pwroff_dev = NULL;

	pr_info("%s\n", __func__);
	key_data = kzalloc(sizeof(*key_data), GFP_KERNEL);
	if (IS_ERR_OR_NULL(key_data)) {
		pr_err("key_data: not enough memory for key data\n");
		return -ENOMEM;
	}

	np = of_find_node_by_name(NULL, "pwr_ctrl");
	if (!np)
		goto err1;

	if (of_property_read_u32(np, "pwroff_gpio_is_irq", &value)) {
		pr_err("%s: get off_gpio_is_irq failed", __func__);
		goto err1;
	}
	key_data->gpio_is_irq = value;
	pr_info("gpio_is_irq=%d\n", key_data->gpio_is_irq);

	key_data->pwr_off_gpio.gpio = of_get_named_gpio_flags(np,
		"power_off_key", 0,
		(enum of_gpio_flags *)(&key_data->pwr_off_gpio));
	if (!gpio_is_valid(key_data->pwr_off_gpio.gpio)) {
		pr_err("%s: pwr_off_gpio is invalid.\n", __func__);
		goto err1;
	}
	pr_info("pwr_off_gpio:(%d, %d, %d, %d, %d)\n",
		key_data->pwr_off_gpio.gpio, key_data->pwr_off_gpio.mul_sel,
		key_data->pwr_off_gpio.pull, key_data->pwr_off_gpio.drv_level,
		key_data->pwr_off_gpio.data);

	if (key_data->gpio_is_irq == 1) {
		init_timer(&key_data->timer);
		key_data->timer.function = buttons_timer_function;

		key_data->irq_num = gpio_to_irq(key_data->pwr_off_gpio.gpio);
		pr_info("irq=%d, gpio=%d\n", key_data->irq_num,
			key_data->pwr_off_gpio.gpio);
		if (request_irq(key_data->irq_num, buttons_irq,
			IRQF_TRIGGER_RISING, "pwroffkey", key_data)) {
			pr_err("request irq failure.\n");
			goto err1;
		}
	} else {
		init_timer(&key_data->timer_auto);
		key_data->timer_auto.function = buttons_timer_function_auto;
		mod_timer(&key_data->timer_auto, jiffies + HZ / 10);
	}

	key_data->key_value = KEY_9;

	pwroff_dev = input_allocate_device();
	if (!pwroff_dev) {
		pr_err("pwroff_dev: not enough memory for input device\n");
		goto err2;
	}

	pwroff_dev->name = "sunxi-pwroff";
	pwroff_dev->phys = "/input2";
	pwroff_dev->id.bustype = BUS_HOST;
	pwroff_dev->id.vendor = 0x0001;
	pwroff_dev->id.product = 0x0001;
	pwroff_dev->id.version = 0x0100;
	pwroff_dev->evbit[0] = BIT_MASK(EV_KEY);

	set_bit(key_data->key_value , pwroff_dev->keybit);

	key_data->input_dev = pwroff_dev;
	platform_set_drvdata(pdev, key_data);

	err = input_register_device(key_data->input_dev);
	if (err)
		goto err3;

	pr_err("pwroff_init ok.\n");
	return 0;

err3:
	input_free_device(key_data->input_dev);
err2:
	if (key_data->gpio_is_irq == 1) {
		free_irq(key_data->irq_num, NULL);
		del_timer(&key_data->timer);
	} else {
		del_timer(&key_data->timer_auto);
	}
err1:
	kfree(key_data);

	pr_err("pwroff_init failed.\n");
	return -ENOMEM;
}

static int sunxi_pwr_off_remove(struct platform_device *pdev)
{
	pr_info("%s\n", __func__);
	input_unregister_device(key_data->input_dev);
	input_free_device(key_data->input_dev);
	if (key_data->gpio_is_irq == 1) {
		free_irq(key_data->irq_num, NULL);
		del_timer(&key_data->timer);
	} else
		del_timer(&key_data->timer_auto);

	kfree(key_data);
	return 0;
}

static struct platform_driver sunxi_pwr_off_driver = {
	.probe  = sunxi_pwr_off_probe,
	.remove = sunxi_pwr_off_remove,
	.driver = {
		.name   = "sunxi_pwr_off",
		.owner  = THIS_MODULE,
		.of_match_table = of_match_ptr(sunxi_pwr_off_of_match),
	},
};

module_platform_driver(sunxi_pwr_off_driver);
MODULE_AUTHOR("F*H");
MODULE_DESCRIPTION("sunxi power off key");
MODULE_LICENSE("GPL");
